/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.RuntimeTests.JsonpLoader.Data({
    extendsConfig: "../../test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/OpenVX_1_0_1/NodesRegressionData.jsonp",
    VX_CANNY_EDGE_DETECTOR: {
        cpp: {
            create: {
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_int32 vxData1Value = 50;" +
                    "auto vxData1Ref = vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_RANGE, VX_TYPE_UINT8);" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxData1Ref;" +
                    "vxSetThresholdAttribute(vxData1Ref, VX_THRESHOLD_THRESHOLD_LOWER, &vxData1Value, sizeof(vxData1Value));" +
                    "vxData1Value = 150;" +
                    "vxSetThresholdAttribute(vxData1Ref, VX_THRESHOLD_THRESHOLD_UPPER, &vxData1Value, sizeof(vxData1Value));" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }
    },
    VX_CUSTOM_CONVOLUTION: {
        cpp: {
            create: {
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_int16 vxData1Value[3][3] = {\n" +
                    "            {1, 0, -1},\n" +
                    "            {3, 0, -3},\n" +
                    "            {1, 0, -1}\n" +
                    "    };" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateConvolution(this->getParent()->getVxContext(), 3, 3);" +
                    "vxCopyConvolutionCoefficients((vx_convolution)this->vxDataMap[\"vxData1\"], &vxData1Value[0][0], VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_S16);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }
    },
    VX_REMAP: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_REMAP;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        imageType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        width: 310,\r\n" +
                "        height: 310,\r\n" +
                "        policy: \"VX_INTERPOLATION_BILINEAR\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxRemapNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_remap)this->vxDataMap[\"vxData1\"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}"
            }
        }
    },
    VX_SCALE_IMAGE: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_SCALE_IMAGE;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        imageType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        width: 310,\r\n" +
                "        height: 310,\r\n" +
                "        scaleType: \"VX_INTERPOLATION_NEAREST_NEIGHBOR\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxScaleImageNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_image)this->vxDataMap[\"vxData1\"], VX_INTERPOLATION_NEAREST_NEIGHBOR);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}"
            }
        }
    },
    VX_TABLE_LOOK_UP: {
        cpp: {
            create: {
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_uint8 *vxData1Value = nullptr;std::vector<vx_uint8> vxData1Data = {\n" +
                    "            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,\n" +
                    "            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,\n" +
                    "            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,\n" +
                    "            61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,\n" +
                    "            81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,\n" +
                    "            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,\n" +
                    "            121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,\n" +
                    "            141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,\n" +
                    "            161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,\n" +
                    "            181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200,\n" +
                    "            201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,\n" +
                    "            221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,\n" +
                    "            241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255\n" +
                    "    };" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateLUT(this->getParent()->getVxContext(), VX_TYPE_UINT8, 256);" +
                    "vxCopyLUT((vx_lut)this->vxDataMap[\"vxData1\"], (void **)&vxData1Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);" +
                    "vxTableLookupNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_lut)this->vxDataMap[\"vxData1\"], (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }
    },
    VX_THRESHOLD: {
        cpp: {
            create: {
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_int32 vxData1Value = 140;" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);" +
                    "vxSetThresholdAttribute((vx_threshold)this->vxDataMap[\"vxData1\"], VX_THRESHOLD_THRESHOLD_VALUE, &vxData1Value, sizeof(vxData1Value));" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }
    },
    VX_WARP_AFFINE: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_WARP_AFFINE;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        imageType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        dataType: \"VX_TYPE_FLOAT32\",\r\n" +
                "        interpolationType: \"VX_INTERPOLATION_BILINEAR\",\r\n" +
                "        columns: 2,\r\n" +
                "        rows: 3,\r\n" +
                "        matrix: [[0.707,0.707],[-0.707,0.707],[0,0]],\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxWarpAffineNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_matrix)this->vxDataMap[\"vxData1\"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}",
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_float32 vxData1Value[3][2] = {\n" +
                    "            {0.707f, 0.707f},\n" +
                    "            {-.707f, 0.707f},\n" +
                    "            {0.0f, 0.0f}\n" +
                    "    };" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 2, 3);" +
                    "vxCopyMatrix((vx_matrix)this->vxDataMap[\"vxData1\"], (void **)&vxData1Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }

    },
    VX_WARP_PERSPECTIVE: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_WARP_PERSPECTIVE;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        imageType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        dataType: \"VX_TYPE_FLOAT32\",\r\n" +
                "        interpolationType: \"VX_INTERPOLATION_BILINEAR\",\r\n" +
                "        columns: 3,\r\n" +
                "        rows: 3,\r\n" +
                "        matrix: [[1,0,0],[0,1,0],[0,0,1]],\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxWarpPerspectiveNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_matrix)this->vxDataMap[\"vxData1\"], VX_INTERPOLATION_BILINEAR, (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}",
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "vx_float32 vxData1Value[3][3] = {\n" +
                    "            {1.000f, 0.0f, 0.0f},\n" +
                    "            {0.0f, 1.000f, 0.0f},\n" +
                    "            {0.0f, 0.0f, 1.000f}\n" +
                    "    };" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 3, 3);" +
                    "vxCopyMatrix((vx_matrix)this->vxDataMap[\"vxData1\"], (void **)&vxData1Value, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            }
        }
    },
    VX_NONLINEAR_FILTER: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_NONLINEAR_FILTER;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        imageType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        function: \"VX_NONLINEAR_FILTER_MEDIAN\",\r\n" +
                "        dataType: \"VX_MATRIX_PATTERN\",\r\n" +
                "        columns: 3,\r\n" +
                "        rows: 3,\r\n" +
                "        matrixType: \"VX_PATTERN_BOX\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",
            data: [
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData0\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].imageType\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_MATRIX;\r\n" +
                "        },\r\n" +
                "        id: \"vxData1\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                this.vxAPI.vxTypes.VX_MATRIX,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].dataType,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].columns,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].rows,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].matrixType\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData2\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].imageType\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
            ]
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxNonLinearFilterNode(this->getVxGraph(), VX_NONLINEAR_FILTER_MEDIAN, (vx_image)this->vxDataMap[\"vxData0\"], (vx_matrix)this->vxDataMap[\"vxData1\"], (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}",
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreateMatrixFromPattern(this->getParent()->getVxContext(), VX_PATTERN_BOX, 3, 3);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), " +
                    "BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            },
            process: {
                node: "",
                data: []
            }
        }
    },
    VX_LAPLACIAN_PYRAMID: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_LAPLACIAN_PYRAMID;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        inputType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        outputType: \"VX_DF_IMAGE_S16\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",
            data: [
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData0\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].inputType\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_PYRAMID;\r\n" +
                "        },\r\n" +
                "        id: \"vxData1\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].outputType\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData2\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].outputType\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
            ]
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxLaplacianPyramidNode(this->getVxGraph(), (vx_image)this->vxDataMap[\"vxData0\"], (vx_pyramid)this->vxDataMap[\"vxData1\"], (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}",
                data: [
                    "if (status == VX_SUCCESS) {this->vxDataMap[\"vxData0\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData1\"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 0, 0, 0, 0, VX_DF_IMAGE_S16);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData0\"]), " +
                    "BaseGraph::getImageHeight(this->vxDataMap[\"vxData0\"]), VX_DF_IMAGE_S16);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            },
            process: {
                node: "",
                data: []
            }
        }
    },
    VX_LAPLACIAN_PYRAMID_RECONSTRUCTION: {
        jsonp: {
            node: "" +
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_LAPLACIAN_PYRAMID_RECONSTRUCTION;\r\n" +
                "        },\r\n" +
                "        name: \"testNode\",\r\n" +
                "        id: \"vxNode0\",\r\n" +
                "        pyramidLevels: 1,\r\n" +
                "        pyramidScale: \"VX_SCALE_PYRAMID_HALF\",\r\n" +
                "        pyramidWidth: 0,\r\n" +
                "        pyramidHeight: 0,\r\n" +
                "        pyramidImgFormat: \"VX_DF_IMAGE_S16\",\r\n" +
                "        inputType: \"VX_DF_IMAGE_S16\",\r\n" +
                "        outputType: \"VX_DF_IMAGE_U8\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[2]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",
            data: [
                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_PYRAMID;\r\n" +
                "        },\r\n" +
                "        id: \"vxData0\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].pyramidLevels,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].pyramidScale,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].pyramidWidth,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].pyramidHeight,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].pyramidImgFormat\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData1\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                0,\r\n" +
                "                0,\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].inputType\r\n" +
                "            ];\r\n" +
                "        },\r\n" +
                "        outputs() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0]\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }",

                "    {\r\n" +
                "        type() {\r\n" +
                "            return this.vxAPI.vxTypes.VX_IMAGE;\r\n" +
                "        },\r\n" +
                "        id: \"vxData2\",\r\n" +
                "        params() {\r\n" +
                "            return [\r\n" +
                "                this.vxContexts[0],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxData[1],\r\n" +
                "                this.vxContexts[0].vxGraphs[0].vxNodes[0].outputType\r\n" +
                "            ];\r\n" +
                "        }\r\n" +
                "    }"
            ]
        },
        cpp: {
            create: {
                node: "" +
                    "if (status == VX_SUCCESS) {" +
                    "this->vxNodesMap[\"vxNode0\"] = (vx_reference)vxLaplacianReconstructNode(this->getVxGraph(), (vx_pyramid)this->vxDataMap[\"vxData0\"], (vx_image)this->vxDataMap[\"vxData1\"], (vx_image)this->vxDataMap[\"vxData2\"]);" +
                    "status = this->getParent()->Check(this->vxNodesMap[\"vxNode0\"]);" +
                    "}",
                data: [
                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData0\"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 1, VX_SCALE_PYRAMID_HALF, 0, 0, VX_DF_IMAGE_S16);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData0\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData1\"] = this->createImage(this->getParent()->getVxContext(), 1, 1, VX_DF_IMAGE_S16);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData1\"]);" +
                    "}",

                    "if (status == VX_SUCCESS) {" +
                    "this->vxDataMap[\"vxData2\"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap[\"vxData1\"]), " +
                    "BaseGraph::getImageHeight(this->vxDataMap[\"vxData1\"]), VX_DF_IMAGE_U8);" +
                    "status = this->getParent()->Check(this->vxDataMap[\"vxData2\"]);" +
                    "}"
                ]
            },
            process: {
                node: "",
                data: []
            }
        }
    }
});
