/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_KERNELS_ENUMS_KERNELTYPE_HPP_
#define IO_VISIONSDK_KERNELS_ENUMS_KERNELTYPE_HPP_

/**
 * This enumeration defines IDs of each custom kernel nodes in that project. Be careful with definitions every
 * single node ID must start on company ID (VX_ID_NXP) with optional shift (0x01) which could
 * split kernel groups etc. Last part of ID is iterative index. Resulting ID has to be UNIQUE.
 */
enum Io::VisionSDK::Kernels::Enums::KernelType : int {
    VX_KERNEL_EXAMPLE = VX_KERNEL_BASE(VX_ID_NXP, 0x0) + 0x0,
    VX_KERNEL_EXAMPLE1 = VX_KERNEL_BASE(VX_ID_NXP, 0x0) + 0x1
};

#endif  // IO_VISIONSDK_KERNELS_ENUMS_KERNELTYPE_HPP_
