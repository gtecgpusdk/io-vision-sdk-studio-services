/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef TESTCASE_1_1_VXGRAPH0_HPP_
#define TESTCASE_1_1_VXGRAPH0_HPP_

#include <io-vision-sdk-studio-libs.hpp>

#include "Reference.hpp"

namespace TestCase_1_1 {
    /**
     * This class overrides BaseGraph methods with generated code.
     */
    class vxGraph0 : public Io::VisionSDK::Studio::Libs::Primitives::BaseGraph {
     public:
        /**
         * Constructs Graph object from parent Context.
         * @param context Specify parent Context.
         */
        explicit vxGraph0(Io::VisionSDK::Studio::Libs::Primitives::BaseContext *context);

     protected:
        vx_status create() override;

        vx_status process(const std::function<vx_status(int)> &$handler) override;

        bool loopCondition(int $loopCnt) const override;
    };
}

#endif  // TESTCASE_1_1_VXGRAPH0_HPP_
