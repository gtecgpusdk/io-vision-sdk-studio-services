/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "BaseGraph.hpp"

namespace NewVisualProject {
    BaseContext::BaseContext(VisualGraph *parent)
            : parent(parent) {
        this->vxContext = vxCreateContext();
    }

    BaseContext::~BaseContext() {
        if (this->vxContext != nullptr) {
            for (auto it : this->graphsMap) {
                delete it.second;
            }
            this->graphsMap.clear();
            vxReleaseContext(&this->vxContext);
        }
    }

    const VisualGraph *BaseContext::getParent() const {
        return this->parent;
    }

    vx_context BaseContext::getVxContext() const {
        return this->vxContext;
    }

    const BaseGraph *BaseContext::getGraph(const std::string &name) const {
        if (this->graphsMap.find(name) != this->graphsMap.end()) {
            return this->graphsMap.at(name);
        }
        return nullptr;
    }

    void BaseContext::removeGraph(const std::string &name) {
        if (this->graphsMap.find(name) != this->graphsMap.end()) {
            delete this->graphsMap.at(name);
            this->graphsMap.erase(name);
        }
    }
}
