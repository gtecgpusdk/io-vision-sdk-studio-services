/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Context0.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;
    using Io::VisionSDK::Studio::Libs::Media::Image;
    using Io::VisionSDK::Studio::Libs::Media::Video;
    using Io::VisionSDK::Studio::Libs::Media::Camera;
    using Io::VisionSDK::Studio::Libs::Media::Display;

    VisualGraph::VisualGraph()
            : BaseVisualGraph() {
        this->contextsMap["context0"] = new Context0(this);
    }

    vx_status VisualGraph::process(const std::function<vx_status(int)> &$handler) {
        return BaseVisualGraph::process([&](int $iteration) -> vx_status {
            vx_status status = VX_FAILURE;
            BaseContext *context0 = const_cast<BaseContext *>(this->getContext("context0"));
            if (context0 != nullptr) {
                status = VX_SUCCESS;
            }
            BaseGraph *graph_0_0 = nullptr;
            if (status == VX_SUCCESS) {
                graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("graph0"));
                if (graph_0_0 != nullptr) {
                    status = VX_SUCCESS;
                } else {
                    status = VX_FAILURE;
                }
            }
            if (status == VX_SUCCESS) {
                this->ioComMap.emplace("ioCom0", std::make_shared<Image>("../data/input/VisionSDK.png", context0->getVxContext()));
            }
            if (status == VX_SUCCESS) {
                this->ioComMap.emplace("ioCom1", std::make_shared<Image>("../data/output/ioCom1.png", context0->getVxContext()));
            }
            if (status == VX_SUCCESS) {
                auto ioCom2 = std::make_shared<Camera>(context0->getVxContext());
                this->ioComMap.emplace("ioCom2", ioCom2);
                ioCom2->Open(0);
                auto vxData119Scalar = (vx_float32)10;
                vxCopyScalar((vx_scalar)graph_0_0->getData("vxData119"), &vxData119Scalar, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            }
            if (status == VX_SUCCESS) {
                if (!this->IsHeadless()) {
                    this->ioComMap.emplace("ioCom3", std::make_shared<Display>(context0->getVxContext(), "Display for ioCom3"));
                }
            }
            if (status == VX_SUCCESS) {
                status = context0->Process();
            }
            return status;
        });
    }
}
