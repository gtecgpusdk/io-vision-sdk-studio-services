/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef TESTCASE_1_1_VISUALGRAPH_HPP_
#define TESTCASE_1_1_VISUALGRAPH_HPP_

#include <io-vision-sdk-studio-libs.hpp>

#include "Reference.hpp"

namespace TestCase_1_1 {
    /**
     * This class holds main application business logic.
     */
    class VisualGraph : public Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph {
     public:
        VisualGraph();

     protected:
        vx_status process(const std::function<vx_status(int)> &$handler) override;
    };
}

#endif  // TESTCASE_1_1_VISUALGRAPH_HPP_
