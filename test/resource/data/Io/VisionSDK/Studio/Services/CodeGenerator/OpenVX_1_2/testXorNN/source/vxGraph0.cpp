/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "vxGraph0.hpp"
#include "VisualGraph.hpp"

namespace TestCase_1_1 {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;

    vxGraph0::vxGraph0(BaseContext *parent)
            : BaseGraph(parent) {}

    vx_status vxGraph0::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            vx_size vxData0Dims[] = {2};
            this->vxDataMap["vxData0"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData0Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData0"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData1Dims[] = {2, 4};
            this->vxDataMap["vxData1"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 2, vxData1Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData1"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData2Dims[] = {4};
            this->vxDataMap["vxData2"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData2Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData2"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData3Dims[] = {4};
            this->vxDataMap["vxData3"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData3Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData3"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData4Dims[] = {4};
            this->vxDataMap["vxData4"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData4Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData4"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData5Dims[] = {4, 1};
            this->vxDataMap["vxData5"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 2, vxData5Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData5"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData6Dims[] = {1};
            this->vxDataMap["vxData6"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData6Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData6"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData7Dims[] = {1};
            this->vxDataMap["vxData7"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData7Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData7"]);
        }
        if (status == VX_SUCCESS) {
            vx_size vxData8Dims[] = {1};
            this->vxDataMap["vxData8"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData8Dims, VX_TYPE_INT16, 8);
            status = this->getParent()->Check(this->vxDataMap["vxData8"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode1"] = (vx_reference)vxFullyConnectedLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData0"], (vx_tensor)this->vxDataMap["vxData1"], (vx_tensor)this->vxDataMap["vxData2"], VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_NEAREST_EVEN, (vx_tensor)this->vxDataMap["vxData3"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode1"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode2"] = (vx_reference)vxActivationLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData3"], VX_NN_ACTIVATION_HYPERBOLIC_TAN, 1, 1, (vx_tensor)this->vxDataMap["vxData4"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode2"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode3"] = (vx_reference)vxFullyConnectedLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData4"], (vx_tensor)this->vxDataMap["vxData5"], (vx_tensor)this->vxDataMap["vxData6"], VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_NEAREST_EVEN, (vx_tensor)this->vxDataMap["vxData7"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode3"]);
        }
        if (status == VX_SUCCESS) {
            this->vxNodesMap["vxNode4"] = (vx_reference)vxActivationLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData7"], VX_NN_ACTIVATION_LOGISTIC, 1, 1, (vx_tensor)this->vxDataMap["vxData8"]);
            status = this->getParent()->Check(this->vxNodesMap["vxNode4"]);
        }
        return status;
    }

    vx_status vxGraph0::process(const std::function<vx_status(int)> &$handler) {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            status = BaseGraph::process([&](int $iteration) -> vx_status {
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                    if ($iteration == 0) {
                        auto vxData0Ref = this->getData("vxData0");
                        status = this->getParent()->Check(ioCom0->getData(vxData0Ref, true));
                    }
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                    if ($iteration == 0) {
                        auto vxData1Ref = this->getData("vxData1");
                        status = this->getParent()->Check(ioCom1->getData(vxData1Ref, true));
                    }
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                    if ($iteration == 0) {
                        auto vxData2Ref = this->getData("vxData2");
                        status = this->getParent()->Check(ioCom2->getData(vxData2Ref, true));
                    }
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                    if ($iteration == 0) {
                        auto vxData5Ref = this->getData("vxData5");
                        status = this->getParent()->Check(ioCom3->getData(vxData5Ref, true));
                    }
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom4 = this->getParent()->getParent()->getIoCom("ioCom4");
                    if ($iteration == 0) {
                        auto vxData6Ref = this->getData("vxData6");
                        status = this->getParent()->Check(ioCom4->getData(vxData6Ref, true));
                    }
                }
                if (status == VX_SUCCESS) {
                    status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom5 = this->getParent()->getParent()->getIoCom("ioCom5");
                    auto vxData8Ref = this->getData("vxData8");
                    status = this->getParent()->Check(ioCom5->setData(vxData8Ref));
                }
                return status;
            });
        }
        return status;
    }

    bool vxGraph0::loopCondition(int $loopCnt) const {
        return BaseGraph::loopCondition($loopCnt);
    }
}
