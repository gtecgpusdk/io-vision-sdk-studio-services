/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "Context.hpp"
#include "Graph.hpp"

namespace NewVisualProject {
    Context::Context(VisualGraph *parent)
            : BaseContext(parent) {
    }

    vx_status Context::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxContext());

        if (status == VX_SUCCESS) {
            this->graphsMap["graph0"] = new Graph(this);

            status = this->graphsMap["graph0"]->create();
            if (status != VX_SUCCESS) {
                this->removeGraph("graph0");
            }
        }

        return status;
    }

    vx_status Context::process() const {
        vx_status status = vxGetStatus((vx_reference)this->getVxContext());

        if (status == VX_SUCCESS) {
            auto graph0 = this->getGraph("graph0");
            if (graph0 != nullptr) {
                status = graph0->process();
                if (status != VX_SUCCESS) {
                    const_cast<Context *>(this)->removeGraph("graph0");
                }
            } else {
                status = VX_FAILURE;
            }
        }

        return status;
    }
}
