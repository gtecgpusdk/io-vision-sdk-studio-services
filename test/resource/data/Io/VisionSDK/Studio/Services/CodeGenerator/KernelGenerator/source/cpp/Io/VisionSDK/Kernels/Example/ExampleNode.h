/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLENODE_H_
#define IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLENODE_H_

#include <VX/vx.h>

/**
 * Interface to OpenVX custom kernel.
 * Enter some algorithm description here.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create Example node.
 * @param $graph The handle to the graph.
 * @param $input Specify input image in VX_DF_IMAGE_U8 type.
 * @param $shift Specify vx_uint32 scalar.
 * @param $output The output image in VX_DF_IMAGE_U8 type with same size as input image.
 * @return Returns instance of Example node.
 */
vx_node vxExampleNode(vx_graph $graph, vx_image $input, vx_scalar $shift, vx_image $output);

/**
 * Immediate version of Example.
 * @param $context The handle to the context.
 * @param $input Specify input image in VX_DF_IMAGE_U8 type.
 * @param $shift Specify vx_uint32 scalar.
 * @param $output The output image in VX_DF_IMAGE_U8 type with same size as input image.
 * @return Returns VX_SUCCESS if succeed, or failure code otherwise.
 */
vx_status vxuExample(vx_context $context, vx_image $input, vx_scalar $shift, vx_image $output);

#ifdef __cplusplus
}
#endif

#endif  // IO_VISIONSDK_KERNELS_EXAMPLE_EXAMPLENODE_H_
