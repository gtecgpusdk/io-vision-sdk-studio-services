/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <gtest/gtest.h>
#include <VX/vx.h>
#include "../../../../../../../source/cpp/Io/VisionSDK/Kernels/Example/ExampleNode.h"

namespace IoVisionSDKKernelsExampleExampleNodeTest {

    void VX_CALLBACK logHandler(vx_context $context, vx_reference $ref, vx_status $status, const vx_char $string[]) {
        std::cout << "log: " << $status << "; " << $string << std::endl;
    }

    class ExampleNodeTest : public testing::Test {
     public:
        vx_context context{};
        vx_graph graph{};

        vx_status Process() {
            return vxProcessGraph(this->graph);
        }

        vx_status Validate() {
            return vxVerifyGraph(this->graph);
        }

     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();
            vxRegisterLogCallback(this->context, logHandler, _vx_bool_e::vx_false_e);

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_("Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }
    };

    TEST_F(ExampleNodeTest, Validate) {
        /// TODO: Write tests of validator implementation for vxExampleNode here.
    }

    TEST_F(ExampleNodeTest, Process) {
        /// TODO: Validate outputs of vxExampleNode implementation with expected data here.
    }

    TEST_F(ExampleNodeTest, Instant) {
        /// TODO: Validate outputs of vxuExample implementation with expected data here.
    }
}
