/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef NEWVISUALPROJECT_VISUALGRAPH_HPP_
#define NEWVISUALPROJECT_VISUALGRAPH_HPP_

#include <io-vision-sdk-studio-libs.hpp>

#include "Reference.hpp"

namespace NewVisualProject {
    /**
     * This class holds main application business logic.
     */
    class VisualGraph : public Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph {
     public:
        VisualGraph();

     protected:
        vx_status process(const std::function<vx_status(int)> &$handler) override;
    };
}

#endif  // NEWVISUALPROJECT_VISUALGRAPH_HPP_
