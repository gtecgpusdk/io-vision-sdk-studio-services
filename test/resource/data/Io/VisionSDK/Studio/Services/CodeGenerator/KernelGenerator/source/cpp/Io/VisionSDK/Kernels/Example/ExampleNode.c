/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <VX/vx_helper.h>

#include "ExampleNode.h"

vx_node vxExampleNode(vx_graph $graph, vx_image $input, vx_scalar $shift, vx_image $output) {
    vx_node node = 0;
    vx_context context = vxGetContext((vx_reference)$graph);
    vx_status status = vxLoadKernels(context, "io-vision-sdk-kernels");
    if (status == VX_SUCCESS) {
        vx_kernel kernel = vxGetKernelByName(context, "io.visionsdk.kernels.example.example");
        if (kernel) {
            node = vxCreateGenericNode($graph, kernel);
            if (vxGetStatus((vx_reference)node) == VX_SUCCESS) {
                vx_status statuses[3];
                statuses[0] = vxSetParameterByIndex(node, 0, (vx_reference)$input);
                statuses[1] = vxSetParameterByIndex(node, 1, (vx_reference)$shift);
                statuses[2] = vxSetParameterByIndex(node, 2, (vx_reference)$output);

                for (vx_uint32 i = 0; i < dimof(statuses); i++) {
                    if (statuses[i] != VX_SUCCESS) {
                        vxReleaseNode(&node);
                        vxReleaseKernel(&kernel);
                        node = 0;
                        kernel = 0;
                        break;
                    }
                }
            } else {
                vxReleaseKernel(&kernel);
            }
        } else {
            vxUnloadKernels(context, "io-vision-sdk-kernels");
        }
    }
    return node;
}

vx_status vxuExample(vx_context $context, vx_image $input, vx_scalar $shift, vx_image $output) {
    vx_status status = VX_FAILURE;
    vx_graph graph = vxCreateGraph($context);
    if (vxGetStatus((vx_reference)graph) == VX_SUCCESS) {
        vx_node node = vxExampleNode(graph, $input, $shift, $output);
        if (node) {
            status = vxVerifyGraph(graph);
            if (status == VX_SUCCESS) {
                status = vxProcessGraph(graph);
            }
            vxReleaseNode(&node);
        }
        vxReleaseGraph(&graph);
    }
    return status;
}
