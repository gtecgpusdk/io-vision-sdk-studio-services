/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef TESTCASE_1_1_CONTEXT0_HPP_
#define TESTCASE_1_1_CONTEXT0_HPP_

#include <io-vision-sdk-studio-libs.hpp>

#include "Reference.hpp"

namespace TestCase_1_1 {
    /**
     * This class overrides BaseContext methods with generated code.
     */
    class Context0 : public Io::VisionSDK::Studio::Libs::Primitives::BaseContext {
     public:
        /**
         * Constructs Context object from parent VisualGraph.
         * @param parent Specify parent VisualGraph.
         */
        explicit Context0(Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph *parent);

     protected:
        vx_status process(const std::function<vx_status()> &$handler) override;
    };
}

#endif  // TESTCASE_1_1_CONTEXT0_HPP_
