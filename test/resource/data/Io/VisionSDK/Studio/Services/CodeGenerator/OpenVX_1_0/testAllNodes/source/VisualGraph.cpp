/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "BaseGraph.hpp"
#include "Context.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    vx_status VisualGraph::create() {
        vx_status status;

        this->propertiesMap["ioCom0Path"] = std::string("../../target/resource/data/VisionSDK.png");
        this->propertiesMap["ioCom1Path"] = std::string("data/output/ioCom1.png");

        this->contextsMap["context0"] = new Context(this);
        status = this->contextsMap["context0"]->create();
        if (status != VX_SUCCESS) {
            delete this->contextsMap["context0"];
            return status;
        }

        return status;
    }

    vx_status VisualGraph::process() {
        vx_status status = VX_FAILURE;

        BaseContext *context0 = const_cast<BaseContext *>(this->getContext("context0"));
        if (context0 != nullptr) {
            status = VX_SUCCESS;
        }

        BaseGraph *graph_0_0 = nullptr;
        if (status == VX_SUCCESS) {
            graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("graph0"));
            if (graph_0_0 != nullptr) {
                status = VX_SUCCESS;
            } else {
                status = VX_FAILURE;
            }
        }

        if (status == VX_SUCCESS) {
            vx_image vxData0Ref = (vx_image)graph_0_0->getData("vxData0");
            status = VXUtils::Media::Image::Load(this->propertiesMap["ioCom0Path"].getValue<std::string>(), context0->getVxContext(), vxData0Ref , true);
        }

        if (status == VX_SUCCESS) {
            VXUtils::Media::Camera camera;
            cv::Mat mat;
            camera.Open(0);
            camera.Capture(mat);
            vx_image vxData2Ref = (vx_image)graph_0_0->getData("vxData2");
            status = VXUtils::Utils::Convert::CvToVx(mat, vxData2Ref, context0->getVxContext());
            camera.Close();
        }

        if (status == VX_SUCCESS) {
            status = context0->process();
        }

        if (status == VX_SUCCESS) {
            status = VXUtils::Media::Image::Save(this->propertiesMap["ioCom1Path"].getValue<std::string>(), context0->getVxContext(), (vx_image)graph_0_0->getData("vxData1"));
        }

        if (status == VX_SUCCESS) {
            if (!this->headless) {
                status = VXUtils::Media::Display::Show(context0->getVxContext(), (vx_image)graph_0_0->getData("vxData3"), "Display for ioCom3");
            }
        }

        this->removeContext("context0");

        return status;
    }

    bool VisualGraph::IsHeadless() const {
        return this->headless;
    }

    void VisualGraph::setHeadless(bool value) {
        this->headless = value;
    }

    const std::string &VisualGraph::getStreamDataName() const {
        return this->streamDataName;
    }

    void VisualGraph::setStreamDataName(const std::string &name) {
        this->streamDataName = name;
    }

    const BaseContext *VisualGraph::getContext(const std::string &name) const {
        if (this->contextsMap.find(name) != this->contextsMap.end()) {
            return this->contextsMap.at(name);
        }
        return nullptr;
    }

    void VisualGraph::removeContext(const std::string &name) {
        if (this->contextsMap.find(name) != this->contextsMap.end()) {
            delete this->contextsMap.at(name);
            this->contextsMap.erase(name);
        }
    }
}
