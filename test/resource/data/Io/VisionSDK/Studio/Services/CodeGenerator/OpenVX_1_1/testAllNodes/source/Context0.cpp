/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "Context0.hpp"
#include "Graph0.hpp"

namespace NewVisualProject {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;

    Context0::Context0(BaseVisualGraph *parent)
            : BaseContext(parent) {
        this->graphsMap["graph0"] = new Graph0(this);
    }

    vx_status Context0::process(const std::function<vx_status()> &$handler) {
        return BaseContext::process([&]() -> vx_status {
            vx_status status = vxGetStatus((vx_reference)this->getVxContext());
            auto graph0 = this->getGraph("graph0");
            if (status == VX_SUCCESS && graph0 != nullptr) {
                status = const_cast<BaseGraph *>(graph0)->Process();
                if (status != VX_SUCCESS) {
                    const_cast<Context0 *>(this)->removeGraph("graph0");
                }
            } else {
                status = VX_FAILURE;
            }
            return status;
        });
    }
}
