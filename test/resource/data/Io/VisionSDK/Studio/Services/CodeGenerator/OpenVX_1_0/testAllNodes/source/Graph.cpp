/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "BaseContext.hpp"
#include "BaseGraph.hpp"
#include "Graph.hpp"
#include "VisualGraph.hpp"

namespace NewVisualProject {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    Graph::Graph(BaseContext *parent)
            : BaseGraph(parent) {
    }

    vx_status Graph::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());

        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData1"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData3"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData6"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData4"]), this->getImageHeight(this->vxDataMap["vxData4"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData7"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData8"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData7"]), this->getImageHeight(this->vxDataMap["vxData7"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData9"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_uint32 vxData10Value = 0;
            this->vxDataMap["vxData10"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData10Value);
            this->vxDataMap["vxData11"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData9"]), this->getImageHeight(this->vxDataMap["vxData9"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData12"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData13Value = 0.0f;
            this->vxDataMap["vxData13"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData13Value);
            this->vxDataMap["vxData14"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData12"]), this->getImageHeight(this->vxDataMap["vxData12"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData15"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData16"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData17"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData15"]), this->getImageHeight(this->vxDataMap["vxData15"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData18"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData19"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData20"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData18"]), this->getImageHeight(this->vxDataMap["vxData18"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData21"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_int32 vxData22Value = 0;
            this->vxDataMap["vxData22"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_INT32, &vxData22Value);
            this->vxDataMap["vxData23"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData21"]), this->getImageHeight(this->vxDataMap["vxData21"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData24"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData25"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData26"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData24"]), this->getImageHeight(this->vxDataMap["vxData24"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData27"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData28"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData29"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData27"]), this->getImageHeight(this->vxDataMap["vxData27"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData30"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData31"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData32"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData30"]), this->getImageHeight(this->vxDataMap["vxData30"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData33"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData34"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData33"]), this->getImageHeight(this->vxDataMap["vxData33"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData35"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData36"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData35"]), this->getImageHeight(this->vxDataMap["vxData35"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData37"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_uint8 vxData38Value = 140;
            this->vxDataMap["vxData38"] = (vx_reference)vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);
            vxSetThresholdAttribute((vx_threshold)this->vxDataMap["vxData38"], VX_THRESHOLD_ATTRIBUTE_THRESHOLD_VALUE, &vxData38Value, sizeof(vxData38Value));
            this->vxDataMap["vxData39"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData37"]), this->getImageHeight(this->vxDataMap["vxData37"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData40"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData41"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData42"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData43"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData44"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData40"]), this->getImageHeight(this->vxDataMap["vxData40"]), VX_DF_IMAGE_NV12);
            this->vxDataMap["vxData45"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_RGB);
            this->vxDataMap["vxData46"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData45"]), this->getImageHeight(this->vxDataMap["vxData45"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData47"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_RGB);
            this->vxDataMap["vxData48"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData47"]), this->getImageHeight(this->vxDataMap["vxData47"]), VX_DF_IMAGE_RGB);
            this->vxDataMap["vxData49"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_int16 vxData50Value[3][3] = {
                    {1, 0, -1},
                    {3, 0, -3},
                    {1, 0, -1}
            };
            this->vxDataMap["vxData50"] = (vx_reference)vxCreateConvolution(this->getParent()->getVxContext(), 3, 3);
            vxWriteConvolutionCoefficients((vx_convolution)this->vxDataMap["vxData50"], &vxData50Value[0][0]);
            this->vxDataMap["vxData51"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData49"]), this->getImageHeight(this->vxDataMap["vxData49"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData52"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData53"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData52"]), this->getImageHeight(this->vxDataMap["vxData52"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData54"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData55"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData54"]), this->getImageHeight(this->vxDataMap["vxData54"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData56"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData57"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData56"]), this->getImageHeight(this->vxDataMap["vxData56"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData58"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData59Value = 10000.000f;
            this->vxDataMap["vxData59"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData59Value);
            this->vxDataMap["vxData60"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            vx_size vxData61Value = 0;
            this->vxDataMap["vxData61"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_SIZE, &vxData61Value);
            this->vxDataMap["vxData62"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData63"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData62"]), this->getImageHeight(this->vxDataMap["vxData62"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData64"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData65"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 4, VX_SCALE_PYRAMID_HALF, 310, 310, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData66"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData67Value = 10000.000f;
            this->vxDataMap["vxData67"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData67Value);
            vx_float32 vxData68Value = 2.000f;
            this->vxDataMap["vxData68"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData68Value);
            vx_float32 vxData69Value = 0.150f;
            this->vxDataMap["vxData69"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData69Value);
            this->vxDataMap["vxData70"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            vx_size vxData71Value = 0;
            this->vxDataMap["vxData71"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_SIZE, &vxData71Value);
            this->vxDataMap["vxData72"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData73"] = (vx_reference)vxCreateDistribution(this->getParent()->getVxContext(), 16, 0, 256);
            this->vxDataMap["vxData74"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData75"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData74"]), this->getImageHeight(this->vxDataMap["vxData74"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData76"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData77"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData76"]), this->getImageHeight(this->vxDataMap["vxData76"]), VX_DF_IMAGE_U32);
            this->vxDataMap["vxData78"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            this->vxDataMap["vxData79"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            this->vxDataMap["vxData80"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData78"]), this->getImageHeight(this->vxDataMap["vxData78"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData81"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData82Value = 0.0f;
            this->vxDataMap["vxData82"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData82Value);
            vx_float32 vxData83Value = 0.0f;
            this->vxDataMap["vxData83"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData83Value);
            this->vxDataMap["vxData84"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData85"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData84"]), this->getImageHeight(this->vxDataMap["vxData84"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData86"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_uint8 vxData87Value = 0;
            this->vxDataMap["vxData87"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT8, &vxData87Value);
            vx_uint8 vxData88Value = 0;
            this->vxDataMap["vxData88"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT8, &vxData88Value);
            this->vxDataMap["vxData89"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_COORDINATES2D, 1000);
            this->vxDataMap["vxData90"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_COORDINATES2D, 1000);
            vx_uint32 vxData91Value = 0;
            this->vxDataMap["vxData91"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData91Value);
            vx_uint32 vxData92Value = 0;
            this->vxDataMap["vxData92"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData92Value);
            this->vxDataMap["vxData93"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 1, VX_SCALE_PYRAMID_HALF, 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData94"] = (vx_reference)vxCreatePyramid(this->getParent()->getVxContext(), 1, VX_SCALE_PYRAMID_HALF, 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData95"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            this->vxDataMap["vxData96"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            vx_float32 vxData97Value = 0.0f;
            this->vxDataMap["vxData97"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData97Value);
            vx_uint32 vxData98Value = 0;
            this->vxDataMap["vxData98"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData98Value);
            vx_bool vxData99Value = (vx_bool)false;
            this->vxDataMap["vxData99"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_BOOL, &vxData99Value);
            vx_uint32 vxData100Value = 0;
            this->vxDataMap["vxData100"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData100Value);
            this->vxDataMap["vxData101"] = (vx_reference)vxCreateArray(this->getParent()->getVxContext(), VX_TYPE_KEYPOINT, 1000);
            this->vxDataMap["vxData102"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            this->vxDataMap["vxData103"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_S16);
            this->vxDataMap["vxData104"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData102"]), this->getImageHeight(this->vxDataMap["vxData102"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData105"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData106"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData107Value = 0.0f;
            this->vxDataMap["vxData107"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData107Value);
            this->vxDataMap["vxData108"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData105"]), this->getImageHeight(this->vxDataMap["vxData105"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData109"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData110"] = (vx_reference)vxCreateRemap(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData109"]), this->getImageHeight(this->vxDataMap["vxData109"]), 310, 310);
            this->vxDataMap["vxData111"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData112"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData113"] = this->createImage(this->getParent()->getVxContext(), 310, 310, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData114"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            this->vxDataMap["vxData115"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData114"]), this->getImageHeight(this->vxDataMap["vxData114"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData116"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData114"]), this->getImageHeight(this->vxDataMap["vxData114"]), VX_DF_IMAGE_S16);
            this->vxDataMap["vxData117"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_uint8 *vxData118Value = nullptr;
            std::vector<vx_uint8> vxData118Data = {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
                101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
                121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
                141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
                161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
                181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200,
                201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
                221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,
                241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255
            };
            this->vxDataMap["vxData118"] = (vx_reference)vxCreateLUT(this->getParent()->getVxContext(), VX_TYPE_UINT8, 256);
            status = vxAccessLUT((vx_lut) this->vxDataMap["vxData118"], (void **) &vxData118Value, VX_WRITE_ONLY);
            if (status == VX_SUCCESS && vxData118Data.size() == 256) {
                for (int i = 0; i < 256; i++) {
                    vxData118Value[i] = vxData118Data[i];
                }
                status = vxCommitLUT((vx_lut)this->vxDataMap["vxData118"], vxData118Value);
            }
            vxTableLookupNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_lut)this->vxDataMap["vxData118"], (vx_image)this->vxDataMap["vxData2"]);
            this->vxDataMap["vxData119"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData117"]), this->getImageHeight(this->vxDataMap["vxData117"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData120"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_uint8 vxData121Value = 140;
            this->vxDataMap["vxData121"] = (vx_reference)vxCreateThreshold(this->getParent()->getVxContext(), VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);
            vxSetThresholdAttribute((vx_threshold)this->vxDataMap["vxData121"], VX_THRESHOLD_ATTRIBUTE_THRESHOLD_VALUE, &vxData121Value, sizeof(vxData121Value));
            this->vxDataMap["vxData122"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData120"]), this->getImageHeight(this->vxDataMap["vxData120"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData123"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData124Value[3][2] = {
                    {1.000f, 1.000f},
                    {2.000f, 2.000f},
                    {0.500f, 0.500f}
            };
            this->vxDataMap["vxData124"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 2, 3);
            vxWriteMatrix((vx_matrix) this->vxDataMap["vxData124"], vxData124Value);            this->vxDataMap["vxData125"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData123"]), this->getImageHeight(this->vxDataMap["vxData123"]), VX_DF_IMAGE_U8);
            this->vxDataMap["vxData126"] = this->createImage(this->getParent()->getVxContext(), 0, 0, VX_DF_IMAGE_U8);
            vx_float32 vxData127Value[3][3] = {
                    {1.000f, 1.000f, 1.000f},
                    {2.000f, 2.000f, 2.000f},
                    {0.500f, 0.500f, 0.500f}
            };
            this->vxDataMap["vxData127"] = (vx_reference)vxCreateMatrix(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, 3, 3);
            vxWriteMatrix((vx_matrix) this->vxDataMap["vxData127"], vxData127Value);            this->vxDataMap["vxData128"] = this->createImage(this->getParent()->getVxContext(), this->getImageWidth(this->vxDataMap["vxData126"]), this->getImageHeight(this->vxDataMap["vxData126"]), VX_DF_IMAGE_U8);

            vxAbsDiffNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData4"], (vx_image)this->vxDataMap["vxData5"], (vx_image)this->vxDataMap["vxData6"]);
            vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData7"], (vx_image)this->vxDataMap["vxData8"]);
            vxAccumulateSquareImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData9"], (vx_scalar)this->vxDataMap["vxData10"], (vx_image)this->vxDataMap["vxData11"]);
            vxAccumulateWeightedImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData12"], (vx_scalar)this->vxDataMap["vxData13"], (vx_image)this->vxDataMap["vxData14"]);
            vxAddNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData15"], (vx_image)this->vxDataMap["vxData16"], VX_CONVERT_POLICY_SATURATE, (vx_image)this->vxDataMap["vxData17"]);
            vxSubtractNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData18"], (vx_image)this->vxDataMap["vxData19"], VX_CONVERT_POLICY_SATURATE, (vx_image)this->vxDataMap["vxData20"]);
            vxConvertDepthNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData21"], (vx_image)this->vxDataMap["vxData23"], VX_CONVERT_POLICY_SATURATE, (vx_scalar)this->vxDataMap["vxData22"]);
            vxAndNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData24"], (vx_image)this->vxDataMap["vxData25"], (vx_image)this->vxDataMap["vxData26"]);
            vxXorNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData27"], (vx_image)this->vxDataMap["vxData28"], (vx_image)this->vxDataMap["vxData29"]);
            vxOrNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData30"], (vx_image)this->vxDataMap["vxData31"], (vx_image)this->vxDataMap["vxData32"]);
            vxNotNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData33"], (vx_image)this->vxDataMap["vxData34"]);
            vxBox3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData35"], (vx_image)this->vxDataMap["vxData36"]);
            vxCannyEdgeDetectorNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData37"], (vx_threshold)this->vxDataMap["vxData38"], (vx_int32)3, VX_NORM_L1, (vx_image)this->vxDataMap["vxData39"]);
            vxChannelCombineNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData40"], (vx_image)this->vxDataMap["vxData41"], (vx_image)this->vxDataMap["vxData42"], (vx_image)this->vxDataMap["vxData43"], (vx_image)this->vxDataMap["vxData44"]);
            vxChannelExtractNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData45"], VX_CHANNEL_0, (vx_image)this->vxDataMap["vxData46"]);
            vxColorConvertNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData47"], (vx_image)this->vxDataMap["vxData48"]);
            vxConvolveNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData49"], (vx_convolution)this->vxDataMap["vxData50"], (vx_image)this->vxDataMap["vxData51"]);
            vxDilate3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData52"], (vx_image)this->vxDataMap["vxData53"]);
            vxEqualizeHistNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData54"], (vx_image)this->vxDataMap["vxData55"]);
            vxErode3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData56"], (vx_image)this->vxDataMap["vxData57"]);
            vxFastCornersNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData58"], (vx_scalar)this->vxDataMap["vxData59"],
                                            (vx_bool)false, (vx_array)this->vxDataMap["vxData60"],
                                            (vx_scalar)this->vxDataMap["vxData61"]);
            vxGaussian3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData62"], (vx_image)this->vxDataMap["vxData63"]);
            vxGaussianPyramidNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData64"], (vx_pyramid)this->vxDataMap["vxData65"]);
            vxHarrisCornersNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData66"], (vx_scalar)this->vxDataMap["vxData67"],
                                            (vx_scalar)this->vxDataMap["vxData68"], (vx_scalar)this->vxDataMap["vxData69"], 5, 3,
                                            (vx_array)this->vxDataMap["vxData70"], (vx_scalar)this->vxDataMap["vxData71"]);
            vxHistogramNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData72"], (vx_distribution)this->vxDataMap["vxData73"]);
            vxHalfScaleGaussianNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData74"], (vx_image)this->vxDataMap["vxData75"], (vx_int32)3);
            vxIntegralImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData76"], (vx_image)this->vxDataMap["vxData77"]);
            vxMagnitudeNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData78"], (vx_image)this->vxDataMap["vxData79"], (vx_image)this->vxDataMap["vxData80"]);
            vxMeanStdDevNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData81"], (vx_scalar)this->vxDataMap["vxData82"], (vx_scalar)this->vxDataMap["vxData83"]);
            vxMedian3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData84"], (vx_image)this->vxDataMap["vxData85"]);
            vxMinMaxLocNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData86"], (vx_scalar)this->vxDataMap["vxData87"], (vx_scalar)this->vxDataMap["vxData88"], (vx_array)this->vxDataMap["vxData89"], (vx_array)this->vxDataMap["vxData90"], (vx_scalar)this->vxDataMap["vxData91"], (vx_scalar)this->vxDataMap["vxData92"]);
            vxOpticalFlowPyrLKNode(this->getVxGraph(), (vx_pyramid)this->vxDataMap["vxData93"], (vx_pyramid)this->vxDataMap["vxData94"], (vx_array)this->vxDataMap["vxData95"], (vx_array)this->vxDataMap["vxData96"], (vx_array)this->vxDataMap["vxData101"], VX_TERM_CRITERIA_ITERATIONS, (vx_scalar)this->vxDataMap["vxData97"], (vx_scalar)this->vxDataMap["vxData98"], (vx_scalar)this->vxDataMap["vxData99"], (vx_size)this->vxDataMap["vxData100"]);
            vxPhaseNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData102"], (vx_image)this->vxDataMap["vxData103"], (vx_image)this->vxDataMap["vxData104"]);
            vxMultiplyNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData105"], (vx_image)this->vxDataMap["vxData106"], (vx_scalar)this->vxDataMap["vxData107"], VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_ZERO, (vx_image)this->vxDataMap["vxData108"]);
            vxRemapNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData109"], (vx_remap)this->vxDataMap["vxData110"], VX_INTERPOLATION_TYPE_BILINEAR, (vx_image)this->vxDataMap["vxData111"]);
            vxScaleImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData112"], (vx_image)this->vxDataMap["vxData113"], VX_INTERPOLATION_TYPE_NEAREST_NEIGHBOR);
            vxSobel3x3Node(this->getVxGraph(), (vx_image)this->vxDataMap["vxData114"], (vx_image)this->vxDataMap["vxData115"], (vx_image)this->vxDataMap["vxData116"]);
            vxTableLookupNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData117"], (vx_lut)this->vxDataMap["vxData118"], (vx_image)this->vxDataMap["vxData119"]);
            vxThresholdNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData120"], (vx_threshold)this->vxDataMap["vxData121"], (vx_image)this->vxDataMap["vxData122"]);
            vxWarpAffineNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData123"], (vx_matrix)this->vxDataMap["vxData124"], VX_INTERPOLATION_TYPE_BILINEAR, (vx_image)this->vxDataMap["vxData125"]);
            vxWarpPerspectiveNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData126"], (vx_matrix)this->vxDataMap["vxData127"], VX_INTERPOLATION_TYPE_BILINEAR, (vx_image)this->vxDataMap["vxData128"]);

            status = vxVerifyGraph(this->getVxGraph());
        }

        return status;
    }

    vx_status Graph::process() const {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());

        if (status == VX_SUCCESS) {
            status = vxProcessGraph(this->getVxGraph());
        }

        if (this->getParent()->getParent()->IsHeadless() && !this->getParent()->getParent()->getStreamDataName().empty()) {
            VXUtils::Streaming::VxImageStreamer::SendQuery(this->getParent()->getVxContext(), (vx_image)this->getData(this->getParent()->getParent()->getStreamDataName()));
        }

        return status;
    }
}
