Release Name: io-vision-sdk-studio-services v2019.1.0

io-vision-sdk-studio-services
Description: WUI Framework library focused on services and business logic for Vision SDK Studio.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: [target/]resource/css/[package-name]-[version].min.css,
          [target/]resource/data/Io/VisionSDK/Studio/Services,
          [target/]resource/data/VisionSDK.png,
          [target/]resource/graphics/SelfExtractor,
          [target/]resource/javascript/[package-name]-[version].d.ts,
          [target/]resource/javascript/[package-name]-[version].min.js,
          [target/]resource/javascript/[package-name]-[version].min.js.map,
          [target/]test/resource/data/Io/VisionSDK/Studio/Services,
          [target/]index.html,
          [target/]wuirunner.config.jsonp,
          [application-name].config.jsonp

io-vision-sdk-usercontrols v2019.1.0
Description: WUI Framework library focused on user controls with design for Vision SDK.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Io/VisionSDK/UserControls,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          index.html

Lato Font N/A
Description: Web font in ttf format
Author: Łukasz Dziedzic
License: OFL-1.1, See OFL.txt
Format: font
Location: resource/libs/FontLato

io-vision-sdk-studio-gui v2019.1.0
Description: WUI Framework library focused on GUI with design for Vision SDK Studio.
Author: FSL
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/VisionSDK.ico,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/graphics/Io/VisionSDK/Studio/Gui,
          index.html

com-wui-framework-commons v2019.1.0
Description: Commons library for WUI Framework front-end projects.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Commons,
          resource/graphics/icon.ico,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          resource/javascript/loader.min.js,
          test/resource/data/Com/Wui/Framework/Commons,
          index.html

com-wui-framework-gui v2019.1.0
Description: WUI Framework base GUI library
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Gui,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/data/Com/Wui/Framework/Gui,
          test/resource/graphics/Com/Wui/Framework/Gui,
          index.html

Oxygen Font N/A
Description: Web font in ttf format
Author: Vernon Adams
License: OFL-1.1, See OFL.txt
Format: font
Location: resource/libs/FontOxygen

com-wui-framework-services v2019.1.0
Description: WUI Framework library focused on services and business logic for applications build on WUI Framework
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: [target/]resource/css/[package-name]-[version].min.css,
          [target/]resource/data/Com/Wui/Framework/Services,
          [target/]resource/javascript/[package-name]-[version].d.ts,
          [target/]resource/javascript/[package-name]-[version].min.js,
          [target/]resource/javascript/[package-name]-[version].min.js.map,
          [target/]test/resource/data/Com/Wui/Framework/Services,
          [target/]index.html,
          [target/]wuirunner.config.jsonp,
          [application-name].config.jsonp

[OPTIONAL]
com-wui-framework-selfextractor v2019.1.0
Description: SelfExtractor for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary
Location: [application-name][.exe]

[OPTIONAL]
com-wui-framework-launcher v2018.0.0
Description: Launcher for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary
Location: [application-name][.exe]

[OPTIONAL]
com-wui-framework-connector v2019.1.0
Description: Stand alone HTTP server for WUI Framework's applications with WebSockets support and built-in web host
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary and embedded resources
Location: target/resource/libs/WuiConnector/[application-name] Connector[.exe],
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/package.json,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/css/[package-name]-[version].min.css,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/graphics/icon.ico,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].d.ts,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].min.js,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].min.js.map,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/loader.min.js,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/test/resource/data/Com/Wui/Framework/Connector

[OPTIONAL]
com-wui-framework-chromiumre v2019.0.0
Description: Stand alone runtime environment for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: tool
Location: wuichromiumre

[OPTIONAL]
com-wui-framework-idejre v1.2.0
Description: WUI Framework runtime environment for Java-based IDEs
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: java package
Location: [package-name]-[version].jar

com-wui-framework-usercontrols v2019.1.0
Description: WUI Framework library focused on basic User Controls
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/UserControls,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/graphics/Com/Wui/Framework/UserControls,
          index.html

io-vision-sdk-studio-libs v2019.1.0
Description: OpenVX code generator back-end for Vision SDK Studio.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code and libraries
Location: resource/libs/VisionSDK/[platform]/libs/VXUtils

openvx v1.1
Description: An open, royalty-free standard API for cross platform acceleration of computer vision applications.
Author: The Khronos Group Inc.
License: MIT. See LICENSE
Format: compiled source code and header files
Location: resource/libs/VisionSDK/[platform]/bins,
          resource/libs/VisionSDK/[platform]/libs/openvx

opencv v2.4.13.4
Description: Library of programming functions mainly aimed at real-time computer vision.
Author: Intel Corporation, Willow Garage, Itseez
License: BSD-3-Clause. See LICENSE
Format: compiled source code and header files
Location: resource/libs/VisionSDK/[platform]/bins,
          resource/libs/VisionSDK/[platform]/libs/opencv

nlohmann-json v3.0.1
Description: Library for processing of JSON data in C++
Author: Niels Lohmann
License: MIT License. See LICENSE.MIT
Format: source code
Location: resource/libs/VisionSDK/[platform]/libs/nlohmann

any N/A
Description: Container for any type
Author: Mercês Amorim
License: Boost Software License, Version 1.0
Format: header file
Location: resource/libs/VisionSDK/[platform]/libs/VxUtils/include/Io/VisionSDK/Studio/Libs/VXUtils/Primitives/Any.hpp

com-wui-framework-xcpplint v2019.1.0
Description: Lint tool for Cpp projects, which requires WUI Framework coding standards
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/data/Com/Wui/Framework/XCppLint,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          resource/javascript/loader.min.js,
          test/resource/data/Com/Wui/Framework/XCppLint
