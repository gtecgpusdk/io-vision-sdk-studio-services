/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    version: "1.0.0",
    $interface: "ICodeGeneratorTemplates",
    vxAPI: {
        vxContext: {
            help: {
                name: "Context",
                methodName: "vxCreateContext",
                tooltip: "Defines the Context Object Interface",
                link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d1/dc3/group__group__context.html",
                description: "" +
                    "The OpenVX context is the object domain for all OpenVX objects. All data objects live in the context as well as all " +
                    "framework objects. The OpenVX context keeps reference counts on all objects and must do garbage collection during " +
                    "its deconstruction to free lost references. While multiple clients may connect to the OpenVX context, all data are " +
                    "private in that the references referring to data objects are given only to the creating party."
            }
        },
        vxGraph: {
            help: {
                name: "Graph",
                methodName: "vxCreateGraph",
                tooltip: "Defines the Graph Object Interface",
                link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d9/d7f/group__group__graph.html",
                description: "" +
                    "A set of nodes connected in a directed (only goes one-way) acyclic (does not loop back) fashion. " +
                    "A Graph may have sets of Nodes that are unconnected to other sets of Nodes within the same Graph."
            }
        },
        vxIoComs: {
            IMAGE_INPUT: {
                help: {
                    tooltip: "Read image file from file system."
                }
            },
            IMAGE_OUTPUT: {
                help: {
                    tooltip: "Write image file to file system."
                }
            },
            VIDEO_INPUT: {
                help: {
                    tooltip: "Read video file from file system."
                }
            },
            VIDEO_OUTPUT: {
                help: {
                    tooltip: "Write video file to file system."
                }
            },
            CAMERA: {
                help: {
                    tooltip: "Input a video stream from a camera device."
                }
            },
            DISPLAY: {
                help: {
                    tooltip: "Display an image or video to screen, but do not save output."
                }
            }
        },
        vxNodes: {
            VX_ABSOLUTE_DIFFERENCE: {
                help: {
                    tooltip: "Computes the absolute difference between two images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dc/ddf/group__group__vision__function__absdiff.html",
                    description: "" +
                        "Computes the absolute difference between two images.</br>" +
                        "</br>" +
                        "Absolute Difference is computed by:</br>" +
                        "</br>" +
                        "<b>out(x,y) = |in1(x,y)−in2(x,y)|</b></br>" +
                        "</br>" +
                        "When the two input parameters have type s16, the conceptual definition describing the overflow is:</br>" +
                        "</br>" +
                        "uint16 uresult = (uint16) abs((int32) (a) - (int32) (b));</br>" +
                        "</br>" +
                        "int16 result = uresult > 32767 ? 32767 : (int16) uresult;"
                }
            },
            VX_ACCUMULATE: {
                help: {
                    tooltip: "Accumulates an input image into output image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d0/d77/group__group__vision__function__accumulate.html",
                    description: "" +
                        "Accumulates an input image into output image.</br>" +
                        "</br>" +
                        "Accumulation is computed by:</br>" +
                        "</br>" +
                        "<b>accum(x,y) = accum(x,y) + input(x,y)</b></br>" +
                        "</br>" +
                        "The overflow policy used is VX_CONVERT_POLICY_SATURATE."
                }
            },
            VX_ACCUMULATE_SQUARED: {
                help: {
                    tooltip: "Accumulates a squared value from an input image to an output image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d0/d2c/group__group__vision__function__accumulate__square.html",
                    description: "" +
                        "Accumulates a squared value from an input image to an output image.</br>" +
                        "</br>" +
                        "Accumulate squares is computed by:</br>" +
                        "</br>" +
                        "<b>accum(x,y)=saturateint16((uint16)accum(x,y)+(((uint16)input(x,y)2)>>(shift)))</b></br>" +
                        "</br>" +
                        "Where <b>0 ≤ shift ≤ 15</b></br>" +
                        "</br>" +
                        "The overflow policy used is VX_CONVERT_POLICY_SATURATE."
                }
            },
            VX_ACCUMULATE_WEIGHTED: {
                help: {
                    tooltip: "Accumulates a weighted value from an input image to an output image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d2/d2e/group__group__vision__function__accumulate__weighted.html",
                    description: "" +
                        "Accumulates a weighted value from an input image to an output image.</br>" +
                        "</br>" +
                        "Weighted accumulation is computed by:</br>" +
                        "</br>" +
                        "<b>accum(x,y)=(1−α)∗accum(x,y)+α∗input(x,y)</b></br>" +
                        "Where <b>0≤α≤1</b> Conceptually, the rounding for this is defined as:</br>" +
                        "</br>" +
                        "<b>output(x,y) = uint8((1−α)∗float32(int32(output(x,y)))+</br>α∗float32(int32(input(x,y))))</b>"
                }
            },
            VX_ARITHMETIC_ADDITION: {
                help: {
                    tooltip: "Performs addition between two images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d6/db0/group__group__vision__function__add.html",
                    description: "" +
                        "Performs addition between two images.</br>" +
                        "</br>" +
                        "Arithmetic addition is performed between the pixel values in two VX_DF_IMAGE_U8 or VX_DF_IMAGE_S16 images. " +
                        "The output image can be VX_DF_IMAGE_U8 only if both source images are VX_DF_IMAGE_U8 and the output image is " +
                        "explicitly set to VX_DF_IMAGE_U8. It is otherwise VX_DF_IMAGE_S16. If one of the input images is of type " +
                        "VX_DF_IMAGE_S16, all values are converted to VX_DF_IMAGE_S16. The overflow handling is controlled by an " +
                        "overflow-policy parameter. For each pixel value in the two input images:</br>" +
                        "</br>" +
                        "<b>out(x,y) = in1(x,y) + in2(x,y)</b>"
                }
            },
            VX_ARITHMETIC_SUBTRACTION: {
                help: {
                    tooltip: "Performs subtraction between two images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d6/d6d/group__group__vision__function__sub.html",
                    description: "" +
                        "Performs subtraction between two images.</br>" +
                        "</br>" +
                        "Arithmetic subtraction is performed between the pixel values in two VX_DF_IMAGE_U8 or two " +
                        "VX_DF_IMA←GE_S16 images. The output image can be VX_DF_IMAGE_U8 only if both source images are " +
                        "VX_DF_IMAGE←U8 and the output image is explicitly set to VX_DF_IMAGE_U8. It is otherwise VX_DF_IMAGE_S16. If one of" +
                        "the input images is of type VX_DF_IMAGE_S16, all values are converted to VX_DF_IMAGE_S16. The overflow" +
                        "handling is controlled by an overflow-policy parameter. For each pixel value in the two input images:</br>" +
                        "</br>" +
                        "<b>out(x, y) = in1(x, y)−in2(x, y)</b>"
                }
            },
            VX_BIT_DEPTH_CONVERT: {
                help: {
                    tooltip: "Converts image bit depth.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/de/d73/group__group__vision__function__convertdepth.html",
                    description: "" +
                        "Converts image bit depth.</br>" +
                        "</br>" +
                        "This kernel converts an image from some source bit-depth to another bit-depth. " +
                        "If the input value is unsigned the shift must be in zeros. If the input value is signed, the shift used must be an " +
                        "arithmetic shift. The columns in the table below are the output types and the rows are the input types. The API " +
                        "version on which conversion is supported is also listed. (An X denotes an invalid operation.)"
                }
            },
            VX_BITWISE_AND: {
                help: {
                    tooltip: "Performs a bitwise AND operation between two VX_DF_IMAGE_U8 images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d4/d4e/group__group__vision__function__and.html",
                    description: "" +
                        "Performs a bitwise AND operation between two VX_DF_IMAGE_U8 images.</br>" +
                        "</br>" +
                        "Bitwise AND is computed by the following, for each bit in each pixel in the input images:</br>" +
                        "</br>" +
                        "<b>out(x, y) = in1(x, y)∧in2(x, y)</b></br>" +
                        "</br>" +
                        "Or expressed as C code:</br>" +
                        "</br>" +
                        "<b>out(x,y) = in_1(x,y) & in_2(x,y)</b>"
                }
            },
            VX_BITWISE_EXCLUSIVE_OR: {
                help: {
                    tooltip: "Performs a bitwise EXCLUSIVE OR (XOR) operation between two VX_DF_IMAGE_U8 images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d3/dd1/group__group__vision__function__xor.html",
                    description: "" +
                        "Performs a bitwise EXCLUSIVE OR (XOR) operation between two VX_DF_IMAGE_U8 images.</br>" +
                        "</br>" +
                        "Bitwise XOR is computed by the following, for each bit in each pixel in the input images:</br>" +
                        "</br>" +
                        "<b>out(x, y) = in1(x, y)(+)in2(x, y)</b></br>" +
                        "</br>" +
                        "Or expressed as C code:</br>" +
                        "</br>" +
                        "<b>out(x,y) = in_1(x,y) ^ in_2(x,y)</b>"
                }
            },
            VX_BITWISE_INCLUSIVE_OR: {
                help: {
                    tooltip: "Performs a bitwise INCLUSIVE OR operation between two VX_DF_IMAGE_U8 images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d2/d5d/group__group__vision__function__or.html",
                    description: "" +
                        "Performs a bitwise INCLUSIVE OR operation between two VX_DF_IMAGE_U8 images.</br>" +
                        "</br>" +
                        "Bitwise INCLUSIVE OR is computed by the following, for each bit in each pixel in the input images:</br>" +
                        "</br>" +
                        "<b>out(x, y) = in1(x, y)∨in2(x, y)</b></br>" +
                        "</br>" +
                        "Or expressed as C code:</br>" +
                        "</br>" +
                        "<b>out(x,y) = in_1(x,y) | in_2(x,y)</b>"
                }
            },
            VX_BITWISE_NOT: {
                help: {
                    tooltip: "Performs a bitwise NOT operation on a VX_DF_IMAGE_U8 input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dc/d06/group__group__vision__function__not.html",
                    description: "" +
                        "Performs a bitwise NOT operation on a VX_DF_IMAGE_U8 input image.</br>" +
                        "</br>" +
                        "Bitwise NOT is computed by the following, for each bit in each pixel in the input image:</br>" +
                        "</br>" +
                        "<b>out(x, y) = in(x, y)</b></br>" +
                        "</br>" +
                        "Or expressed as C code:</br>" +
                        "</br>" +
                        "<b>out(x,y) = ~in_1(x,y)</b>"
                }
            },
            VX_BOX_FILTER: {
                help: {
                    tooltip: "Computes a Box filter over a window of the input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/da/d7c/group__group__vision__function__box__image.html",
                    description: "" +
                        "Computes a Box filter over a window of the input image."
                }
            },
            VX_CANNY_EDGE_DETECTOR: {
                help: {
                    tooltip: "Provides a Canny edge detector kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d7/d71/group__group__vision__function__canny.html",
                    description: "" +
                        "Provides a Canny edge detector kernel.</br>" +
                        "</br>" +
                        "This function implements an edge detection algorithm similar to that described in [2]. The main components of " +
                        "the algorithm are:</br>" +
                        "</br>" +
                        "• Gradient magnitude and orientation computation using a noise resistant operator (Sobel).</br>" +
                        "• Non-maximum suppression of the gradient magnitude, using the gradient orientation information.</br>" +
                        "• Tracing edges in the modified gradient image using hysteresis thresholding to produce a binary result"
                }
            },
            VX_CHANNEL_COMBINE: {
                help: {
                    tooltip: "Implements the Channel Combine Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/de/df2/group__group__vision__function__channelcombine.html",
                    description: "" +
                        "Implements the Channel Combine Kernel.</br>" +
                        "</br>" +
                        "This kernel takes multiple VX_DF_IMAGE_U8 planes to recombine them into a multi-planar or interleaved " +
                        "format from vx_df_image_e. The user must specify only the number of channels that are appropriate for the " +
                        "combining operation. If a user specifies more channels than necessary, the operation results in an error. For the " +
                        "case where the destination image is a format with subsampling, the input channels are expected to have been " +
                        "subsampled before combining (by stretching and resizing)."
                }
            },
            VX_CHANNEL_EXTRACT: {
                help: {
                    tooltip: "Implements the Channel Extraction Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dd/dc1/group__group__vision__function__channelextract.html",
                    description: "" +
                        "Implements the Channel Extraction Kernel.</br>" +
                        "</br>" +
                        "This kernel removes a single VX_DF_IMAGE_U8 channel (plane) from a multi-planar or interleaved image " +
                        "format from vx_df_image_e."
                }
            },
            VX_COLOR_CONVERT: {
                help: {
                    tooltip: "Implements the Color Conversion Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d1/dc2/group__group__vision__function__colorconvert.html",
                    description: "" +
                        "Implements the Color Conversion Kernel.</br>" +
                        "</br>" +
                        "This kernel converts an image of a designated vx_df_image_e format to another vx_df_image_e format " +
                        "for those combinations listed in the below table, where the columns are output types and the rows are input types."
                }
            },
            VX_CUSTOM_CONVOLUTION: {
                help: {
                    tooltip: "Convolves the input with the client supplied convolution matrix.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d3/d3b/group__group__vision__function__custom__convolution.html",
                    description: "" +
                        "Convolves the input with the client supplied convolution matrix.</br>" +
                        "</br>" +
                        "The client can supply a vx_int16 typed convolution matrix Cm,n. Outputs will be in the VX_DF_IMAGE_S16 " +
                        "format unless a VX_DF_IMAGE_U8 image is explicitly provided. If values would have been out of range of U8 for " +
                        "VX_DF_IMAGE_U8, the values are clamped to 0 or 255."
                }
            },
            VX_DILATE_IMAGE: {
                help: {
                    tooltip: "Implements Dilation, which grows the white space in a VX_DF_IMAGE_U8 Boolean image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dc/d73/group__group__vision__function__dilate__image.html",
                    description: "" +
                        "Implements Dilation, which grows the white space in a VX_DF_IMAGE_U8 Boolean image.</br>" +
                        "</br>" +
                        "This kernel uses a 3x3 box around the output pixel used to determine value."
                }
            },
            VX_EQUALIZE_HISTOGRAM: {
                help: {
                    tooltip: "Equalizes the histogram of a grayscale image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d1/d70/group__group__vision__function__equalize__hist.html",
                    description: "" +
                        "Equalizes the histogram of a grayscale image.</br>" +
                        "</br>" +
                        "This kernel uses Histogram Equalization to modify the values of a grayscale image so that it will automatically have " +
                        "a standardized brightness and contrast."
                }
            },
            VX_ERODE_IMAGE: {
                help: {
                    tooltip: "Implements Erosion, which shrinks the white space in a VX_DF_IMAGE_U8 Boolean image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dc/dff/group__group__vision__function__erode__image.html",
                    description: "" +
                        "Implements Erosion, which shrinks the white space in a VX_DF_IMAGE_U8 Boolean image.</br>" +
                        "</br>" +
                        "This kernel uses a 3x3 box around the output pixel used to determine value."
                }
            },
            VX_FAST_CORNER: {
                help: {
                    tooltip: "Computes the corners in an image using a method based upon FAST9 algorithm.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dd/d22/group__group__vision__function__fast.html",
                    description: "" +
                        "Computes the corners in an image using a method based upon FAST9 algorithm with modifications described below. " +
                        "It extracts corners by evaluating pixels on the Bresenham circle around a candidate point. If N contiguous pixels " +
                        "are brighter than the candidate point by at least a threshold value t or darker by at least t , then the candidate " +
                        "point is considered to be a corner. For each detected corner, its strength is computed. Optionally, a non-maxima " +
                        "suppression step is applied on all detected corners to remove multiple or spurious responses"
                }
            },
            VX_GAUSSIAN_BLUR: {
                help: {
                    tooltip: "Computes a Gaussian filter over a window of the input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d6/d58/group__group__vision__function__gaussian__image.html",
                    description: "" +
                        "Computes a Gaussian filter over a window of the input image."
                }
            },
            VX_GAUSSIAN_IMAGE_PYRAMID: {
                help: {
                    tooltip: "Computes a Gaussian Image Pyramid from an input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d0/d15/group__group__vision__function__gaussian__pyramid.html",
                    description: "" +
                        "Computes a Gaussian Image Pyramid from an input image.</br>" +
                        "</br>" +
                        "This vision function creates the Gaussian image pyramid from the input image using the particular 5x5 Gaussian " +
                        "on each level of the pyramid then scales the image to the next level using VX_INTERPOLATION_TYPE_NEAREST_NEIGHBOR.</br>" +
                        "</br>" +
                        "Level 0 shall always have the same resolution as the input image. For the Gaussian pyramid, " +
                        "level 0 shall be the same as the input image. The pyramids must be configured with one of the following level scaling:</br>" +
                        "</br>" +
                        "• VX_SCALE_PYRAMID_HALF</br>" +
                        "• VX_SCALE_PYRAMID_ORB"
                }
            },
            VX_HARRIS_CORNER: {
                help: {
                    tooltip: "Computes the Harris Corners of an image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d7/d5f/group__group__vision__function__harris.html",
                    description: "" +
                        "Computes the Harris Corners of an image."
                }
            },
            VX_HISTOGRAM: {
                help: {
                    tooltip: "Generates a distribution from an image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d6/dcb/group__group__vision__function__histogram.html",
                    description: "" +
                        "Generates a distribution from an image.</br>" +
                        "</br>" +
                        "This kernel counts the number of occurrences of each pixel value within the window size of a pre-calculated number of bins."
                }
            },
            VX_HALF_SCALE_GAUSSIAN: {
                help: {
                    tooltip: "Performs a Gaussian Blur on an image then half-scales it.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d1/d26/group__group__vision__function__scale__image.html",
                    description: "" +
                        "Implements the Image Resizing Kernel.</br>" +
                        "</br>" +
                        "Performs a Gaussian Blur on an image then half-scales it.</br>" +
                        "</br>" +
                        "This kernel resizes an image from the source to the destination dimensions. The supported interpolation types are currently:</br>" +
                        "</br>" +
                        "VX_INTERPOLATION_TYPE_NEAREST_NEIGHBOR</br>" +
                        "VX_INTERPOLATION_TYPE_AREA</br>" +
                        "VX_INTERPOLATION_TYPE_BILINEAR</br>" +
                        "The sample positions used to determine output pixel values are generated by scaling the outside edges of the source image " +
                        "pixels to the outside edges of the destination image pixels. As described in the documentation for vx_interpolation_type_e, " +
                        "samples are taken at pixel centers. This means that, unless the scale is 1:1, the sample position for the top left " +
                        "destination pixel typically does not fall exactly on the top left source pixel but will be generated by interpolation."
                }
            },
            VX_INTEGRAL_IMAGE: {
                help: {
                    tooltip: "Computes the integral image of the input.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d0/d7b/group__group__vision__function__integral__image.html",
                    description: "" +
                        "Computes the integral image of the input.</br>" +
                        "</br>" +
                        "Each output pixel is the sum of the corresponding input pixel and all other pixels above and to the left of it."
                }
            },
            VX_MAGNITUDE: {
                help: {
                    tooltip: "Implements the Gradient Magnitude Computation Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/dd/df2/group__group__vision__function__magnitude.html",
                    description: "" +
                        "Implements the Gradient Magnitude Computation Kernel.</br>" +
                        "</br>" +
                        "This kernel takes two gradients in VX_DF_IMAGE_S16 format and computes the VX_DF_IMAGE_S16 normalized magnitude.</br>" +
                        "The conceptual definition describing the overflow is given as:</br>" +
                        "</br>" +
                        "<b>uint16 z = uint16( sqrt( double( uint32( int32(x) * int32(x) ) + uint32( int32(y) * int32(y) ) ) ) + 0.5);</b></br>" +
                        "</br>" +
                        "<b>int16 mag = z > 32767 ? 32767 : z;</b>"
                }
            },
            VX_MEAN_AND_STANDARD: {
                help: {
                    tooltip: "" +
                        "Computes the mean pixel value and the standard deviation<br>" +
                        "of the pixels in the input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d8/d85/group__group__vision__function__meanstddev.html",
                    description: "" +
                        "Computes the mean pixel value and the standard deviation of the pixels in the input image (which has a dimension width and height)."
                }
            },
            VX_MEDIAN_FILTER: {
                help: {
                    tooltip: "Computes a median pixel value over a window of the input image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d3/d77/group__group__vision__function__median__image.html",
                    description: "" +
                        "Computes a median pixel value over a window of the input image.</br>" +
                        "</br>" +
                        "The median is the middle value over an odd-numbered, sorted range of values."
                }
            },
            VX_MIN_AND_MAX_LOCATION: {
                help: {
                    tooltip: "Finds the minimum and maximum values in an image and a location for each.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d8/d05/group__group__vision__function__minmaxloc.html",
                    description: "" +
                        "Finds the minimum and maximum values in an image and a location for each.</br>" +
                        "</br>" +
                        "If the input image has several minimums/maximums, the kernel returns all of them.</br>" +
                        "</br>" +
                        "<b>minVal = min0 ≤ x′ ≤ width0 ≤ y′ ≤ heightsrc(x′,y′)</br>" +
                        "maxVal = max0 ≤ x′ ≤ width0 ≤ y′ ≤ heightsrc(x′,y′)</b>"
                }
            },
            VX_OPTICAL_FLOW: {
                help: {
                    tooltip: "Computes the optical flow using the Lucas-Kanade method between two pyramid images.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d0/d0c/group__group__vision__function__opticalflowpyrlk.html",
                    description: "" +
                        "Computes the optical flow using the Lucas-Kanade method between two pyramid images.</br>" +
                        "</br>" +
                        "The function is an implementation of the algorithm described in [1]. The function inputs are two vx_pyramid objects, " +
                        "old and new, along with a vx_array of vx_keypoint_t structs to track from the old vx_pyramid. The function outputs a " +
                        "vx_array of vx_keypoint_t structs that were tracked from the old vx_pyramid to the new vx_pyramid. Each element in the " +
                        "vx_array of vx_keypoint_t structs in the new array may be valid or not. The implementation shall return the same number " +
                        "of vx_keypoint_t structs in the new vx_array that were in the older vx_array.</br>" +
                        "</br>" +
                        "In more detail: The Lucas-Kanade method finds the affine motion vector V for each point in the old image tracking " +
                        "points array, using the following equation:</br>" +
                        "</br>" +
                        "<b>[VxVy] = [∑iIx2∑iIx∗Iy∑iIx∗Iy∑iIy2]−1[−∑iIx∗It−∑iIy∗It]</b></br>" +
                        "Where Ix and Iy are obtained using the Scharr gradients on the input image."
                }
            },
            VX_PHASE: {
                help: {
                    tooltip: "Implements the Gradient Phase Computation Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/db/d4e/group__group__vision__function__phase.html",
                    description: "" +
                        "Implements the Gradient Phase Computation Kernel.</br>" +
                        "</br>" +
                        "This kernel takes two gradients in VX_DF_IMAGE_S16 format and computes the angles for each pixel and stores this in a VX_DF_IMAGE_U8 image.</br>" +
                        "</br>" +
                        "<b>ϕ = tan−1grady(x,y)gradx(x,y)</b></br>" +
                        "Where ϕ is then translated to 0≤ϕ<2π. Each ϕ value is then mapped to the range 0 to 255 inclusive."
                }
            },
            VX_PIXEL_WISE_MULTIPLICATION: {
                help: {
                    tooltip: "Performs element-wise multiplication between two images and a scalar value.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d7/dae/group__group__vision__function__mult.html",
                    description: "" +
                        "Performs element-wise multiplication between two images and a scalar value.</br>" +
                        "</br>" +
                        "Pixel-wise multiplication is performed between the pixel values in two VX_DF_IMAGE_U8 or VX_DF_IMAGE_S16 images and a " +
                        "scalar floating-point number scale. The output image can be VX_DF_IMAGE_U8 only if both source images are " +
                        "VX_DF_IMAGE_U8 and the output image is explicitly set to VX_DF_IMAGE_U8. It is otherwise VX_DF_IMAGE_S16. If one of " +
                        "the input images is of type VX_DF_IMAGE_S16, all values are converted to VX_DF_IMAGE_S16.</br>" +
                        "</br>" +
                        "The scale with a value of 1/2n, where n is an integer and 0≤n≤15, and 1/255 (0x1.010102p-8 C99 float hex) must be " +
                        "supported. The support for other values of scale is not prohibited. Furthermore, for scale with a value of 1/255 " +
                        "the rounding policy of VX_ROUND_POLICY_TO_NEAREST_EVEN must be supported whereas for the scale with value of 1/2n " +
                        "the rounding policy of VX_ROUND_POLICY_TO_ZERO must be supported. The support of other rounding modes for any values " +
                        "of scale is not prohibited."
                }
            },
            VX_REMAP: {
                help: {
                    tooltip: "Maps output pixels in an image from input pixels in an image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/df/dca/group__group__vision__function__remap.html",
                    description: "" +
                        "Maps output pixels in an image from input pixels in an image.</br>" +
                        "</br>" +
                        "Remap takes a remap table object vx_remap to map a set of output pixels back to source input pixels. A remap " +
                        "is typically defined as:</br>" +
                        "</br>" +
                        "<b>out put(x1, y1) = input(mapx(x0, y0),mapy(x0, y0))</b></br>" +
                        "</br>" +
                        "However, the mapping functions are contained in the vx_remap object.</br>" +
                        "</br>"
                }
            },
            VX_SCALE_IMAGE: {
                help: {
                    tooltip: "Implements the Image Resizing Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d1/d26/group__group__vision__function__scale__image.html",
                    description: "" +
                        "Implements the Image Resizing Kernel.</br>" +
                        "</br>" +
                        "Performs a Gaussian Blur on an image then half-scales it. " +
                        "This kernel resizes an image from the source to the destination dimensions. The only format supported is " +
                        "VX_DF_IMAGE_U8. The supported interpolation types are currently:</br>" +
                        "</br>" +
                        "• VX_INTERPOLATION_TYPE_NEAREST_NEIGHBOR</br>" +
                        "• VX_INTERPOLATION_TYPE_AREA</br>" +
                        "• VX_INTERPOLATION_TYPE_BILINEAR</br>" +
                        "</br>" +
                        "The sample positions used to determine output pixel values are generated by scaling the outside edges of the " +
                        "source image pixels to the outside edges of the destination image pixels. As described in the documentation for " +
                        "vx_interpolation_type_e, samples are taken at pixel centers. This means that, unless the scale is 1:1, " +
                        "the sample position for the top left destination pixel typically does not fall exactly on the top left source pixel but will " +
                        "be generated by interpolation."
                }
            },
            VX_SOBEL: {
                help: {
                    tooltip: "Implements the Sobel Image Filter Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/da/d4b/group__group__vision__function__sobel3x3.html",
                    description: "" +
                        "Implements the Sobel Image Filter Kernel.</br>" +
                        "</br>" +
                        "This kernel produces two output planes (one can be omitted) in the x and y plane."
                }
            },
            VX_TABLE_LOOK_UP: {
                help: {
                    tooltip: "Implements the Table Lookup Image Kernel.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d5/d4e/group__group__vision__function__lut.html",
                    description: "" +
                        "Implements the Table Lookup Image Kernel.</br>" +
                        "</br>" +
                        "This kernel uses each pixel in an image to index into a LUT and put the indexed LUT value into the output image.</br>" +
                        "</br>" +
                        "The format supported is VX_DF_IMAGE_U8."
                }
            },
            VX_THRESHOLD: {
                help: {
                    tooltip: "Thresholds an input image and produces an output Boolean image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d3/d1e/group__group__vision__function__threshold.html",
                    description: "" +
                        "Thresholds an input image and produces an output Boolean image."
                }
            },
            VX_WARP_AFFINE: {
                help: {
                    tooltip: "Performs an affine transform on an image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/d5/d5f/group__group__vision__function__warp__affine.html",
                    description: "" +
                        "Performs an affine transform on an image.</br>" +
                        "</br>" +
                        "This kernel performs an affine transform with a 2x3 Matrix."
                }
            },
            VX_WARP_PERSPECTIVE: {
                help: {
                    tooltip: "Performs a perspective transform on an image.",
                    link: "https://www.khronos.org/registry/OpenVX/specs/1.0.1/html/da/d6a/group__group__vision__function__warp__perspective.html",
                    description: "" +
                        "Performs a perspective transform on an image.</br>" +
                        "</br>" +
                        "This kernel performs an perspective transform with a 3x3 Matrix."
                }
            }
        }
    }
});
