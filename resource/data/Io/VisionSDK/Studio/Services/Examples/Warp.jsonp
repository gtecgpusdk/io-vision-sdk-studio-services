/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.VisionSDK.Studio.Services.DAO.Resources.Data({
    $interface: "IVisualGraph",
    imports: {
        vxAPI: "resource/data/Io/VisionSDK/Studio/Services/Configuration/OpenVX_1_1_0/CodeGeneratorTemplates.jsonp"
    },
    id: "b5e9340bf66d36186ed01e1f3f6e2d8d4d3f99a3",
    name: "Warp",
    description: "This example shows usage of Warp Affine and Perspective OpenVX nodes.<br><br>" +
        "<img src=\"resource/data/Io/VisionSDK/Studio/Services/Examples/Snapshots/Warp.png\">",
    gridDimX: 4,
    gridDimY: 4,
    ioComs: [
        {
            type() {
                return this.vxAPI.vxTypes.CAMERA;
            },
            name: "ioCom0",
            id: "ioCom0",
            column: 0,
            row: 0,
            deviceIndex: 0,
            height: 480,
            width: 640,
            outputType: "VX_DF_IMAGE_RGB",
            fpsType: "VX_TYPE_FLOAT32",
            fps: 10.000,
            params() {
                return [
                    this.ioComs[0].deviceIndex,
                    this.vxContexts[0].vxGraphs[0].vxData[0],
                    this.vxContexts[0].vxGraphs[0].vxData[7]
                ];
            },
            outputs() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[0],
                    this.vxContexts[0].vxGraphs[0].vxData[7]
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DISPLAY;
            },
            name: "ioCom1",
            id: "ioCom1",
            column: 4,
            row: 0,
            inputType: "VX_DF_IMAGE_U8",
            windowTitle: "Warp Affine",
            params() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[4],
                    this.ioComs[1].windowTitle
                ];
            }
        },
        {
            type() {
                return this.vxAPI.vxTypes.DISPLAY;
            },
            name: "ioCom2",
            id: "ioCom2",
            column: 4,
            row: 1,
            inputType: "VX_DF_IMAGE_U8",
            windowTitle: "Warp Perspective",
            params() {
                return [
                    this.vxContexts[0].vxGraphs[0].vxData[6],
                    this.ioComs[2].windowTitle
                ];
            }
        }
    ],
    vxContexts: [
        {
            name: "Context",
            id: "Context0",
            column: 0,
            row: 0,
            gridDimX: 4,
            gridDimY: 4,
            vxGraphs: [
                {
                    name: "Graph",
                    id: "Graph0",
                    column: 0,
                    row: 0,
                    gridDimX: 5,
                    gridDimY: 4,
                    vxNodes: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_COLOR_CONVERT;
                            },
                            name: "vxNode0",
                            id: "vxNode0",
                            column: 1,
                            row: 0,
                            inputImageFormat: "VX_DF_IMAGE_RGB",
                            outputImageFormat: "VX_DF_IMAGE_YUV4",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_CHANNEL_EXTRACT;
                            },
                            name: "vxNode1",
                            id: "vxNode1",
                            column: 2,
                            row: 0,
                            inputType: "VX_DF_IMAGE_YUV4",
                            channel: "VX_CHANNEL_Y",
                            outputType: "VX_DF_IMAGE_U8",
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxData[2]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_WARP_AFFINE;
                            },
                            name: "vxNode2",
                            id: "vxNode2",
                            column: 3,
                            row: 0,
                            imageType: "VX_DF_IMAGE_U8",
                            dataType: "VX_TYPE_FLOAT32",
                            interpolationType: "VX_INTERPOLATION_BILINEAR",
                            columns: 2,
                            rows: 3,
                            matrix: [[0.707,0.707],[-0.707,0.707],[250,-200]],
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[3],
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[4]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_WARP_PERSPECTIVE;
                            },
                            name: "vxNode3",
                            id: "vxNode3",
                            column: 3,
                            row: 1,
                            imageType: "VX_DF_IMAGE_U8",
                            dataType: "VX_TYPE_FLOAT32",
                            interpolationType: "VX_INTERPOLATION_BILINEAR",
                            columns: 3,
                            rows: 3,
                            matrix: [[1,0.5,0],[0,1,0],[0,0,1]],
                            params() {
                                return [
                                    this.vxContexts[0].vxGraphs[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[5],
                                    this.vxContexts[0].vxGraphs[0].vxData[6]
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxData[6]
                                ];
                            }
                        }
                    ],
                    vxData: [
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData0",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].width,
                                    this.ioComs[0].height,
                                    this.ioComs[0].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData1",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[0],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[0].outputImageFormat
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData2",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxData[1],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[1].outputType
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_MATRIX;
                            },
                            id: "vxData3",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxAPI.vxTypes.VX_MATRIX,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].dataType,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].columns,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].rows,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].matrix
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData4",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[2].imageType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[1]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_MATRIX;
                            },
                            id: "vxData5",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxAPI.vxTypes.VX_MATRIX,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].dataType,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].columns,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].rows,
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].matrix
                                ];
                            },
                            outputs() {
                                return [
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_IMAGE;
                            },
                            id: "vxData6",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxData[2],
                                    this.vxContexts[0].vxGraphs[0].vxNodes[3].imageType
                                ];
                            },
                            outputs() {
                                return [
                                    this.ioComs[2]
                                ];
                            }
                        },
                        {
                            type() {
                                return this.vxAPI.vxTypes.VX_SCALAR;
                            },
                            id: "vxData7",
                            params() {
                                return [
                                    this.vxContexts[0],
                                    this.ioComs[0].fpsType,
                                    this.ioComs[0].fps
                                ];
                            }
                        }
                    ]
                }
            ]
        }
    ]
});
