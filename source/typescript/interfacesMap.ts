/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.VisionSDK.Studio.Services.DAO.CodeGenerator {
    "use strict";
    export let IVisualGraphDAOEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnError"
            ]);
        }();
}

/* tslint:enable */
