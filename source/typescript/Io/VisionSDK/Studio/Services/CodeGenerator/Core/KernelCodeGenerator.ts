/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Core {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IKernelTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelTemplates;
    import KernelTemplatesDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.KernelTemplatesDAO;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IKernelCodeAttributes = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelCodeAttributes;
    import IKernelsCodeProject = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelsCodeProject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class KernelCodeGenerator extends BaseObject {
        private fileSystem : FileSystemHandlerConnector;
        private projectDirectory : string;
        private readonly templates : IKernelTemplates;
        private codeCache : any[];

        constructor($dao : KernelTemplatesDAO) {
            super();

            this.fileSystem = new FileSystemHandlerConnector();
            this.projectDirectory = "";
            this.templates = $dao.getStaticConfiguration();
            this.codeCache = [];
        }

        public ProjectDirectory($value? : string) : string {
            return this.projectDirectory = Property.String(this.projectDirectory, $value);
        }

        public LoadKernels($project : IKernelsCodeProject,
                           $callback : ($status : boolean, $kernels : IKernelCodeAttributes[]) => void, $registerPath? : string) : void {
            let kernels : IKernelCodeAttributes[] = [];
            if (ObjectValidator.IsEmptyOrNull($registerPath)) {
                $registerPath = this.projectDirectory + "/resource/configs/KernelsRegister.json";
            }
            this.fileSystem.Read($registerPath).Then(($data : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    try {
                        if (ObjectValidator.IsString($data)) {
                            kernels = <IKernelCodeAttributes[]>JSON.parse($data);
                        } else {
                            kernels = (<any>$data);
                        }
                        $callback(true, kernels);
                    } catch (ex) {
                        $callback(false, kernels);
                        Echo.Printf(ex.stack);
                        LogIt.Error("Failed kernels parser register.", ex);
                    }
                } else {
                    $callback(false, kernels);
                }
            });
        }

        public GenerateKernel($project : IKernelsCodeProject, $kernel : IKernelCodeAttributes,
                              $callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            this.generateLoader($project, $kernel, ($status : boolean, $loaderFilesMap? : string[]) : void => {
                if ($status) {
                    this.generateSource($kernel, ($status : boolean, $filesMap? : string[]) : void => {
                        if ($status) {
                            $callback(true, $loaderFilesMap.concat($filesMap));
                        } else {
                            $callback(false);
                        }
                    });
                } else {
                    $callback(false);
                }
            });
        }

        private generateLoader($project : IKernelsCodeProject, $kernel : IKernelCodeAttributes,
                               $callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            this.codeCache = [];

            const projectNamespace : string = StringUtils.Replace($project.namespace, ".", "/");

            const types : string[] = [];
            $project.kernels.forEach(($kernel : IKernelCodeAttributes) : void => {
                types.push($kernel.name);
            });
            if (types.indexOf($kernel.name) === -1) {
                types.push($kernel.name);
                $project.kernels.push($kernel);
            }

            let cwd : string = this.projectDirectory + "/source/cpp";
            this.checkDirectory(cwd);
            this.codeCache.push({
                content: this.templates.main.apply(this.templates, [
                    types
                ]),
                path   : cwd + "/LibraryMain.cpp",
                task   : "Write"
            });

            this.codeCache.push({
                content: this.templates.vxKernels.apply(this.templates, [
                    $project.namespace,
                    "VxKernels",
                    types
                ]),
                path   : cwd + "/VxKernels.hpp",
                task   : "Write"
            });

            cwd = this.projectDirectory + "/source/cpp/" + projectNamespace + "/Enums";
            this.checkDirectory(cwd);
            this.codeCache.push({
                content: this.templates.type.apply(this.templates, [
                    $project.namespace + ".Enums",
                    "KernelType",
                    $project.companyId,
                    $project.index,
                    types
                ]),
                path   : cwd + "/KernelType.hpp",
                task   : "Write"
            });

            cwd = this.projectDirectory + "/resource/configs";
            this.checkDirectory(cwd);
            this.codeCache.push({
                content: this.templates.register.apply(this.templates, [
                    $project.kernels
                ]),
                path   : cwd + "/KernelsRegister.json",
                task   : "Write"
            });

            this.saveFiles($callback);
        }

        private generateSource($attributes : IKernelCodeAttributes, $callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            this.codeCache = [];

            const projectNamespace : string = StringUtils.Replace($attributes.project, ".", "/");
            const kernelNamespace : string = StringUtils.Replace($attributes.namespace, ".", "/");
            const fullNamespace : string = $attributes.project + "." + $attributes.namespace;

            let cwd : string = this.projectDirectory + "/source/cpp/" + projectNamespace + "/" + kernelNamespace;
            this.checkDirectory(cwd);
            this.codeCache.push({
                content: this.templates.cpp.header.apply(this.templates, [
                    fullNamespace,
                    $attributes.name + "Kernel",
                    $attributes.description,
                    $attributes.params
                ]),
                path   : cwd + "/" + $attributes.name + "Kernel.hpp",
                task   : "Write"
            });
            this.codeCache.push({
                content: this.templates.cpp.source.apply(this.templates, [
                    $attributes.project,
                    fullNamespace,
                    $attributes.name + "Kernel",
                    $attributes.params
                ]),
                path   : cwd + "/" + $attributes.name + "Kernel.cpp",
                task   : "Write"
            });
            this.codeCache.push({
                content: this.templates.c.header.apply(this.templates, [
                    fullNamespace,
                    $attributes.name + "Node",
                    $attributes.description,
                    $attributes.params
                ]),
                path   : cwd + "/" + $attributes.name + "Node.h",
                task   : "Write"
            });
            this.codeCache.push({
                content: this.templates.c.source.apply(this.templates, [
                    $attributes.target,
                    fullNamespace,
                    $attributes.name + "Node",
                    $attributes.params
                ]),
                path   : cwd + "/" + $attributes.name + "Node.c",
                task   : "Write"
            });

            cwd = this.projectDirectory + "/test/unit/cpp/" + projectNamespace + "/" + kernelNamespace;
            this.checkDirectory(cwd);
            this.codeCache.push({
                content: this.templates.test.apply(this.templates, [
                    fullNamespace,
                    $attributes.name + "NodeTest"
                ]),
                path   : cwd + "/" + $attributes.name + "NodeTest.cpp",
                task   : "Write"
            });

            this.saveFiles($callback);
        }

        private saveFiles($callback : ($status : boolean, $filesMap? : string[]) => void) : void {
            const filesMap : string[] = [];
            const processNextFileSystemTask : any = ($index : number) : void => {
                if ($index < this.codeCache.length) {
                    const task : any = this.codeCache[$index];
                    filesMap.push(task.path);
                    switch (task.task) {
                    case "CreateDirectory":
                        this.fileSystem
                            .Exists(task.path)
                            .Then(($status : boolean) : void => {
                                if (!$status) {
                                    LogIt.Debug("Create directory: {0}", task.path);
                                    this.fileSystem
                                        .CreateDirectory(task.path)
                                        .Then(($status : boolean) : void => {
                                            if ($status) {
                                                processNextFileSystemTask($index + 1);
                                            } else {
                                                $callback(false);
                                            }
                                        });
                                } else {
                                    processNextFileSystemTask($index + 1);
                                }
                            });
                        break;
                    case "Write":
                        const write : any = () : void => {
                            LogIt.Debug("Create/Update file: {0}", task.path);
                            this.fileSystem.Write(task.path, task.content)
                                .Then(($status : boolean) : void => {
                                    if ($status) {
                                        processNextFileSystemTask($index + 1);
                                    } else {
                                        $callback(false);
                                    }
                                });
                        };
                        this.fileSystem
                            .Exists(task.path)
                            .Then(($status : boolean) : void => {
                                if (!$status) {
                                    write();
                                } else {
                                    this.fileSystem
                                        .Read(task.path)
                                        .Then(($content : string) : void => {
                                            if (task.content !== $content) {
                                                write();
                                            } else {
                                                processNextFileSystemTask($index + 1);
                                            }
                                        });
                                }
                            });
                        break;
                    default:
                        processNextFileSystemTask($index + 1);
                        break;
                    }
                } else {
                    $callback(true, filesMap);
                }
            };
            processNextFileSystemTask(0);
        }

        private checkDirectory($dirPath : string) : void {
            this.codeCache.push({
                path: $dirPath,
                task: "CreateDirectory"
            });
        }

        private normalizeChars($input : string, $regex : RegExp) : string {
            return (<any>$input).replace($regex, "_");
        }
    }
}
