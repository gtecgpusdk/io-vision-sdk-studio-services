/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Models {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import IVisualGraphNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraphNode;
    import IVisualGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraph;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Size = Com.Wui.Framework.Gui.Structures.Size;

    export class VisualGraphNode extends BaseObject {
        private parent : VisualGraphNode;
        private type : string;
        private index : number;
        private uniqueId : string;
        private name : string;
        private column : number;
        private row : number;
        private gridDimX : number;
        private gridDimY : number;
        private configuration : IVisualGraph;

        constructor() {
            super();
            this.parent = null;
            this.index = -1;
            this.name = "";
            this.uniqueId = null;
        }

        public Parent($value? : VisualGraphNode) : VisualGraphNode {
            if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsMemberOf(VisualGraphNode)) {
                this.parent = $value;
            }
            return this.parent;
        }

        public Configuration($value? : IVisualGraph) : IVisualGraph {
            if (ObjectValidator.IsSet($value)) {
                this.configuration = $value;
            }
            return this.configuration;
        }

        public Type($value? : string) : string {
            return this.type = Property.String(this.type, $value);
        }

        public Index($value? : number) : number {
            return this.index = Property.Integer(this.index, $value);
        }

        public Name($value? : string) : string {
            return this.name = Property.String(this.name, $value);
        }

        public UniqueId($value? : string) : string {
            this.uniqueId = Property.String(this.uniqueId, $value);
            if (ObjectValidator.IsEmptyOrNull(this.uniqueId) && this.configuration.vxAPI.vxData.hasOwnProperty(this.type)) {
                this.uniqueId = this.getVarId();
            }
            return this.uniqueId;
        }

        public getReference() : string {
            if (this.configuration.vxAPI.vxIoComs.hasOwnProperty(this.type)) {
                return "this.ioComs[" + this.Index() + "]";
            } else if (this.configuration.vxAPI.vxNodes.hasOwnProperty(this.type)) {
                return "" +
                    "this.vxContexts[" + this.Parent().Parent().Index() + "]" +
                    ".vxGraphs[" + this.Parent().Index() + "]" +
                    ".vxNodes[" + this.Index() + "]";
            } else if (this.configuration.vxAPI.vxData.hasOwnProperty(this.type)) {
                return "" +
                    "this.vxContexts[" + this.Parent().Parent().Index() + "]" +
                    ".vxGraphs[" + this.Parent().Index() + "]" +
                    ".vxData[" + this.Index() + "]";
            } else {
                return "" +
                    "this.vxContexts[" + this.Index() + "]";
            }
        }

        public ToKernelNode($iconType : IconType) : KernelNode {
            const node : KernelNode = new KernelNode($iconType, this.Row(), this.Column());
            node.Type($iconType.toString());
            node.Name(this.Name());
            node.UniqueId(this.UniqueId());

            const size : Size = new Size();
            size.Width(this.GridDimX());
            size.Height(this.GridDimY());
            node.GridSize(size);
            return node;
        }

        public FromKernelNode($node : KernelNode) : void {
            this.Type($node.Type());
            this.Name($node.Name());
            this.UniqueId($node.UniqueId());
            this.Row($node.Row());
            this.Column($node.Column());
            this.GridDimX($node.GridSize().Width());
            this.GridDimY($node.GridSize().Height());
        }

        public FromNodeConfiguration($node : IVisualGraphNode) : void {
            if (!ObjectValidator.IsEmptyOrNull($node.type)) {
                this.Type($node.type.apply(this.configuration));
            }
            this.Name($node.name);
            this.UniqueId($node.id);
            this.Column($node.column);
            this.Row($node.row);
        }

        public Column($value? : number) : number {
            return this.column = Property.Integer(this.column, $value);
        }

        public Row($value? : number) : number {
            return this.row = Property.Integer(this.row, $value);
        }

        public GridDimX($value? : number) : number {
            return this.gridDimX = Property.Integer(this.gridDimX, $value);
        }

        public GridDimY($value? : number) : number {
            return this.gridDimY = Property.Integer(this.gridDimY, $value);
        }

        public ToString($prefix? : string, $htmlTag? : boolean) : string {
            return "" +
                $prefix + "name: " + this.Name() + StringUtils.NewLine($htmlTag) +
                $prefix + "index: " + this.Index() + StringUtils.NewLine($htmlTag) +
                $prefix + "type: " + this.Type();
        }

        protected getVarId() : string {
            const index : string = this.Index() !== -1 ? this.Index().toString() : "";
            if (!this.Configuration().vxAPI.vxNodes.hasOwnProperty(this.Type())) {
                return "vxData" + index;
            }
        }
    }
}
