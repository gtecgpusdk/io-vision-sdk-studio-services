/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Models {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class VisualGraph extends VisualGraphNode {
        private id : string;
        private description : string;
        private readonly ioComs : ArrayList<VXNode>;
        private readonly vxContexts : ArrayList<VXContext>;

        constructor() {
            super();

            this.id = "";
            this.description = "";
            this.ioComs = new ArrayList<VXNode>();
            this.vxContexts = new ArrayList<VXContext>();
        }

        public Id($value? : string) : string {
            return this.id = Property.String(this.id, $value);
        }

        public Description($value? : string) : string {
            return this.description = Property.String(this.description, $value);
        }

        public getIoComs() : ArrayList<VXNode> {
            return this.ioComs;
        }

        public getVxContexts() : ArrayList<VXContext> {
            return this.vxContexts;
        }

        public ToString($prefix? : string, $htmlTag? : boolean) : string {
            return "" +
                $prefix + "IOComs: " + this.getIoComs().ToString($prefix + StringUtils.Tab(1, $htmlTag), $htmlTag) +
                StringUtils.NewLine($htmlTag) +
                $prefix + "VXContexts: " + this.getVxContexts().ToString($prefix + StringUtils.Tab(1, $htmlTag), $htmlTag);
        }
    }
}
