/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.CodeGenerator.Models {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IVXContext = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXContext;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class VXContext extends VisualGraphNode {
        private readonly vxGraphs : ArrayList<VXGraph>;
        private varName : string;

        constructor() {
            super();
            this.vxGraphs = new ArrayList<VXGraph>();
        }

        public Parent($value? : VisualGraph) : VisualGraph {
            return <VisualGraph>super.Parent($value);
        }

        public getVxGraphs() : ArrayList<VXGraph> {
            return this.vxGraphs;
        }

        public VarName($value? : string) : string {
            this.varName = Property.String(this.varName, $value);
            if (ObjectValidator.IsEmptyOrNull(this.varName)) {
                this.varName = "context" + this.Index();
            }
            return this.varName;
        }

        public FromNodeConfiguration($node : IVXContext) : void {
            super.FromNodeConfiguration($node);
        }

        public ToString($prefix? : string, $htmlTag? : boolean) : string {
            return $prefix + "VXGraphs: " + this.getVxGraphs().ToString($prefix + StringUtils.Tab(1, $htmlTag), $htmlTag);
        }

        protected getVarId() : string {
            const index : string = this.Index() !== -1 ? this.Index().toString() : "";
            return "Context" + index;
        }
    }
}
