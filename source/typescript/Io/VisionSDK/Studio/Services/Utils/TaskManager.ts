/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import TerminalOptions = Com.Wui.Framework.Services.Connectors.TerminalOptions;
    import ITaskManagerEnvironment = Io.VisionSDK.Studio.Services.Interfaces.ITaskManagerEnvironment;
    import FFIProxyConnector = Com.Wui.Framework.Services.Connectors.FFIProxyConnector;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import FFIOptions = Com.Wui.Framework.Services.Connectors.FFIOptions;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;

    export class TaskManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private readonly fileSystem : FileSystemHandlerConnector;
        private readonly terminal : TerminalConnector;
        private sdkPath : string;
        private readonly sdkVersion : string;
        private compilerPath : string;
        private cmakePath : string;
        private readonly projectPath : string;
        private readonly targetName : string;
        private readonly cachePath : string;
        private forceKilled : boolean;
        private readonly platform : PlatformType;
        private printHandler : ($message : string) => void;
        private errorHandler : ($message : string) => void;
        private readonly remoteFileSystem : FileSystemHandlerConnector;
        private readonly remoteTerminal : TerminalConnector;
        private remoteProjectPath : string;
        private remoteCachePath : string;
        private readonly environment : ITaskManagerEnvironment;
        private readonly useFFIRunner : boolean;
        private readonly ffi : FFIProxyConnector;
        private readonly remoteFfi : FFIProxyConnector;
        private readonly remoteManager : RemoteTargetManager;
        private readonly isRemote : boolean;
        private readonly isAgent : boolean;
        private readonly userID : string;
        private buildExecuted : boolean;
        private notifications : IMainPageNotifications;

        constructor($projectPath : string, $targetName : string, $environment : ITaskManagerEnvironment,
                    $remoteManager? : RemoteTargetManager) {
            super();
            this.useFFIRunner = false;
            this.buildExecuted = false;
            this.notifications = $environment.notifications;
            this.fileSystem = new FileSystemHandlerConnector(true);
            this.terminal = new TerminalConnector(true);
            if (this.useFFIRunner) {
                this.ffi = new FFIProxyConnector(true);
            }
            this.environment = $environment;
            this.sdkVersion = StringUtils.Replace(this.environment.sdkVersion, ".", "-");
            this.projectPath = $projectPath;
            this.cachePath = this.projectPath + "/build_cache";
            this.targetName = $targetName.replace(new RegExp(/[^a-zA-Z0-9_\-.]/g), "_");
            this.forceKilled = false;
            this.compilerPath = Property.String(this.compilerPath, this.environment.compilerPath);
            this.cmakePath = Property.String(this.cmakePath, this.environment.cmakePath);
            this.platform = Property.EnumType(this.platform, this.environment.platform, PlatformType, PlatformType.WIN_GCC);

            // todo replace this userID from sessionid by real userID when user accounts will be available
            this.userID = PersistenceFactory.getHttpSessionId();
            LogIt.Debug("UserID: {0}", this.userID);
            LogIt.Debug("Project path: {0}", this.projectPath);

            this.printHandler = ($message : string) : void => {
                Echo.Println($message);
            };
            this.errorHandler = ($message : string) : void => {
                Echo.Println($message);
            };
            this.remoteManager = $remoteManager;
            if (!ObjectValidator.IsEmptyOrNull(this.remoteManager)) {
                this.isRemote = true;
                this.isAgent = this.remoteManager.IsAgent();
            } else {
                this.isRemote = false;
                this.isAgent = false;
            }
            if (ObjectValidator.IsEmptyOrNull($environment.hubUrl)) {
                $environment.hubUrl = $environment.serverUrl;
            }
            if (this.isRemote) {
                this.remoteFileSystem = new FileSystemHandlerConnector();
                this.remoteTerminal = new TerminalConnector();
                if (this.useFFIRunner) {
                    this.remoteFfi = new FFIProxyConnector();
                }
                [this.remoteFileSystem, this.remoteTerminal, this.remoteFfi].forEach(($connector : BaseConnector) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($connector)) {
                        $connector.setAgent($environment.agentName, $environment.hubUrl + "/connector.config.jsonp");
                    }
                });
            }
            [
                this.fileSystem, this.terminal, this.remoteFileSystem, this.remoteTerminal, this.remoteFfi
            ].forEach(($connector : BaseConnector) : void => {
                if (!ObjectValidator.IsEmptyOrNull($connector)) {
                    $connector.ErrorPropagation(true);
                    $connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                        this.errorHandler($eventArgs.Message());
                    });
                }
            });
        }

        public setOnPrint($callback : ($message : string) => void) : void {
            this.printHandler = $callback;
        }

        public setOnError($callback : ($message : string) => void) : void {
            this.errorHandler = $callback;
        }

        public IsAgent() : boolean {
            return this.isAgent;
        }

        public getSdkVersion() : string {
            return this.environment.sdkVersion;
        }

        public getPlatform() : PlatformType {
            return this.platform;
        }

        public Init($callback : () => void) : void {
            this.buildExecuted = false;
            if (this.isAgent) {
                this.remoteFileSystem.getTempPath().Then(($path : string) : void => {
                    this.sdkPath = $path + "/vision-sdk-studio-sync/VisionSDK/" +
                        this.sdkVersion + "-" + this.environment.runtimeVersion + "/VisionSDK";
                    this.remoteProjectPath = $path + "/vision-sdk-studio-sync/" + this.userID + "/" + StringUtils.getSha1(this.projectPath);
                    this.remoteCachePath = this.remoteProjectPath + "/build_cache";
                    if (this.platform === PlatformType.WIN_GCC || this.platform === PlatformType.MSVC) {
                        ToolchainManager.getLocalAppDataPath(this.remoteTerminal, ($appDataPath : string) : void => {
                            this.environment.cmakePath =
                                StringUtils.Replace(this.environment.cmakePath, "%localappdata%", $appDataPath);
                            this.environment.compilerPath =
                                StringUtils.Replace(this.environment.compilerPath, "%localappdata%", $appDataPath);
                            this.compilerPath = Property.String(this.compilerPath, this.environment.compilerPath);
                            this.cmakePath = Property.String(this.cmakePath, this.environment.cmakePath);
                            $callback();
                        });
                    } else {
                        $callback();
                    }
                });
            } else if (!this.isRemote) {
                if (ObjectValidator.IsEmptyOrNull(this.environment.sdkPath)) {
                    this.fileSystem.getTempPath().Then(($path : string) : void => {
                        this.sdkPath = $path + "/vision-sdk-libraries/" + this.sdkVersion + "-" + this.environment.runtimeVersion +
                            "/VisionSDK";
                        $callback();
                    });
                } else {
                    this.sdkPath = this.environment.sdkPath;
                    $callback();
                }
            } else {
                if (!ObjectValidator.IsEmptyOrNull(this.environment.sdkPath)) {
                    this.sdkPath = this.environment.sdkPath;
                } else {
                    this.sdkPath = "/var/tmp/vision-sdk-studio/VisionSDK/" +
                        this.sdkVersion + "-" + this.environment.runtimeVersion;
                }
                $callback();
            }
        }

        public Build($callback : ($message : string, $status? : boolean) => void) : void {
            let fileSystem : FileSystemHandlerConnector = this.fileSystem;
            let terminal : TerminalConnector = this.terminal;
            let cachePath : string = this.cachePath;
            if (this.isRemote) {
                fileSystem = this.remoteFileSystem;
                terminal = this.remoteTerminal;
                cachePath = this.remoteCachePath;
            }
            let cacheGenerated : boolean = false;
            const generateCache : any = ($onSuccess : () => void) : void => {
                $callback(this.notifications.generating_cpp_make_files);
                this.generateMakefiles(($status : boolean, $message : string) : void => {
                    if ($status) {
                        cacheGenerated = true;
                        $onSuccess();
                    } else {
                        $callback($message, false);
                    }
                });
            };
            const build : any = () : void => {
                $callback(this.notifications.building_cpp_code);
                const buildArgs : string[] = ["--build", ".", "--"];
                const options : TerminalOptions = {
                    cwd: cachePath
                };
                if (this.platform === PlatformType.MSVC) {
                    options.env = {
                        PATH: StringUtils.Replace(this.cmakePath, "/", "\\") + ";" +
                            "%PATH%"
                    };
                    buildArgs.push("/m");
                } else if (this.platform === PlatformType.WIN_GCC) {
                    options.env = {
                        PATH: StringUtils.Replace(this.cmakePath, "/", "\\") + ";" +
                            StringUtils.Replace(this.compilerPath, "/", "\\") + ";" +
                            "%PATH%"
                    };
                    buildArgs.push("-j4");
                } else if (this.platform === PlatformType.LINUX) {
                    buildArgs.push("-j4");
                    options.env = {
                        PATH: this.cmakePath + ":$PATH"
                    };
                } else if (this.platform === PlatformType.IMX) {
                    buildArgs.push("-j4");
                }

                terminal
                    .Spawn("cmake", buildArgs, options)
                    .OnMessage(($data : string) : void => {
                        this.printHandler($data);
                    })
                    .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                        if ($exitCode === 0) {
                            this.buildExecuted = true;
                            $callback(this.notifications.cpp_code_was_successfully_generated, true);
                        } else {
                            if (!cacheGenerated && StringUtils.ContainsIgnoreCase($stdout + $stderr, "could not load cache")) {
                                generateCache(build);
                            } else {
                                $callback(this.notifications.unable_to_build_cpp_code, false);
                            }
                        }
                    });
            };
            const validateCache : any = () : void => {
                fileSystem
                    .Exists(cachePath)
                    .Then(($status : boolean) : void => {
                        if (!$status) {
                            generateCache(build);
                        } else {
                            build();
                        }
                    });
            };
            if (this.isAgent) {
                this.Synchronize(($message : string, $status? : boolean) : void => {
                    if ($status) {
                        validateCache();
                    } else {
                        $callback($message, $status);
                    }
                });
            } else if (!this.isRemote) {
                this.prepareSdk(($message : string, $status? : boolean) : void => {
                    if (ObjectValidator.IsSet($status)) {
                        if ($status) {
                            validateCache();
                        } else {
                            $callback($message, $status);
                        }
                    } else {
                        $callback($message);
                    }
                });
            } else {
                validateCache();
            }
        }

        public ExecutableExists($callback : ($status : boolean) => void) : void {
            let fileSystem : FileSystemHandlerConnector = this.fileSystem;
            let projectPath : string = this.projectPath;
            if (this.isRemote) {
                fileSystem = this.remoteFileSystem;
                projectPath = this.remoteProjectPath;
            }
            fileSystem
                .Exists(projectPath + "/build/" + this.targetName +
                    ((this.platform === PlatformType.WIN_GCC || this.platform === PlatformType.MSVC) ? ".exe" : ""))
                .Then($callback);
        }

        public Run($inLoop : boolean, $callback : ($message : string, $status? : boolean) => void) : void {
            let terminal : TerminalConnector = this.terminal;
            let ffi : FFIProxyConnector = this.ffi;
            let projectPath : string = this.projectPath;
            if (this.isRemote) {
                terminal = this.remoteTerminal;
                ffi = this.remoteFfi;
                projectPath = this.remoteProjectPath;
            }
            const getEnvPath : any = ($callback : ($path : string) => void) : void => {
                if (this.platform === PlatformType.WIN_GCC) {
                    $callback(StringUtils.Replace(this.sdkPath + "/bins", "/", "\\") + ";%PATH%");
                } else if (this.platform === PlatformType.LINUX || this.platform === PlatformType.IMX) {
                    $callback(StringUtils.Replace(this.sdkPath + "/bins", "\\", "/") + ":$PATH");
                }
            };
            const finalize : any = () : void => {
                if (this.isAgent) {
                    this.getProducts(($message : string, $status : boolean) : void => {
                        if ($status) {
                            this.buildExecuted = false;
                            $callback(this.notifications.run_successfully, true);
                        } else {
                            $callback($message, false);
                        }
                    });
                } else {
                    this.buildExecuted = false;
                    $callback(this.notifications.run_successfully, true);
                }
            };
            const run : any = () : void => {
                getEnvPath(($binsPath : string) : void => {
                    if (!this.useFFIRunner) {
                        let cmd : string = this.targetName + ".exe";
                        if (this.platform === PlatformType.LINUX || this.platform === PlatformType.IMX) {
                            cmd = "./" + this.targetName;
                        }
                        terminal
                            .Spawn("\"" + cmd + "\"", [
                                "--headless", $inLoop ? "--looped" : ""
                            ], {
                                cwd: projectPath + "/build",
                                env: {PATH: $binsPath}
                            })
                            .OnMessage(($data : string) : void => {
                                this.printHandler($data);
                            })
                            .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                if ($exitCode === 0 || this.forceKilled) {
                                    this.forceKilled = false;
                                    finalize();
                                } else if (!this.buildExecuted && StringUtils.ContainsIgnoreCase($stdout + $stderr,
                                    "Can not load library from path")) {
                                    this.Clean(($status : boolean) : void => {
                                        if ($status) {
                                            this.Build(($message : string, $status : boolean) : void => {
                                                if (ObjectValidator.IsSet($status)) {
                                                    if ($status) {
                                                        run();
                                                    } else {
                                                        $callback($message, false);
                                                    }
                                                } else {
                                                    $callback($message);
                                                }
                                            });
                                        } else {
                                            $callback(this.notifications.failed_to_clean_up_cache, false);
                                        }
                                    });
                                } else {
                                    $callback(this.notifications.run_failed, false);
                                }
                            });
                    } else {
                        ffi
                            .Release()
                            .Then(() : void => {
                                ffi
                                    .Load(<FFIOptions>{
                                        cwd        : projectPath + "/build",
                                        env        : {
                                            path: $binsPath
                                        },
                                        libraryPath: projectPath + "/build/" +
                                            (this.platform === PlatformType.WIN_GCC ? "VisionGraph.dll" : "libVisionGraph.so")
                                    })
                                    .Then(($status : boolean, $libPath : string) : void => {
                                        if ($status) {
                                            ffi
                                                .InvokeMethod("Create", LiveContentArgumentType.RETURN_INT)
                                                .Then(($returnValue : number) : void => {
                                                    if ($returnValue === 0) {
                                                        ffi
                                                            .InvokeMethod("Validate", LiveContentArgumentType.RETURN_INT)
                                                            .Then(($returnValue : number) : void => {
                                                                if ($returnValue === 0) {
                                                                    ffi
                                                                        .InvokeMethod("Process", LiveContentArgumentType.RETURN_INT,
                                                                            {
                                                                                type : LiveContentArgumentType.IN_STRUCT,
                                                                                value: {
                                                                                    fields  : {
                                                                                        /* tslint:disable: object-literal-sort-keys */
                                                                                        looped  : $inLoop,
                                                                                        headless: true
                                                                                        /* tslint:enable */
                                                                                    },
                                                                                    typeName: "ProcessOptions"
                                                                                }
                                                                            })
                                                                        .Then(($returnValue : number) : void => {
                                                                            if ($returnValue === 0) {
                                                                                finalize();
                                                                            } else {
                                                                                $callback(this.notifications.visual_graph_process_failed,
                                                                                    false);
                                                                            }
                                                                        });
                                                                } else {
                                                                    $callback(this.notifications.failed_to_validate_visual_graph, false);
                                                                }
                                                            });
                                                    } else {
                                                        $callback(this.notifications.unable_to_create_visual_graph_instance, false);
                                                    }
                                                });
                                        } else {
                                            $callback(this.notifications.unable_to_load_runtime_library, false);
                                        }
                                    });
                            });
                    }
                });
            };
            if ($inLoop) {
                this.saveRuntimeConfig(true, "", ($status : boolean) : void => {
                    if ($status) {
                        run();
                    } else {
                        $callback(this.notifications.unable_to_save_runtime_configuration, false);
                    }
                });
            } else {
                run();
            }
        }

        public Stop($force : boolean, $callback : ($message : string, $status? : boolean) => void) : void {
            if (!this.useFFIRunner) {
                const terminal : TerminalConnector = this.isRemote ? this.remoteTerminal : this.terminal;
                this.saveRuntimeConfig(false, "", ($status : boolean) : void => {
                    const forceKill : any = ($callback : ($status : boolean) => void) : void => {
                        let cmd : string = "taskkill";
                        let args : string[] = ["/F", "/IM", "\"" + this.targetName + ".exe\"", "/T"];
                        if (this.platform === PlatformType.LINUX || this.platform === PlatformType.IMX) {
                            cmd = "pkill";
                            args = ["-9", "\"" + this.targetName + "\""];
                        }
                        terminal
                            .Spawn(cmd, args)
                            .OnMessage(($data : string) : void => {
                                this.printHandler($data);
                            })
                            .Then(($exitCode : number, $stdout : string, $stderr : string) : void => {
                                $callback($exitCode === 0 ||
                                    StringUtils.ContainsIgnoreCase($stdout + $stderr, " not found.") ||
                                    StringUtils.ContainsIgnoreCase($stdout + $stderr, " no running instance of the task."));
                            });
                    };
                    if (!$force) {
                        this.forceKilled = false;
                        if ($status) {
                            $callback(this.notifications.stopped_successfully, true);
                        } else {
                            forceKill(($status : boolean) : void => {
                                if ($status) {
                                    $callback(this.notifications.force_stop_send_successfully, true);
                                } else {
                                    $callback(this.notifications.force_stop_failed, false);
                                }
                            });
                        }
                    } else {
                        this.forceKilled = true;
                        forceKill(($status : boolean) : void => {
                            if ($status) {
                                $callback(this.notifications.stopped_successfully, true);
                            } else {
                                $callback(this.notifications.stop_failed, false);
                            }
                        });
                    }
                });
            } else {
                this.ffi
                    .InvokeMethod("StopGraph", LiveContentArgumentType.RETURN_VOID)
                    .Then(() : void => {
                        $callback(this.notifications.stopped_successfully, true);
                    });
            }
        }

        public SelectDisplay($name : string, $callback : ($status : boolean) => void) : void {
            this.saveRuntimeConfig(true, $name, $callback);
        }

        public Clean($callback : ($status : boolean) => void) : void {
            let fileSystem : FileSystemHandlerConnector = this.fileSystem;
            let cachePath : string = this.cachePath;
            let projectPath : string = this.projectPath;
            if (this.isRemote) {
                fileSystem = this.remoteFileSystem;
                cachePath = this.remoteCachePath;
                projectPath = this.remoteProjectPath;
            }
            fileSystem.Delete(cachePath)
                .Then(($statusCache : boolean) => {
                    fileSystem.Delete(projectPath + "/build")
                        .Then(($statusBuild : boolean) => {
                            $callback($statusCache && $statusBuild);
                        });
                });
        }

        public Synchronize($callback : ($message : string, $status : boolean) => void) : void {
            const finalize : any = ($status : boolean) : void => {
                if ($status) {
                    $callback(StringUtils.Format(this.notifications.synchronizing__0__with__1__succeeded,
                        this.projectPath, this.remoteProjectPath), true);
                } else {
                    $callback(StringUtils.Format(this.notifications.synchronizing__0__with__1__failed,
                        this.projectPath, this.remoteProjectPath), false);
                }
            };

            this.prepareSdk(($message : string, $status? : boolean) : void => {
                if (ObjectValidator.IsSet($status)) {
                    if ($status) {
                        this.printHandler(this.notifications.runtime_sdk_is_up_to_date__starting_synchronization);
                        if (this.isAgent) {
                            this.agentSync([
                                this.projectPath + "/**/*",
                                "!" + this.projectPath + "/.visionsdk/**/*",
                                "!" + this.projectPath + "/build/**/*",
                                "!" + this.projectPath + "/build_cache/**/*",
                                "!" + this.projectPath + "/data/output/**/*"
                            ], true, finalize);
                        } else {
                            this.remoteManager.Deploy(this.projectPath + "/*", this.remoteProjectPath, finalize);
                        }
                    } else {
                        $callback($message, $status);
                    }
                } else {
                    this.printHandler($message);
                }
            });
        }

        public RemoveSDK($callback : ($status : boolean) => void) : void {
            const fileSystem : FileSystemHandlerConnector = this.isRemote ? this.remoteFileSystem : this.fileSystem;

            fileSystem.Delete(this.sdkPath).Then($callback);
        }

        public getProducts($callback : ($message : string, $status : boolean) => void) : void {
            const finalize : any = ($status : boolean) : void => {
                if ($status) {
                    $callback(StringUtils.Format(this.notifications.synchronizing__0__with__1__succeeded,
                        this.remoteProjectPath, this.projectPath), true);
                } else {
                    $callback(StringUtils.Format(this.notifications.synchronizing__0__with__1__failed,
                        this.remoteProjectPath, this.projectPath), false);
                }
            };
            if (this.isAgent) {
                this.agentSync([
                    this.remoteProjectPath + "/data/output/**/*"
                ], false, finalize);
            } else {
                const syncFolders : string[] = [
                    "data/output"
                ];
                const syncNextFolder : any = ($index : number) : void => {
                    if ($index < syncFolders.length) {
                        this.fileSystem
                            .CreateDirectory(this.projectPath + "/" + syncFolders[$index])
                            .Then(($success : boolean) : void => {
                                if ($success) {
                                    this.remoteManager.Deploy(
                                        this.remoteProjectPath + "/" + syncFolders[$index] + "/*",
                                        this.projectPath + "/" + syncFolders[$index],
                                        ($status : boolean) : void => {
                                            if ($status) {
                                                syncNextFolder($index + 1);
                                            } else {
                                                finalize($status);
                                            }
                                        }, false);
                                } else {
                                    finalize(false);
                                }
                            });
                    } else {
                        finalize(true);
                    }
                };
                syncNextFolder(0);
            }
        }

        private generateMakefiles($callback : ($status : boolean, $message? : string) => void) : void {
            let fileSystem : FileSystemHandlerConnector = this.fileSystem;
            let terminal : TerminalConnector = this.terminal;
            let cachePath : string = this.cachePath;
            if (this.isRemote) {
                fileSystem = this.remoteFileSystem;
                terminal = this.remoteTerminal;
                cachePath = this.remoteCachePath;
            }
            fileSystem
                .CreateDirectory(cachePath)
                .Then(($status : boolean) : void => {
                    if ($status) {
                        let args : string[] = [];
                        const options : TerminalOptions = {
                            cwd: cachePath
                        };

                        if (this.platform === PlatformType.WIN_GCC) {
                            args = [
                                "-G", "\"MinGW Makefiles\"",
                                "-DCMAKE_BUILD_TYPE=Debug",
                                "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                                "-DTARGET_PLATFORM=win",
                                ".."
                            ];
                            options.env = {
                                PATH: StringUtils.Replace(this.cmakePath, "/", "\\") + ";" +
                                    StringUtils.Replace(this.compilerPath, "/", "\\") + ";" +
                                    "%PATH%"
                            };
                        } else if (this.platform === PlatformType.MSVC) {
                            args = [
                                "-G", "\"Visual Studio 14 2015 Win64\"",
                                "-DCMAKE_C_COMPILER=\"" + this.compilerPath + "\"",
                                "-DCMAKE_CXX_COMPILER=\"" + this.compilerPath + "\"",
                                "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                                "-DTARGET_PLATFORM=msvc",
                                ".."
                            ];
                            options.env = {
                                PATH: StringUtils.Replace(this.cmakePath, "/", "\\") + ";" +
                                    "%PATH%"
                            };
                        } else if (this.platform === PlatformType.LINUX) {
                            args = [
                                "-G", "\"Unix Makefiles\"",
                                "-DCMAKE_BUILD_TYPE=Debug",
                                "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                                "-DTARGET_PLATFORM=linux",
                                ".."
                            ];
                            options.env = {
                                PATH: this.cmakePath
                            };
                        } else if (this.platform === PlatformType.IMX) {
                            args = [
                                "-G", "\"Unix Makefiles\"",
                                "-DCMAKE_BUILD_TYPE=Debug",
                                "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                                "-DTARGET_PLATFORM=arm",
                                ".."
                            ];
                        } else {
                            $callback(false, this.notifications.unknown_platform);
                        }
                        this.printHandler("> execute cmake with: " + JSON.stringify(args));
                        terminal
                            .Spawn("cmake", args, options)
                            .OnMessage(($data : string) : void => {
                                this.printHandler($data);
                            })
                            .Then(($exitCode : number) : void => {
                                if ($exitCode === 0) {
                                    $callback(true);
                                } else {
                                    fileSystem.Delete(cachePath).Then(() : void => {
                                        $callback(false, this.notifications.unable_to_generate_cpp_make_files);
                                    });
                                }
                            });
                    } else {
                        $callback(false, this.notifications.unable_to_create_build_directory);
                    }
                });
        }

        private saveRuntimeConfig($running : boolean, $displayName : string = "", $callback : ($status : boolean) => void) : void {
            let fileSystem : FileSystemHandlerConnector = this.fileSystem;
            let projectPath : string = this.projectPath;
            if ((this.platform === PlatformType.LINUX || this.platform === PlatformType.IMX)) {
                fileSystem = this.remoteFileSystem;
                projectPath = this.remoteProjectPath;
            }
            projectPath += "/build";
            const writeConfig : any = () : void => {
                fileSystem
                    .Write(projectPath + "/runtime.config", "run=" + $running + "&vxDataName=\"" + $displayName + "\"")
                    .Then($callback);
            };
            fileSystem.Exists(projectPath).Then(($exists : boolean) : void => {
                if ($exists) {
                    writeConfig();
                } else {
                    fileSystem
                        .CreateDirectory(projectPath)
                        .Then(writeConfig);
                }
            });
        }

        private agentSync($search : string[], $toTarget : boolean, $callback : ($status : boolean) => void) : void {
            const sourceFileSystem : FileSystemHandlerConnector = $toTarget ? this.fileSystem : this.remoteFileSystem;
            const destinationFileSystem : FileSystemHandlerConnector = $toTarget ? this.remoteFileSystem : this.fileSystem;
            const sourcePath : string = $toTarget ? this.projectPath : this.remoteProjectPath;
            const destinationPath : string = $toTarget ? this.remoteProjectPath : this.projectPath;

            sourceFileSystem.Expand($search).Then(($paths : string[]) : void => {
                const syncNextPath : any = ($index : number) : void => {
                    if ($index < $paths.length) {
                        const validateSync : any = ($success : boolean) : void => {
                            if (!$success) {
                                $callback(false);
                            } else {
                                syncNextPath($index + 1);
                            }
                        };
                        const source : string = $paths[$index];
                        const destination : string = destinationPath + "/" + StringUtils.Remove(source, sourcePath + "/");
                        sourceFileSystem.IsFile(source).Then(($success : boolean) : void => {
                            if ($success) {
                                this.printHandler("> sync from " + source + " to " + destination);
                                sourceFileSystem.Read(source, true).Then(($data : string) : void => {
                                    if (ObjectValidator.IsEmptyOrNull($data)) {
                                        this.printHandler(">> skipped file with empty content");
                                        syncNextPath($index + 1);
                                    } else {
                                        destinationFileSystem.Write(destination, $data, false, true).Then(validateSync);
                                    }
                                });
                            } else {
                                destinationFileSystem.CreateDirectory(destination).Then(validateSync);
                            }
                        });
                    } else {
                        $callback(true);
                    }
                };
                syncNextPath(0);
            });
        }

        private validateSdk($sdkPath : string, $callback : ($status : boolean) => void) : void {
            const fileSystem : FileSystemHandlerConnector = this.isRemote ? this.remoteFileSystem : this.fileSystem;
            fileSystem.Exists($sdkPath).Then(($status : boolean) : void => {
                if ($status) {
                    fileSystem.Expand($sdkPath + "/**/*.*").Then(($paths : string[]) : void => {
                        $callback($paths.length > 200);  // todo better check over SHA, this needs changes in lib distribution
                    });
                } else {
                    $callback(false);
                }
            });
        }

        private lockSdk($callback : ($status : boolean) => void) : void {
            const fileSystem : FileSystemHandlerConnector = this.isRemote ? this.remoteFileSystem : this.fileSystem;
            const lockerPath : string = this.sdkPath + "/locker.lock";
            fileSystem.Exists(lockerPath).Then(($status : boolean) : void => {
                if ($status) {
                    // check for locker agent and expiration time
                    fileSystem.Read(lockerPath).Then(($lockerOwner : string) : void => {
                        if ($lockerOwner === this.userID) {
                            $callback(true);
                        } else {
                            $callback(false);
                        }
                    });
                } else {
                    fileSystem.Write(lockerPath, this.userID).Then(() : void => {
                        $callback(true);
                    });
                }
            });
        }

        private unlockSdk($callback : () => void) : void {
            const fileSystem : FileSystemHandlerConnector = this.isRemote ? this.remoteFileSystem : this.fileSystem;
            const lockerPath : string = this.sdkPath + "/locker.lock";
            fileSystem.Delete(lockerPath).Then($callback);
        }

        private prepareSdk($callback : ($message : string, $status? : boolean) => void) : void {
            const fileSystem : FileSystemHandlerConnector = this.isRemote ? this.remoteFileSystem : this.fileSystem;
            const cleanUp : any = ($archivePath : string, $sdkPath : string) : void => {
                this.validateSdk($sdkPath, ($status : boolean) : void => {
                    if ($status) {
                        this.remoteFileSystem.Delete($archivePath).Then(($status : boolean) : void => {
                            if ($status) {
                                $callback(this.notifications.finished_preparing_remote_runtime_sdk, true);
                            } else {
                                $callback(this.notifications.unable_to_clean_up_sync_folder, false);
                            }
                        });
                    } else {
                        $callback(this.notifications.preparation_of_runtime_sdk_has_failed, false);
                    }
                });
            };
            const download : any = ($handler : ($finalize : () => void) => void) : void => {
                let reported : number = 0;
                ToolchainManager
                    .DownloadSDK(this.isAgent ? this.remoteFileSystem : this.fileSystem,
                        this.environment.serverUrl,
                        this.environment.sdkVersion,
                        this.platform,
                        this.environment.runtimeVersion)
                    .OnStart(() : void => {
                        $callback(this.notifications.downloading_runtime_sdk);
                    })
                    .OnChange(($args : ProgressEventArgs) : void => {
                        const percentage : number = Convert.ToFixed(100 / $args.RangeEnd() * $args.CurrentValue(), 0);
                        if (percentage % 10 === 0 && percentage > reported) {
                            reported = percentage;
                            this.printHandler(StringUtils.Format(this.notifications.downloaded__0__, percentage + ""));
                        }
                    })
                    .OnError(($error : ErrorEventArgs) : void => {
                        LogIt.Debug("Failed to download runtime sdk: {0}", $error);
                        this.unlockSdk(() : void => {
                            $callback(this.notifications.preparation_of_runtime_sdk_has_failed);
                        });
                    })
                    .Then(($path : string) : void => {
                        let output : string = this.sdkPath;
                        if (StringUtils.EndsWith(output, "VisionSDK")) {
                            output = StringUtils.Substring(output, 0, output.length - "/VisionSDK".length);
                        }
                        $handler(() : void => {
                            if (!this.isRemote || this.isAgent) {
                                fileSystem
                                    .Unpack($path, {output})
                                    .OnError(() : void => {
                                        this.unlockSdk(() : void => {
                                            $callback(this.notifications.preparation_of_runtime_sdk_has_failed);
                                        });
                                    })
                                    .Then(() : void => {
                                        if (this.isAgent) {
                                            cleanUp($path, output);
                                        } else {
                                            $callback(this.notifications.finished_preparing_runtime_sdk, true);
                                        }
                                    });
                            } else {
                                let zipPath : string = "/var/tmp/vision-sdk-studio";
                                this.remoteManager.Deploy($path, zipPath, ($status : boolean) : void => {
                                    if ($status) {
                                        this.fileSystem.Delete($path)
                                            .Then(() : void => {
                                                zipPath = zipPath + StringUtils.Substring($path, StringUtils.IndexOf($path, "/", false));
                                                this.remoteFileSystem
                                                    .Unpack(zipPath, {output})
                                                    .Then(() : void => {
                                                        cleanUp(zipPath, output);
                                                    });
                                            });
                                    } else {
                                        $callback(this.notifications.unable_to_upload_runtime_sdk, false);
                                    }
                                });
                            }
                        });
                    });
            };
            this.validateSdk(this.sdkPath, ($status : boolean) : void => {
                if ($status) {
                    $callback(this.notifications.runtime_sdk_already_exists, true);
                } else {
                    this.lockSdk(($status : boolean) : void => {
                        if (!$status) {
                            // todo this was prepared for parallelism in connector, to be replaced by periodical check for available sdk
                            $callback(
                                this.notifications.runtime_sdk_is_locked_on_target_due_to_running_download_task_initiated_by_another_client,
                                false);
                        } else {
                            download(($finalize : () => void) : void => {
                                this.unlockSdk(() : void => {
                                    $finalize();
                                });
                            });
                        }
                    });
                }
            });
        }
    }
}
