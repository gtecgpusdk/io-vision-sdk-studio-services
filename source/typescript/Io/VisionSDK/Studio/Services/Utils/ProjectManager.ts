/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import CppCodeGenerator = Io.VisionSDK.Studio.Services.CodeGenerator.Core.CppCodeGenerator;
    import IProjectEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IProjectEnvironment;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import IConnectionSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettingsValue;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IToolchainSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettingsValue;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IStatus = Io.VisionSDK.Studio.Services.Interfaces.IStatus;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import ITaskManagerEnvironment = Io.VisionSDK.Studio.Services.Interfaces.ITaskManagerEnvironment;
    import IGlobalSettings = Io.VisionSDK.Studio.Services.Interfaces.DAO.IGlobalSettings;
    import StatusType = Io.VisionSDK.Studio.Services.Enums.StatusType;
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;

    export class ProjectManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private taskManager : TaskManager;
        private readonly env : IProjectEnvironment;
        private projectFolder : string;
        private appRootFolder : string;
        private readonly historyPersistence : HistoryPersistenceManager;
        private readonly settingsPersistence : SettingsPersistenceManager;
        private errorHandler : ($message : string) => void;
        private printHandler : ($message : string) => void;
        private errorNotificationHandler : () => void;
        private isExampleLoaded : boolean;
        private notifications : IMainPageNotifications;
        private connections : ArrayList<RemoteTargetManager>;
        private toolchainPersistence : IPersistenceHandler;

        constructor($appRootFolder : string, $environment : IProjectEnvironment) {
            super();
            this.env = $environment;
            this.appRootFolder = $appRootFolder;
            this.isExampleLoaded = false;
            this.notifications = this.env.dao.getPageConfiguration().notifications;
            this.historyPersistence = new HistoryPersistenceManager(this.env.fileSystem);
            this.settingsPersistence = new SettingsPersistenceManager(this.appRootFolder, this.env.dao.getSettingsDao(),
                this.env.fileSystem, this.env.terminal);
            this.connections = new ArrayList<RemoteTargetManager>();
            this.toolchainPersistence = PersistenceFactory.getPersistence(this.getClassName());
            this.printHandler = ($message : string) : void => {
                // default event handler
            };
            this.errorHandler = ($message : string) : void => {
                // default event handler
            };
            this.errorNotificationHandler = () : void => {
                // default event handler
            };
        }

        public setOnPrint($callback : ($message : string) => void) : void {
            this.printHandler = $callback;
        }

        public setOnError($callback : ($message : string) => void) : void {
            this.errorHandler = $callback;
        }

        public setOnErrorNotification($callback : ($message? : string) => void) : void {
            this.errorNotificationHandler = $callback;
        }

        public getProjectFolder() : string {
            return this.projectFolder;
        }

        public getAppFolder() : string {
            return this.appRootFolder;
        }

        public TaskManager($value? : TaskManager) : TaskManager {
            if (ObjectValidator.IsSet($value)) {
                this.taskManager = $value;
            }
            return this.taskManager;
        }

        public getHistoryPersistence() : HistoryPersistenceManager {
            return this.historyPersistence;
        }

        public getSettingsPersistence() : SettingsPersistenceManager {
            return this.settingsPersistence;
        }

        public getEnvironment() : IProjectEnvironment {
            return this.env;
        }

        public IsExampleLoaded() : boolean {
            return this.isExampleLoaded;
        }

        public IsBuildable() : boolean {
            return this.IsInitialized();
        }

        public IsInitialized() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.taskManager);
        }

        public SaveProject($callback? : ($message : IStatus) => void, $persistPath : boolean = true) : void {
            if (!ObjectValidator.IsFunction($callback)) {
                $callback = () : void => {
                    // default callback
                };
            }

            $callback({message: this.notifications.saving_visual_graph, type: StatusType.PROGRESS});
            this.saveConfiguration(this.env.dao.getVisualGraphPath(), this.env.dao.ToJsonp(),
                ($message : string, $success : boolean) : void => {
                    if ($success) {
                        if ($success && $persistPath) {
                            this.env.dao.PersistProjectPath(this.projectFolder);
                            this.isExampleLoaded = false;
                        }
                        this.saveMetadata(($status : IStatus) : void => {
                            if (ObjectValidator.IsSet($status.success)) {
                                if ($status.success) {
                                    $callback({
                                        message: StringUtils.Format(
                                            this.notifications.project__0__was_saved_successfully,
                                            this.env.dao.getVisualGraph().Name()),
                                        success: $status.success,
                                        type   : StatusType.SUCCESS
                                    });
                                } else {
                                    $callback({
                                        message: StringUtils.Format(this.notifications
                                                .project__0__was_saved_successfully__failed_to_save_metadata,
                                            this.env.dao.getVisualGraph().Name()),
                                        success: $status.success,
                                        type   : StatusType.ERROR
                                    });
                                }
                            }
                        });
                    } else {
                        $callback({
                            message: StringUtils.Format(
                                this.notifications.failed_to_save_project__0__,
                                this.env.dao.getVisualGraph().Name()),
                            success: $success,
                            type   : StatusType.ERROR
                        });
                    }
                });
        }

        public CreateProject($descriptor : IProjectSettingsValue, $callback : ($status : IStatus) => void,
                             $persistPath? : boolean) : void {
            this.projectFolder = $descriptor.path;
            const projectPath : string = this.projectFolder + "/VisualGraph.jsonp";
            $callback({message: this.notifications.creating_project, type: StatusType.PROGRESS});
            this.env.fileSystem
                .Copy(this.appRootFolder + "/" +
                    this.env.dao.getPageConfiguration().globalSettings.templates[$descriptor.vxVersion], projectPath)
                .Then(($success : boolean) : void => {
                    const callback : any = ($success : boolean) : void => {
                        if ($success) {
                            $callback({
                                message: StringUtils.Format(this.notifications.project__0__was_created_successfully,
                                    this.env.dao.getVisualGraph().Name()),
                                success: $success,
                                type   : StatusType.SUCCESS
                            });
                        } else {
                            $callback({
                                message: StringUtils.Format(this.notifications.failed_to_create__project__0,
                                    this.env.dao.getVisualGraph().Name()),
                                success: $success,
                                type   : StatusType.ERROR
                            });
                        }
                    };

                    if ($success) {
                        this.env.fileSystem.Exists(projectPath).Then(($success : boolean) : void => {
                            if ($success) {
                                this.LoadProject(projectPath, ($status : IStatus) : void => {
                                    if (ObjectValidator.IsSet($status.success)) {
                                        if ($status.success) {
                                            this.isExampleLoaded = false;
                                            this.UpdateProject($descriptor, ($status : IStatus) => {
                                                if (ObjectValidator.IsSet($status.success)) {
                                                    callback($status.success);
                                                }
                                            });
                                        } else {
                                            callback($status.success);
                                        }
                                    }
                                }, $persistPath);
                            } else {
                                callback($success);
                            }
                        });
                    } else {
                        callback($success);
                    }
                });
        }

        public UpdateProject($descriptor : IProjectSettingsValue, $callback : ($status : IStatus) => void) : void {
            const graph : VisualGraph = this.env.dao.getVisualGraph();
            graph.Name($descriptor.name);
            graph.Description($descriptor.description);
            this.projectFolder = StringUtils.Remove($descriptor.path, "VisualGraph.jsonp");
            this.env.dao.getVisualGraphDAO().setConfigurationPath(this.projectFolder + "/VisualGraph.jsonp");
            this.updateMetadata();
            this.SaveProject(($status : IStatus) : void => {
                $callback($status);
            });
        }

        public LoadProject($path : string, $callback : ($status : IStatus) => void, $persistPath : boolean = true) : void {
            $callback({message: this.notifications.loading_project, type: StatusType.PROGRESS});
            if (!StringUtils.EndsWith($path, ".jsonp")) {
                if (!StringUtils.EndsWith($path, "/")) {
                    $path += "/";
                }
                $path += "VisualGraph.jsonp";
            }
            this.env.fileSystem.Exists($path)
                .Then(($success : boolean) : void => {
                    if ($success) {
                        BaseDAO.UnloadResource($path);
                        this.env.dao.LoadProject($path, () : void => {
                            this.projectFolder = StringUtils.Substring($path, 0,
                                StringUtils.IndexOf($path, "/", false));
                            this.loadMetadata(($status : IStatus) : void => {
                                if (ObjectValidator.IsSet($status.success)) {
                                    if ($status.success) {
                                        if ($persistPath) {
                                            this.env.dao.PersistProjectPath(this.projectFolder);
                                        }
                                        this.isExampleLoaded = false;
                                        this.InitTaskManager(($status : IStatus) : void => {
                                            if (ObjectValidator.IsSet($status.success)) {
                                                if ($status.success) {
                                                    $callback({
                                                        message: StringUtils.Format(this.notifications.project__0__was_loaded_successfully,
                                                            this.env.dao.getVisualGraph().Name()),
                                                        success: $status.success,
                                                        type   : StatusType.SUCCESS
                                                    });
                                                } else {
                                                    $callback($status);
                                                }
                                            }
                                        });
                                    } else {
                                        $callback({
                                            message: StringUtils.Format(this.notifications.failed_to_load_project,
                                                this.env.dao.getVisualGraph().Name()),
                                            success: $status.success,
                                            type   : StatusType.ERROR
                                        });
                                    }
                                } else {
                                    $callback($status);
                                }
                            });
                        }, true);
                    } else {
                        $callback({
                            message: this.notifications.failed_to_load_project,
                            success: $success,
                            type   : StatusType.ERROR
                        });
                    }
                });
        }

        public BuildProject($callback : ($status : IStatus) => void, $forceBuild? : boolean) : void {
            let cmakeClean : boolean = false;
            const process : any = () : void => {
                this.taskManager.Build(($message : string, $success? : boolean) : void => {
                    if (ObjectValidator.IsSet($success)) {
                        if ($success) {
                            $callback({
                                success: $success
                            });
                        } else if (!cmakeClean) {
                            this.taskManager.Clean(($cleanCacheStatus : boolean) : void => {
                                if ($cleanCacheStatus) {
                                    cmakeClean = true;
                                    process();
                                } else {
                                    $callback({
                                        message: this.notifications.unable_to_clean_CMake_cache,
                                        success: $success,
                                        type   : StatusType.ERROR
                                    });
                                }
                            });
                        } else {
                            $callback({
                                message: this.notifications.unable_to_build_generated_code,
                                success: $success,
                                type   : StatusType.ERROR
                            });
                        }
                    } else {
                        $callback({message: $message, type: StatusType.PROGRESS});
                    }
                });
            };

            this.SaveProject(($status : IStatus) : void => {
                if (ObjectValidator.IsSet($status.success)) {
                    if ($status.success) {
                        this.env.fileSystem
                            .Expand([this.projectFolder + "/source/**/*", this.projectFolder + "/include/**/*"])
                            .Then(($structurePre : string[]) : void => {
                                this.generateProject(this.projectFolder, this.taskManager.getPlatform(), [], this.taskManager.IsAgent(),
                                    ($status : IStatus, $generator : CppCodeGenerator, $structurePost : string[]) : void => {
                                        if ($status.success) {
                                            const cleanUp : string[] = [];
                                            if (!ObjectValidator.IsEmptyOrNull($structurePre) && ObjectValidator.IsArray($structurePre)) {
                                                $structurePre.forEach(($filePath : string) : void => {
                                                    if (!$structurePost.indexOf($filePath)) {
                                                        cleanUp.push($filePath);
                                                    }
                                                });
                                                const removeSource : any = ($index : number) : void => {
                                                    if ($index < cleanUp.length) {
                                                        this.env.fileSystem
                                                            .Delete(cleanUp[$index])
                                                            .Then(($success : boolean) : void => {
                                                                if ($success) {
                                                                    removeSource($index + 1);
                                                                } else {
                                                                    $callback({
                                                                        message: this.notifications.unable_to_clean_sources,
                                                                        success: $success,
                                                                        type   : StatusType.ERROR
                                                                    });
                                                                }
                                                            });
                                                    } else {
                                                        if ($forceBuild || !ObjectValidator.IsEmptyOrNull(cleanUp)) {
                                                            this.taskManager.Clean(($success : boolean) : void => {
                                                                if ($success) {
                                                                    cmakeClean = true;
                                                                    process();
                                                                } else {
                                                                    $callback({
                                                                        message: this.notifications.unable_to_clean_CMake_cache,
                                                                        success: $success,
                                                                        type   : StatusType.ERROR
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            process();
                                                        }
                                                    }
                                                };
                                                removeSource(0);
                                            } else {
                                                process();
                                            }
                                        } else {
                                            $callback($status);
                                        }
                                    });
                            });
                    } else {
                        $callback($status);
                    }
                }
            }, !this.isExampleLoaded);
        }

        public InspectExportPackage($platform : PlatformType) : string[] {
            const content : string[] = [
                "/VisionSDK",
                "/include",
                "/source"
            ];
            this.env.dao.getVisualGraph().getIoComs().foreach(($ioCom : VXNode) : void => {
                if (!ObjectValidator.IsEmptyOrNull($ioCom.getAttributeValue("path"))) {
                    let path : string = StringUtils.Replace($ioCom.getAttributeValue("path"), "\\", "/");
                    path = StringUtils.Replace(path, "//", "/");
                    if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.IMAGE_INPUT ||
                        $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                        $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DATA_INPUT) {
                        if (!StringUtils.StartsWith(path, "data/input/")) {
                            path = "/data/input/" + StringUtils.Substring(path, StringUtils.IndexOf(path, "/", false) + 1);
                            if (content.indexOf(path) !== -1) {
                                path = "/data/input/" + $ioCom.UniqueId() +
                                    StringUtils.Substring(path, StringUtils.IndexOf(path, ".", false));
                            }
                        }
                        if (!StringUtils.StartsWith(path, "/")) {
                            path = "/" + path;
                        }
                        if (content.indexOf(path) === -1) {
                            content.push(path);
                        }
                    }
                }
            });
            const extension : string =
                $platform === PlatformType.MSVC || $platform === PlatformType.WIN_GCC ? ".cmd" : ".sh";
            content.push(
                "/data/output",
                "/build" + extension,
                "/clean" + extension,
                "/run" + extension,
                "/CMakeLists.txt",
                "/README.txt",
                "/LICENSE.txt"
            );
            return content;
        }

        public ExportProject($platform : PlatformType, $ignore : string[], $callback : ($status : IStatus) => void,
                             $packagePath? : string, $openExportFolder : boolean = false) : void {
            $callback({message: this.notifications.collecting_export_information, type: StatusType.PROGRESS});
            const getExportPath : any = ($callback : ($path : string) => void) : void => {
                this.env.fileSystem.getTempPath().Then(($tmpPath : string) : void => {
                    $callback($tmpPath + "/vision-sdk-studio-export");
                });
            };
            const downloadSDK : any = ($destination : string, $onSuccess : () => void) : void => {
                ToolchainManager
                    .DownloadSDK(this.env.fileSystem,
                        this.env.dao.getPageConfiguration().globalSettings.hubUrl,
                        this.settingsPersistence.Settings().projectSettings.vxVersion,
                        $platform,
                        this.env.dao.getPageConfiguration().globalSettings.runtimeSdkVersion)
                    .OnStart(() : void => {
                        $callback({message: this.notifications.downloading_vision_sdk, type: StatusType.PROGRESS});
                    })
                    .Then(($path : string) : void => {
                        $callback({message: this.notifications.preparing_vision_sdk, type: StatusType.PROGRESS});
                        this.env.fileSystem
                            .Unpack($path, {output: $destination})
                            .Then($onSuccess);
                    });
            };
            const prepareSDK : any = ($sdkPath : string, $destination : string, $onSuccess : () => void) : void => {
                if ($ignore.indexOf("/VisionSDK") === -1) {
                    this.env.fileSystem
                        .Copy($sdkPath, $destination)
                        .Then(($success : boolean) : void => {
                            if ($success) {
                                $onSuccess();
                            } else {
                                $callback({message: this.notifications.unable_to_copy_sdk, type: StatusType.ERROR, success: $success});
                            }
                        });
                } else {
                    $onSuccess();
                }
            };
            const createPackage : any = ($source : string, $destination : string) : void => {
                $callback({message: this.notifications.creating_package, type: StatusType.PROGRESS});
                let extension : string = ".zip";
                if ($platform === PlatformType.LINUX) {
                    extension = ".tar.gz";
                } else if ($platform === PlatformType.IMX) {
                    extension = ".tar.bz2";
                }
                if (($platform === PlatformType.LINUX || $platform === PlatformType.IMX) &&
                    StringUtils.EndsWith($destination, ".zip")) {
                    extension = ".zip";
                }
                $destination = StringUtils.Remove($destination, ".zip", ".tar.gz", ".tar.bz2", ".tar");
                if (!StringUtils.EndsWith($destination, extension)) {
                    $destination += extension;
                }
                this.env.fileSystem.Delete($destination).Then(() : void => {
                    this.env.fileSystem
                        .Pack($source, {output: $destination, type: StringUtils.Remove(extension, ".")})
                        .Then(() : void => {
                            this.env.fileSystem.Delete($source).Then(() : void => {
                                if ($openExportFolder) {
                                    const packageFolder : string = StringUtils.Substring($destination, 0,
                                        StringUtils.IndexOf($destination, "/", false));
                                    this.env.terminal.Execute("cmd", [
                                        "/c", "\"start", "\"\"",
                                        "\"" + StringUtils.Replace(packageFolder, "/", "\\") + "\"\""
                                    ]).Then(() : void => {
                                        $callback({
                                            message: this.notifications.project_was_successfully_exported,
                                            success: true,
                                            type   : StatusType.SUCCESS
                                        });
                                    });
                                } else {
                                    $callback({
                                        message: this.notifications.project_was_successfully_exported,
                                        success: true,
                                        type   : StatusType.SUCCESS
                                    });
                                }
                            });
                        });
                });
            };
            getExportPath(($path : string) : void => {
                let projectPath : string = $path + "/" + this.env.dao.getVisualGraph().Name();
                if (!ObjectValidator.IsEmptyOrNull($packagePath)) {
                    $packagePath = StringUtils.Replace($packagePath, "\\", "/");
                    projectPath = $path + "/" + StringUtils.Substring($packagePath, StringUtils.IndexOf($packagePath, "/", false) + 1);
                    projectPath = StringUtils.Remove(projectPath, ".zip", ".tar");
                } else {
                    $packagePath = projectPath;
                }
                const sdkPath : string = $path + "/" + $platform + "-" +
                    StringUtils.Replace(Loader.getInstance().getEnvironmentArgs().getProjectVersion(), ".", "-");
                this.env.fileSystem.Delete(projectPath).Then(() : void => {
                    this.generateProject(projectPath, $platform, $ignore, true,
                        ($status : IStatus, $generator : CppCodeGenerator) : void => {
                            if ($status.success) {
                                $generator.GenerateScripts(($success : boolean) : void => {
                                    if ($success) {
                                        this.env.fileSystem.Exists(sdkPath).Then(($success : boolean) : void => {
                                            const composeExport : any = () : void => {
                                                prepareSDK(sdkPath, projectPath, () : void => {
                                                    createPackage(projectPath, $packagePath);
                                                });
                                            };
                                            if (!$success) {
                                                downloadSDK(sdkPath, () : void => {
                                                    composeExport();
                                                });
                                            } else {
                                                composeExport();
                                            }
                                        });
                                    } else {
                                        $callback({
                                            message: this.notifications.unable_to_generate_scripts,
                                            success: $success,
                                            type   : StatusType.ERROR
                                        });
                                    }
                                });
                            } else {
                                $callback($status);
                            }
                        });
                });
            });
        }

        public CreateInitialWorkspace($callback : ($status : IStatus) => void,
                                      $loadPersisted : boolean = true) : void {
            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
            const globalSettings : IGlobalSettings = this.env.dao.getPageConfiguration().globalSettings;

            let projectFolder : string = this.env.dao.getVisualGraphPath();
            projectFolder = StringUtils.Substring(projectFolder, 0, StringUtils.IndexOf(projectFolder, "/", false));
            projectFolder = StringUtils.Substring(projectFolder, StringUtils.IndexOf(projectFolder, "/", false) + 1);
            if (projectFolder === "Configuration") {
                projectFolder = StringUtils.Replace(this.env.dao.getVisualGraph().Name(), " ", "_");
            }

            const initProject : any = ($persistedPath : string) : void => {
                this.env.dao.getVisualGraphDAO().setConfigurationPath(this.projectFolder + "/VisualGraph.jsonp");
                this.loadMetadata(($status : IStatus) : void => {
                    if (ObjectValidator.IsSet($status.success)) {
                        if ($status.success) {
                            if (ObjectValidator.IsEmptyOrNull($persistedPath)) {
                                this.SaveProject(($status : IStatus) : void => {
                                    if (ObjectValidator.IsSet($status.success)) {
                                        if ($status.success) {
                                            $callback({
                                                message: this.notifications.initial_workspace_created,
                                                success: $status.success,
                                                type   : StatusType.SUCCESS
                                            });
                                        } else {
                                            $callback($status);
                                        }
                                    }
                                });
                            } else {
                                $callback({
                                    message: StringUtils.Format(this.notifications.project__0__was_loaded_successfully,
                                        this.env.dao.getVisualGraph().Name()),
                                    success: $status.success,
                                    type   : StatusType.SUCCESS
                                });
                            }
                        } else {
                            $callback($status);
                        }
                    }
                });
            };
            const startLocalSession : any = ($persistedPath : string) : void => {
                this.projectFolder = ObjectValidator.IsEmptyOrNull($persistedPath) ?
                    StringUtils.Remove(this.appRootFolder, "/target") + "/designer_cache/" + projectFolder :
                    $persistedPath;
                initProject($persistedPath);
            };
            const initEnvironment : any = ($appDataPath : string, $persistedPath? : string) : void => {
                $callback({
                    message: ObjectValidator.IsEmptyOrNull($persistedPath) ?
                        this.notifications.creating_initial_workspace :
                        this.notifications.loading_project,
                    type   : StatusType.PROGRESS
                });

                if (request.IsWuiHost()) {
                    this.appRootFolder = $appDataPath;
                    this.projectFolder = ObjectValidator.IsEmptyOrNull($persistedPath) ? "resource/data/" + projectFolder : $persistedPath;
                    initProject($persistedPath);
                } else if (request.IsWuiJre()) {
                    this.env.fileSystem.Exists(this.appRootFolder).Then(($success : boolean) : void => {
                        if (!$success) {
                            this.appRootFolder = $appDataPath + "/NXP/Vision SDK Studio";
                            this.projectFolder = ObjectValidator.IsEmptyOrNull($persistedPath) ?
                                this.appRootFolder + "/" + projectFolder : $persistedPath;
                            initProject($persistedPath);
                        } else {
                            startLocalSession($persistedPath);
                        }
                    });
                } else {
                    this.env.terminal
                        .Execute("wui", ["--path"])
                        .Then(($exitCode : number, $stdout : string) : void => {
                            if ($exitCode === 0) {
                                let modulesPath : string =
                                    StringUtils.Remove($stdout, "\r\n", "\n", "\\\"").trim() + "/external_modules";
                                modulesPath = StringUtils.Replace(modulesPath, "\\", "/");
                                modulesPath = StringUtils.Replace(modulesPath, "//", "/");
                                const mingwPath : string = modulesPath + "/msys2";
                                const cmakePath : string = modulesPath + "/cmake/bin";
                                this.env.fileSystem.Exists(mingwPath).Then(($success : boolean) : void => {
                                    if ($success) {
                                        globalSettings.msysPath = mingwPath;
                                        this.env.fileSystem.Exists(cmakePath).Then(($success : boolean) : void => {
                                            if ($success) {
                                                globalSettings.cmakePath = cmakePath;
                                            }
                                            startLocalSession($persistedPath);
                                        });
                                    } else {
                                        startLocalSession($persistedPath);
                                    }
                                });
                            } else {
                                startLocalSession($persistedPath);
                            }
                        });
                }
            };
            const initWorkspace : any = ($appDataPath : string) : void => {
                globalSettings.msysPath = StringUtils.Replace(globalSettings.msysPath, "%localappdata%", $appDataPath);
                globalSettings.cmakePath = StringUtils.Replace(globalSettings.cmakePath, "%localappdata%", $appDataPath);

                if ($loadPersisted) {
                    this.env.dao.getPersistedProjectPath(($persistedPath : string) : void => {
                        initEnvironment($appDataPath, $persistedPath);
                    });
                } else {
                    initEnvironment($appDataPath);
                }
            };

            if (request.IsWuiHost()) {
                initWorkspace(this.env.dao.getPageConfiguration().globalSettings.hostTargetPath);
            } else {
                ToolchainManager.getLocalAppDataPath(this.env.terminal, ($appDataPath : string) : void => {
                    initWorkspace($appDataPath);
                });
            }
        }

        public Settings($settings? : ISettings) : ISettings {
            if (ObjectValidator.IsSet($settings)) {
                this.settingsPersistence.Settings($settings);
                const graph : VisualGraph = this.env.dao.getVisualGraph();
                graph.Name($settings.projectSettings.name);
                graph.Description($settings.projectSettings.description);
            }
            return this.settingsPersistence.Settings();
        }

        public setBuildTarget($name : string, $callback? : ($status : IStatus) => void) : void {
            const settings : ISettings = this.Settings();
            settings.selectedPlatform = $name;
            this.Settings(settings);
            this.InitTaskManager(($status : IStatus) : void => {
                this.settingsPersistence.SaveGlobal(() : void => {
                    if ($status.success) {
                        $callback({
                            message: this.notifications.build_platform_changed_successfully,
                            success: $status.success,
                            type   : StatusType.SUCCESS
                        });
                    } else {
                        $callback($status);
                    }
                });
            });
        }

        public LoadExample($descriptor : IProjectSettingsValue, $callback : ($status : IStatus) => void) : void {
            $callback({
                message: StringUtils.Format(this.notifications.loading_example, $descriptor.name),
                type   : StatusType.PROGRESS
            });

            const callback : any = ($success : boolean) : void => {
                if ($success) {
                    $callback({
                        message: StringUtils.Format(this.notifications.example__0__was_loaded_successfully,
                            this.env.dao.getVisualGraph().Name()),
                        success: $success,
                        type   : StatusType.SUCCESS
                    });
                } else {
                    $callback({
                        message: StringUtils.Format(this.notifications.failed_to_load__example__0,
                            this.env.dao.getVisualGraph().Name()),
                        success: $success,
                        type   : StatusType.ERROR
                    });
                }
            };
            const load : any = ($path : string) : void => {
                this.projectFolder = $path + "/vision-sdk-studio-examples/" + StringUtils.getSha1($descriptor.path);
                const examplePath : string = this.projectFolder + "/VisualGraph.jsonp";
                const prepareExample : any = () : void => {
                    this.env.fileSystem.Delete(examplePath).Then(($success) : void => {
                        if ($success) {
                            this.env.fileSystem
                                .Read(this.appRootFolder + "/" + $descriptor.path)
                                .Then(($data : string) : void => {
                                    if (!ObjectValidator.IsEmptyOrNull($data)) {
                                        this.env.fileSystem
                                            .Write(examplePath, StringUtils.Replace($data, "{appPath}", this.appRootFolder))
                                            .Then(($success : boolean) : void => {
                                                if ($success) {
                                                    this.env.fileSystem.Exists(examplePath).Then(($success : boolean) : void => {
                                                        if ($success) {
                                                            this.isExampleLoaded = true;
                                                            this.LoadProject(examplePath, ($status : IStatus) : void => {
                                                                if (ObjectValidator.IsSet($status.success)) {
                                                                    this.isExampleLoaded = $status.success;
                                                                    callback($status.success);
                                                                }
                                                            });
                                                        } else {
                                                            callback($success);
                                                        }
                                                    });
                                                } else {
                                                    callback($success);
                                                }
                                            });
                                    } else {
                                        callback(false);
                                    }
                                });
                        } else {
                            callback($success);
                        }
                    });
                };
                this.env.fileSystem.Exists(this.projectFolder).Then(($success : boolean) : void => {
                    if ($success) {
                        prepareExample();
                    } else {
                        this.env.fileSystem.CreateDirectory(this.projectFolder).Then(($success : boolean) : void => {
                            if ($success) {
                                prepareExample();
                            } else {
                                callback($success);
                            }
                        });
                    }
                });
            };
            if (Loader.getInstance().getHttpManager().getRequest().IsWuiHost()) {
                load("resource/data");
            } else {
                this.env.fileSystem.getTempPath().Then(load);
            }
        }

        public OpenProjectFolder() : void {
            this.env.terminal.Execute("cmd", [
                "/c", "\"start", "\"\"",
                "\"" + StringUtils.Replace(this.projectFolder, "/", "\\") + "\"\""
            ]);
        }

        public InitTaskManager($callback : ($status : IStatus) => void) : void {
            const settings : ISettings = this.Settings();
            this.taskManager = null;

            const getTaskManagerEnvironment : any = ($toolchain : IConnectionSettingsValue) : ITaskManagerEnvironment => {
                const env : ITaskManagerEnvironment = {
                    cmakePath     : this.env.dao.getPageConfiguration().globalSettings.cmakePath,
                    compilerPath  : this.env.dao.getPageConfiguration().globalSettings.msysPath + "/mingw64/bin",
                    notifications : this.env.dao.getPageConfiguration().notifications,
                    platform      : PlatformType.WIN_GCC,
                    runtimeVersion: this.env.dao.getPageConfiguration().globalSettings.runtimeSdkVersion,
                    sdkVersion    : settings.projectSettings.vxVersion,
                    serverUrl     : this.env.dao.getPageConfiguration().globalSettings.hubUrl
                };
                if (!ObjectValidator.IsEmptyOrNull($toolchain.platform)) {
                    env.platform = $toolchain.platform;
                }
                if (!ObjectValidator.IsEmptyOrNull($toolchain.agentName)) {
                    env.agentName = $toolchain.agentName;
                }
                if (!ObjectValidator.IsEmptyOrNull($toolchain.cmake)) {
                    env.cmakePath = StringUtils.Remove($toolchain.cmake, "/cmake.exe");
                }
                if (!ObjectValidator.IsEmptyOrNull($toolchain.compiler)) {
                    env.compilerPath = StringUtils.Remove($toolchain.compiler, "/g++.exe", "/MSBuild.exe");
                }
                if (!ObjectValidator.IsEmptyOrNull($toolchain.address)) {
                    env.hubUrl = $toolchain.address;
                }
                return env;
            };

            let isLocal : boolean = false;
            settings.toolchainSettings.options.forEach(($value : IToolchainSettingsValue) : boolean => {
                if (!ObjectValidator.IsSet($value.enabled)) {
                    $value.enabled = true;
                }
                if ($value.enabled && $value.name === settings.selectedPlatform) {
                    this.installBuildDependencies(settings, ($status : IStatus) : void => {
                        if ($status.success) {
                            this.taskManager = new TaskManager(settings.projectSettings.path, settings.projectSettings.name,
                                getTaskManagerEnvironment($value));
                            this.taskManager.setOnPrint(this.printHandler);
                            this.taskManager.setOnError(this.errorHandler);
                            this.taskManager.Init(() : void => {
                                $callback($status);
                            });
                        } else {
                            $callback($status);
                        }
                    });
                    isLocal = true;
                    return false;
                }
            });

            if (!isLocal) {
                let isDone : boolean = false;
                const callback : any = ($success : boolean, $name : string) : void => {
                    if (!$success && isDone) {
                        this.errorNotificationHandler();
                    } else {
                        if (!isDone) {
                            $callback({
                                message: StringUtils.Format($success ?
                                    this.notifications.connected_to_target__0__ : this.notifications.failed_to_connect_to_target__0__, $name
                                ),
                                success: $success,
                                type   : $success ? StatusType.SUCCESS : StatusType.ERROR
                            });
                            isDone = true;
                        }
                    }
                };

                settings.connectionSettings.options.forEach(($value : IConnectionSettingsValue) : boolean => {
                    if (!ObjectValidator.IsSet($value.enabled)) {
                        $value.enabled = true;
                    }
                    if ($value.enabled && $value.name === settings.selectedPlatform) {
                        const connection : RemoteTargetManager = new RemoteTargetManager({
                            agentName: $value.agentName,
                            host     : $value.address,
                            name     : $value.name,
                            pass     : $value.password,
                            user     : $value.username
                        }, {
                            homePath : this.appRootFolder + "/..",
                            hubUrl   : $value.address,
                            msysPath : this.env.dao.getPageConfiguration().globalSettings.msysPath,
                            serverUrl: this.env.dao.getPageConfiguration().globalSettings.hubUrl,
                            sshPort  : 22
                        });

                        $callback({message: this.notifications.connecting, type: StatusType.PROGRESS});
                        connection.setOnPrint(this.printHandler);
                        connection.setOnError(($message : string) : void => {
                            this.errorHandler($message);
                            callback(false, $value.name);
                        });
                        connection.Start(($success : boolean) : void => {
                            if ($success) {
                                this.taskManager = new TaskManager(settings.projectSettings.path, settings.projectSettings.name,
                                    getTaskManagerEnvironment($value), connection);
                                this.taskManager.setOnPrint(this.printHandler);
                                this.taskManager.setOnError(this.errorHandler);
                                this.taskManager.Init(() : void => {
                                    callback(true, $value.name);
                                });
                            } else {
                                callback(false, $value.name);
                            }
                        });
                        return false;
                    }
                });
            }
        }

        public IsPlatformInvalid($platform : string) : boolean {
            let isLocal : boolean = false;
            this.Settings().toolchainSettings.options.forEach(($value : IToolchainSettingsValue) : boolean => {
                if ($value.name === $platform) {
                    return !(isLocal = true);
                }
            });
            return isLocal && (!this.toolchainPersistence.Exists($platform) || this.toolchainPersistence
                .Variable($platform) !== this.env.environment.getProjectVersion());
        }

        private saveMetadata($callback? : ($status : IStatus) => void) : void {
            this.settingsPersistence.Save(this.env.dao.getVisualGraph().Id(), this.projectFolder, () : void => {
                this.historyPersistence.Save(this.env.dao.getVisualGraph().Id(), this.projectFolder, () : void => {
                    $callback({success: true});
                });
            });
        }

        private loadMetadata($callback? : ($status : IStatus) => void) : void {
            this.settingsPersistence.Init(this.env.dao.getVisualGraph().Id(), this.projectFolder, () : void => {
                this.historyPersistence.Init(this.env.dao.getVisualGraph().Id(), this.projectFolder, !this.isExampleLoaded,
                    () : void => {
                        this.updateMetadata();
                        $callback({success: true});
                    });
            });
        }

        private updateMetadata() : void {
            const settings : ISettings = this.settingsPersistence.Settings();
            settings.projectSettings = {
                description: this.env.dao.getVisualGraph().Description(),
                name       : this.env.dao.getVisualGraph().Name(),
                path       : this.projectFolder,
                vxVersion  : this.env.dao.getTemplates().version
            };
            let platformExists : boolean;
            [settings.connectionSettings.options, settings.toolchainSettings.options]
                .forEach(($options : IToolchainSettingsValue[]) : boolean => {
                    $options.forEach(($option : IToolchainSettingsValue) : boolean => {
                        if ($option.name === settings.selectedPlatform) {
                            platformExists = true;
                            return !platformExists;
                        }
                    });
                    return !platformExists;
                });
            if (!platformExists) {
                settings.selectedPlatform = !ObjectValidator.IsEmptyOrNull(settings.connectionSettings.options) ?
                    settings.connectionSettings.options[0].name : settings.toolchainSettings.options[0].name;
            }
            this.settingsPersistence.Settings(settings);
        }

        private installBuildDependencies($settings : ISettings, $callback : ($status : IStatus) => void) : void {
            if (this.IsPlatformInvalid($settings.selectedPlatform)) {
                $callback({message: this.notifications.toolchain_validation, type: StatusType.PROGRESS});
                new ToolchainManager()
                    .setOnPrint(this.printHandler)
                    .setOnError(($message : string) : void => {
                        $callback({
                            message: $message,
                            success: false,
                            type   : StatusType.ERROR
                        });
                        this.errorHandler($message);
                    })
                    .InstallBuildDependencies(this.env.dao.getInstallDao(),
                        this.env.dao.getPageConfiguration().globalSettings.msysPath,
                        this.env.dao.getPageConfiguration().globalSettings.cmakePath,
                        ($message : string, $success? : boolean) : void => {
                            if ($success) {
                                this.toolchainPersistence.Variable($settings.selectedPlatform,
                                    this.env.environment.getProjectVersion());
                            }
                            $callback({
                                message: $message,
                                success: $success,
                                type   : ObjectValidator.IsSet($success) ?
                                    ($success ? StatusType.SUCCESS : StatusType.ERROR) : StatusType.PROGRESS
                            });
                        });
            } else {
                $callback({
                    message: this.notifications.build_dependencies_are_up_to_date,
                    success: true,
                    type   : StatusType.INFO
                });
            }
        }

        private saveConfiguration($path : string, $data : string, $callback : ($message : string, $status : boolean) => void) : void {
            const writeConfig : any = () : void => {
                this.env.fileSystem
                    .Write($path, $data)
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            $callback(this.notifications.data_was_saved_successfully, true);
                        } else {
                            $callback(this.notifications.failed_to_save_data, false);
                        }
                    });
            };

            const projectFolder : string = StringUtils.Remove($path, "/VisualGraph.jsonp");
            this.env.fileSystem.Exists(projectFolder).Then(($exists : boolean) : void => {
                if ($exists) {
                    writeConfig();
                } else {
                    this.env.fileSystem
                        .CreateDirectory(projectFolder)
                        .Then(writeConfig);
                }
            });
        }

        private generateProject($projectPath : string, $platform : PlatformType, $ignore : string[], $convertIoComs : boolean,
                                $callback : ($status : IStatus, $generator? : CppCodeGenerator, $filesMap? : string[]) => void) : void {
            const ioComsRegister : any[] = [];
            const prepareIOComs : any = ($destination : string, $onSuccess : () => void) : void => {
                const copyFiles : any[] = [];
                const inputFiles : string[] = [];
                const outputFiles : string[] = [];
                this.env.dao.getVisualGraph().getIoComs().foreach(($ioCom : VXNode) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($ioCom.getAttributeValue("path"))) {
                        const origPath : string = $ioCom.getAttributeValue("path");
                        let path : string = StringUtils.Replace(origPath, "\\", "/");
                        path = StringUtils.Replace(path, "//", "/");
                        if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.IMAGE_INPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DATA_INPUT) {
                            if (!StringUtils.StartsWith(path, "data/input/")) {
                                ioComsRegister.push(() : void => {
                                    $ioCom.getAttribute("path").value = origPath;
                                });
                                path = "../data/input/" + StringUtils.Substring(path, StringUtils.IndexOf(path, "/", false) + 1);
                                if (inputFiles.indexOf(path) !== -1) {
                                    path = "../data/input/" + $ioCom.UniqueId() +
                                        StringUtils.Substring(path, StringUtils.IndexOf(path, ".", false));
                                }
                                $ioCom.getAttribute("path").value = path;
                            }
                            path = $ioCom.getAttributeValue("path");
                            if (inputFiles.indexOf(path) === -1) {
                                inputFiles.push(path);
                                if (StringUtils.StartsWith(path, "../")) {
                                    path = StringUtils.Substring(path, 2);
                                }
                                copyFiles.push({from: origPath, to: path});
                            }
                        } else if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.IMAGE_OUTPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_OUTPUT ||
                            $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.DATA_OUTPUT) {
                            if (!StringUtils.StartsWith(path, "data/output/")) {
                                ioComsRegister.push(() : void => {
                                    $ioCom.getAttribute("path").value = origPath;
                                });
                                path = "../data/output/" + StringUtils.Substring(path, StringUtils.IndexOf(path, "/", false) + 1);
                                if (outputFiles.indexOf(path) !== -1) {
                                    path = "../data/output/" + $ioCom.UniqueId() +
                                        StringUtils.Substring(path, StringUtils.IndexOf(path, ".", false));
                                }
                                $ioCom.getAttribute("path").value = path;
                            }
                            path = $ioCom.getAttributeValue("path");
                            if (outputFiles.indexOf(path) === -1) {
                                outputFiles.push(path);
                            }
                        }
                    }
                });
                const getNextFile : any = ($index : number) : void => {
                    if ($index < copyFiles.length) {
                        let from : string = copyFiles[$index].from;
                        if (StringUtils.StartsWith(from, "data/output/") ||
                            StringUtils.StartsWith(from, "data/input/")) {
                            from = this.projectFolder + "/" + from;
                        } else if (StringUtils.Contains(from, "/target/")) {
                            from = this.appRootFolder + "/" + StringUtils.Remove(StringUtils.Substring(
                                from, StringUtils.IndexOf(from, "/target/")), "/target/");
                        }
                        const to : string = "/" + copyFiles[$index].to;
                        if ($ignore.indexOf(to) === -1) {
                            this.env.fileSystem
                                .Copy(from, $destination + to)
                                .Then(() : void => {
                                    getNextFile($index + 1);
                                });
                        } else {
                            getNextFile($index + 1);
                        }
                    } else {
                        $onSuccess();
                    }
                };
                getNextFile(0);
            };
            const restoreIOComs : any = () : void => {
                ioComsRegister.forEach(($register : () => void) : void => {
                    $register();
                });
            };
            const generate : any = () : void => {
                const generator : CppCodeGenerator =
                    new CppCodeGenerator(this.env.dao.getVisualGraph(), this.env.dao.getVisualGraphDAO(), $platform);
                generator.ProjectDirectory($projectPath);
                $callback({message: this.notifications.generating_code_sources, type: StatusType.PROGRESS});
                generator.GenerateCode(($success : boolean, $structurePost : string[]) : void => {
                    if ($convertIoComs) {
                        restoreIOComs();
                    }
                    if ($success) {
                        $callback({success: true}, generator, $structurePost);
                    } else {
                        $callback({
                            message: $convertIoComs ?
                                this.notifications.unable_to_generate_code_for_export : this.notifications.unable_to_generate_code,
                            success: false,
                            type   : StatusType.ERROR
                        });
                    }
                });
            };
            if ($convertIoComs) {
                prepareIOComs($projectPath, () : void => {
                    this.env.fileSystem
                        .Write($projectPath + "/VisualGraph.jsonp", this.env.dao.ToJsonp())
                        .Then(($success : boolean) : void => {
                            if ($success) {
                                generate();
                            } else {
                                $callback({
                                    message: this.notifications.failed_to_save_generated_code,
                                    success: false,
                                    type   : StatusType.ERROR
                                });
                            }
                        });
                });
            } else {
                generate();
            }
        }
    }
}
