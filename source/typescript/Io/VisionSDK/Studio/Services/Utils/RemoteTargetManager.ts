/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import IFileSystemDownloadPromise = Com.Wui.Framework.Services.Connectors.IFileSystemDownloadPromise;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import AgentsRegisterConnector = Com.Wui.Framework.Services.Connectors.AgentsRegisterConnector;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;
    import EventsManger = Com.Wui.Framework.Commons.Events.EventsManager;

    export class RemoteTargetManager extends BaseObject {
        private terminal : TerminalConnector;
        private fileSystem : FileSystemHandlerConnector;
        private connection : IRemoteConnection;
        private environment : IRemoteConnectionEnvironment;
        private readonly unixHome : string;
        private readonly sshKeyPath : string;
        private printHandler : ($message : string) => void;
        private isAgent : boolean;
        private platform : PlatformType;

        constructor($connection : IRemoteConnection, $environment : IRemoteConnectionEnvironment) {
            super();

            this.environment = $environment;
            this.unixHome = "/" + StringUtils.Remove(StringUtils.Replace(this.environment.homePath, "\\", "/"), ":");
            this.sshKeyPath = this.unixHome + "/.ssh/id_rsa";

            this.terminal = new TerminalConnector();
            this.fileSystem = new FileSystemHandlerConnector();
            this.printHandler = ($message : string) : void => {
                Echo.Println($message);
            };

            this.connection = $connection;
            if (ObjectValidator.IsEmptyOrNull($connection.name)) {
                $connection.name = $connection.user + "@" + $connection.host;
            }
            if (ObjectValidator.IsEmptyOrNull($environment.hubUrl)) {
                $environment.hubUrl = $environment.serverUrl;
            }
            if (!StringUtils.EndsWith($environment.hubUrl, "/connector.config.jsonp")) {
                $environment.hubUrl += "/connector.config.jsonp";
            }
            this.isAgent = false;
            this.platform = PlatformType.LINUX;
        }

        public setOnError($callback : ($message : string) => void) : void {
            this.fileSystem.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                $callback($eventArgs.Message());
            });
            this.terminal.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                $callback($eventArgs.Message());
            });
        }

        public setOnPrint($callback : ($message : string) => void) : void {
            this.printHandler = $callback;
        }

        public getAgentName() : string {
            return this.connection.agentName;
        }

        public getServerConfiguration() : string {
            return this.environment.hubUrl;
        }

        public getName() : string {
            return this.connection.name;
        }

        public getPlatform() : PlatformType {
            return this.platform;
        }

        public IsAgent() : boolean {
            return this.isAgent;
        }

        public Start($callback? : ($status : boolean) => void) : void {
            if (ObjectValidator.IsEmptyOrNull($callback)) {
                $callback = () : void => {
                    // default callback
                };
            }
            this.platform = PlatformType.LINUX;

            this.checkConnector(($status : boolean) : void => {
                if ($status) {
                    this.printHandler("Connector is already running...");
                    $callback($status);
                } else {
                    this.generateSSHKeys(($status : boolean) : void => {
                        if ($status) {
                            this.printHandler("Downloading connector ...");
                            this.downloadConnector().Then(($packagePath : string) : void => {
                                this.fileSystem.Unpack($packagePath).Then(($path : string) : void => {
                                    this.fileSystem.Delete($packagePath).Then(() : void => {
                                        this.uploadConnector($path, ($status : boolean, $remotePath : string) : void => {
                                            if ($status) {
                                                this.startConnector($remotePath, ($status : boolean) : void => {
                                                    if ($status) {
                                                        this.checkConnector(($status : boolean) : void => {
                                                            $callback($status);
                                                            if ($status) {
                                                                this.printHandler("Init has been successful");
                                                            } else {
                                                                this.printHandler("Unable to start communication with target.");
                                                            }
                                                        }, true);
                                                    } else {
                                                        $callback(false);
                                                    }
                                                });
                                            } else {
                                                $callback(false);
                                            }
                                        });
                                    });
                                });
                            });
                        } else {
                            $callback(false);
                        }
                    });
                }
            });
        }

        public Deploy($source : string, $destination : string, $callback : ($status : boolean) => void, $toTarget : boolean = true) : void {
            $source = "/" + StringUtils.Remove(StringUtils.Replace($source, "\\", "/"), ":");
            let cmd : string = "rsync -a --progress -e 'ssh -p " + this.environment.sshPort + "-i " + this.sshKeyPath +
                " -o StrictHostKeyChecking=no -l " + this.connection.user + "' ";
            if ($toTarget) {
                cmd += $source + " " + this.connection.host + ":" + $destination;
            } else {
                cmd += this.connection.host + ":" + $source + " " + $destination;
            }

            this.shellExecute(
                this.sshExecute("rsync --version"),
                ($exitCode : number, $stdout : string) : void => {
                    if (StringUtils.Contains($stdout, "command not found")) {
                        cmd = "scp -C -r -P " + this.environment.sshPort + " -i" + this.sshKeyPath +
                            " -o StrictHostKeyChecking=no -o User=" + this.connection.user + " ";
                        if ($toTarget) {
                            cmd += $source + " " + this.connection.host + ":" + $destination;
                        } else {
                            cmd += this.connection.host + ":" + $source + " " + $destination;
                        }
                    }
                    if ($toTarget) {
                        cmd = this.sshExecute("mkdir -p " + $destination) + " && " + cmd;
                    }
                    this.shellExecute(cmd, ($exitCode : number) : void => {
                        if ($exitCode === 0) {
                            $callback(true);
                        } else {
                            this.printHandler("Unable to deploy \"" + $source + "\" to \"" + $destination + "\"");
                            $callback(false);
                        }
                    });
                });
        }

        protected checkConnector($callback : ($status : boolean) => void, $loopedCheck : boolean = false) : void {
            const agentsRegister : AgentsRegisterConnector = new AgentsRegisterConnector(true, this.environment.hubUrl);

            let checkIterations : number = 0;
            const checkRegisteredAgent : any = ($callback : ($status : boolean) => void) : void => {
                agentsRegister.getAgentsList()
                    .Then(($agentsList : IAgentInfo[]) : void => {
                        let agentAvailable : boolean = false;
                        $agentsList.forEach(($agentInfo : IAgentInfo) : void => {
                            if ($agentInfo.name === this.connection.agentName) {
                                this.platform = $agentInfo.platform;
                                agentAvailable = true;
                            }
                        });

                        if (!agentAvailable && checkIterations < 10 && $loopedCheck) {
                            checkIterations++;
                            this.printHandler("Connector not detected, try again (" + checkIterations + "/10)");
                            EventsManger.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                checkRegisteredAgent($callback);
                            }, 500);
                        } else {
                            $callback(agentAvailable);
                        }
                    });
            };

            this.isAgent = false;
            checkRegisteredAgent(($status : boolean) : void => {
                if ($status) {
                    this.isAgent = true;
                    if (ObjectValidator.IsEmptyOrNull(this.platform)) {
                        const terminalCheck : TerminalConnector = new TerminalConnector();
                        terminalCheck.setAgent(this.connection.agentName, this.environment.hubUrl);
                        terminalCheck.Execute("uname", ["-a"])
                            .Then(($exitCode : number, $stdOut : string) : void => {
                                if (StringUtils.ContainsIgnoreCase($stdOut, "aarch64")) {
                                    this.platform = PlatformType.IMX;
                                }
                                $callback($exitCode === 0);
                            });
                    } else {
                        $callback(true);
                    }
                } else {
                    $callback(false);
                }
            });
        }

        private validateSshConnection($callback : ($status : boolean, $stdout? : string) => void) : void {
            this.printHandler("Validating connection ...");
            this.shellExecute(this.sshExecute(
                "echo hello from",
                "uname -a",
                "exit") + " & sleep 1 && exit",
                ($exitCode : number, $stdout : string) : void => {
                    if (StringUtils.ContainsIgnoreCase($stdout, "aarch64")) {
                        this.platform = PlatformType.IMX;
                    }
                    $callback($exitCode === 0 &&
                        !StringUtils.ContainsIgnoreCase($stdout, this.connection.user + "@" + this.connection.host + "'s password:"),
                        $stdout);
                });
        }

        private sendSSHKey($callback : ($status : boolean) => void) : void {
            this.printHandler("Sending public key to target ...");
            const copyCommand : string = "ssh-copy-id -p " + this.environment.sshPort + " -o StrictHostKeyChecking=no " +
                this.connection.user + "@" + this.connection.host;
            const send : any = ($command : string) : void => {
                this.shellExecute("export HOME=" + this.unixHome + " && " + $command, ($exitCode : number) : void => {
                    if ($exitCode === 0) {
                        $callback(true);
                    } else {
                        this.printHandler("Unable to register public key");
                        $callback(false);
                    }
                });
            };
            if (!ObjectValidator.IsEmptyOrNull(this.connection.pass)) {
                send("sshpass -p \"" + this.connection.pass + "\" " + copyCommand);
            } else {
                this.shellExecute("ssh -p " + this.environment.sshPort + " -o StrictHostKeyChecking=no " + this.connection.user + "@" +
                    this.connection.host + " " + "'echo hello && exit' & sleep 1 && exit",
                    ($exitCode : number, $stdout : string) : void => {
                        if ($exitCode === 0) {
                            if (StringUtils.ContainsIgnoreCase($stdout,
                                this.connection.user + "@" + this.connection.host + "'s password:")) {
                                this.printHandler("User password is required");
                                $callback(false);
                            } else {
                                send(copyCommand);
                            }
                        } else {
                            $callback(false);
                        }
                    });
            }
        }

        private generateSSHKeys($callback : ($status : boolean) => void) : void {
            const localKeyPath : string = this.environment.homePath + "/.ssh";
            const unixKeyPath : string = "/" + StringUtils.Remove(StringUtils.Replace(localKeyPath, "\\", "/"), ":");
            const sendKey : any = () : void => {
                this.sendSSHKey(($status : boolean) : void => {
                    if ($status) {
                        this.validateSshConnection(($status : boolean) : void => {
                            if (!$status) {
                                this.printHandler("Connection to target has failed.");
                            }
                            $callback($status);
                        });
                    } else {
                        $callback(false);
                    }
                });
            };
            this.fileSystem.Exists(localKeyPath).Then(($status : boolean) : void => {
                if ($status) {
                    this.validateSshConnection(($status : boolean, $stdout : string) : void => {
                        if (!$status) {
                            if (StringUtils.ContainsIgnoreCase($stdout, "error", "denied")) {
                                sendKey();
                            } else {
                                $callback(false);
                            }
                        } else {
                            $callback(true);
                        }
                    });
                } else {
                    this.fileSystem.CreateDirectory(localKeyPath).Then(($status : boolean) : void => {
                        if ($status) {
                            this.printHandler("Generating keys ...");
                            this.shellExecute(
                                "cd " + unixKeyPath + " && " +
                                "ssh-keygen -f id_rsa -t rsa -b 4096 -N ''",
                                ($exitCode : number, $stdout : string) : void => {
                                    if ($exitCode === 0 && StringUtils.ContainsIgnoreCase($stdout, "Your public key has been saved")) {
                                        sendKey();
                                    } else {
                                        this.printHandler("Unable to generate key pairs");
                                        $callback(false);
                                    }
                                });
                        } else {
                            this.printHandler("Unable to create keys storage");
                            $callback(false);
                        }
                    });
                }
            });
        }

        private uploadConnector($source : string, $callback : ($status : boolean, $remotePath? : string) => void) : void {
            const dest : string = "/var/tmp/vision-sdk-externalmodules";
            this.printHandler("Uploading connector ...");
            this.Deploy($source, dest, ($status : boolean) : void => {
                $source = StringUtils.Replace($source, "\\", "/");
                $callback($status, dest + StringUtils.Substring($source, StringUtils.IndexOf($source, "/", false)));
            });
        }

        private startConnector($remotePath : string, $callback : ($status : boolean) => void) : void {
            this.printHandler("Starting connector ...");
            this.shellExecute(this.sshExecute(
                "cd " + $remotePath,
                "chmod u+x WuiConnector resource/libs/p7zip/bin/7z resource/libs/p7zip/bin/7za",
                "./WuiConnector agent --name=\"" + this.connection.agentName + "\" &") +
                " & sleep 1 && exit",
                ($exitCode : number) : void => {
                    if ($exitCode === 0) {
                        $callback(true);
                    } else {
                        this.printHandler("Unable to start WuiConnector.");
                        $callback(false);
                    }
                });
        }

        private sshExecute(...$commands : string[]) : string {
            return "ssh -p " + this.environment.sshPort + " -i " + this.sshKeyPath + " -o StrictHostKeyChecking=no " +
                this.connection.user + "@" + this.connection.host + " " + "'" + $commands.join(" && ") + "'";
        }

        private shellExecute($call, $callback : ($exitCode : number, $stdout : string) => void) : void {
            this.terminal
                .Spawn(this.environment.msysPath + "/usr/bin/mintty.exe",
                    [
                        "--log", "-",
                        "-d",
                        "--nopin",
                        "--window", "hide",
                        "--hold", "never",
                        "-o", "'ConfirmExit=no'",
                        "--",
                        "/usr/bin/sh",
                        "-lc", "\"" + $call + "\""
                    ], {
                        cwd    : this.environment.msysPath,
                        verbose: false
                    })
                .OnMessage(($message : string) : void => {
                    this.printHandler($message);
                })
                .Then(($exitCode : number, $stdout : string) : void => {
                    if (StringUtils.ContainsIgnoreCase($stdout, "error", "denied", "not known")) {
                        $exitCode = -1;
                    }
                    $callback($exitCode, $stdout);
                });
        }

        private downloadConnector() : IFileSystemDownloadPromise {
            return this.fileSystem.Download(<FileSystemDownloadOptions>{
                url: this.environment.serverUrl + "/Update/com-wui-framework-connector/" + (this.platform === PlatformType.IMX ?
                    "i.MX/arm64" : "Linux/linux64") + "/2018.1.1-alpha"
            });
        }
    }

    export class IRemoteConnection {
        public host : string;
        public user : string;
        public pass : string;
        public name? : string;
        public agentName? : string;
    }

    export class IRemoteConnectionEnvironment {
        public sshPort : number;
        public homePath : string;
        public msysPath : string;
        public serverUrl : string;
        public hubUrl? : string;
    }
}
