/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Utils {
    "use strict";
    import SettingsDAO = Io.VisionSDK.Studio.Services.DAO.Pages.SettingsDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import Resources = Io.VisionSDK.Studio.Services.DAO.Resources;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import IToolchainSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IToolchainSettingsValue;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;

    export class SettingsPersistenceManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private readonly globalSettingsPath : string;
        private settings : ISettings;
        private globalSettings : ISettings;
        private projectId : string;
        private settingsPath : string;
        private fileSystem : FileSystemHandlerConnector;
        private readonly terminal : TerminalConnector;
        private dao : SettingsDAO;

        constructor($globalSettingsFolder : string, $settingsDao : SettingsDAO, $fileSystem : FileSystemHandlerConnector,
                    $terminal : TerminalConnector) {
            super();
            this.globalSettingsPath = $globalSettingsFolder + "/.visionsdk/settings.jsonp";
            this.fileSystem = $fileSystem;
            this.terminal = $terminal;
            this.dao = $settingsDao;
            this.settings = <ISettings>{projectSettings: {}};
            this.globalSettings = <ISettings>{
                connectionSettings: {options: []},
                selectedPlatform  : undefined,
                toolchainSettings : {options: []}
            };
        }

        public Save($id : string, $path : string, $callback : ($status : boolean, $message? : string) => void) : void {
            this.setProjectInfo($id, $path);
            this.save(this.settingsPath, this.settings, ($status : boolean) => {
                if ($status) {
                    this.save(this.globalSettingsPath, this.globalSettings, ($status : boolean) => {
                        if ($status) {
                            $callback(true, "Settings saved successfully.");
                        } else {
                            $callback(false, "Global settings failed to save at path : " + this.globalSettingsPath);
                        }
                    });
                } else {
                    $callback(false, "Project settings failed to save at path : " + this.settingsPath);
                }
            });
        }

        public SaveGlobal($callback : ($status : boolean, $message? : string) => void) : void {
            this.save(this.globalSettingsPath, this.globalSettings, ($status : boolean) => {
                if ($status) {
                    $callback(true, "Global settings saved successfully.");
                } else {
                    $callback(false, "Global settings failed to save at path : " + this.globalSettingsPath);
                }
            });
        }

        public Init($id : string, $path : string, $callback? : () => void) : void {
            const completeInit = () : void => {
                this.globalSettings.projectSettings = undefined;
                this.settings.toolchainSettings = undefined;
                this.settings.connectionSettings = undefined;
                this.settings.selectedPlatform = undefined;
                if (!Loader.getInstance().getHttpManager().getRequest().IsWuiHost()) {
                    ToolchainManager.getLocalAppDataPath(this.terminal, ($appDataPath : string) : void => {
                        this.globalSettings.toolchainSettings.options.forEach(($option : IToolchainSettingsValue) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($option.compiler)) {
                                $option.compiler = StringUtils.Replace($option.compiler, "%localappdata%", $appDataPath);
                            }
                            if (!ObjectValidator.IsEmptyOrNull($option.cmake)) {
                                $option.cmake = StringUtils.Replace($option.cmake, "%localappdata%", $appDataPath);
                            }
                        });
                        if (ObjectValidator.IsFunction($callback)) {
                            $callback();
                        }
                    });
                } else {
                    $callback();
                }
            };

            if ($id !== this.projectId) {
                this.setProjectInfo($id, $path);
                const loadShared : any = () : void => {
                    this.fileSystem.Exists(this.globalSettingsPath).Then(($success : boolean) => {
                        if ($success) {
                            BaseDAO.UnloadResource(this.globalSettingsPath);
                            this.dao.setConfigurationPath(this.globalSettingsPath);
                            this.dao.Load(LanguageType.EN, () : void => {
                                this.globalSettings = this.dao.getPageConfiguration().values;
                                this.dao.setConfigurationPath(SettingsDAO.defaultConfigurationPath);
                                completeInit();
                            }, true);
                        } else {
                            this.globalSettings = JSON.parse(JSON.stringify(this.settings));
                            completeInit();
                        }
                    });
                };

                const loadDao : any = ($force : boolean) : void => {
                    if ($force) {
                        BaseDAO.UnloadResource(this.settingsPath);
                    }
                    this.dao.Load(LanguageType.EN, () : void => {
                        if ($force && $id === this.dao.getPageConfiguration().projectId) {
                            this.settings = this.dao.getPageConfiguration().values;
                            loadShared();
                        } else {
                            this.dao.setConfigurationPath(SettingsDAO.defaultConfigurationPath);
                            this.dao.Load(LanguageType.EN, () : void => {
                                this.settings = this.dao.getPageConfiguration().values;
                                loadShared();
                            }, true);
                        }
                    }, $force);
                };
                this.fileSystem.Exists(this.settingsPath).Then(($success : boolean) => {
                    if ($success) {
                        this.dao.setConfigurationPath(this.settingsPath);
                        loadDao(true);
                    } else {
                        const fallbackPath : string = StringUtils.Remove(this.settingsPath, ".jsonp");
                        this.fileSystem.Exists(fallbackPath).Then(($success : boolean) => {
                            if ($success) {
                                this.fileSystem.Rename(fallbackPath, this.settingsPath).Then(($success : boolean) : void => {
                                    if ($success) {
                                        this.dao.setConfigurationPath(this.settingsPath);
                                        loadDao(true);
                                    } else {
                                        this.dao.setConfigurationPath(SettingsDAO.defaultConfigurationPath);
                                        loadDao(false);
                                    }
                                });
                            } else {
                                this.dao.setConfigurationPath(SettingsDAO.defaultConfigurationPath);
                                loadDao(false);
                            }
                        });
                    }
                });
            } else {
                this.setProjectInfo($id, $path);
                completeInit();
            }
        }

        public Settings($value? : ISettings) : ISettings {
            if (ObjectValidator.IsSet($value)) {
                this.globalSettings.selectedPlatform = $value.selectedPlatform;
                this.settings.projectSettings = $value.projectSettings;
                this.globalSettings.connectionSettings = $value.connectionSettings;
                this.globalSettings.toolchainSettings = $value.toolchainSettings;
            }
            return {
                connectionSettings: this.globalSettings.connectionSettings,
                projectSettings   : this.settings.projectSettings,
                selectedPlatform  : this.globalSettings.selectedPlatform,
                toolchainSettings : this.globalSettings.toolchainSettings
            };
        }

        private setProjectInfo($id : string, $path : string) : void {
            this.projectId = $id;
            this.settingsPath = $path + "/.visionsdk/settings.jsonp";
        }

        private save($path : string, $settings : ISettings, $callback : ($status : boolean) => void) : void {
            const settings : any = {
                $interface   : "ISettingLocalization",
                extendsConfig: "resource/data/Io/VisionSDK/Studio/Services/Localization/SettingsLocalization.jsonp",
                projectId    : this.projectId,
                values       : {
                    connectionSettings: $settings.connectionSettings,
                    projectSettings   : $settings.projectSettings,
                    selectedPlatform  : $settings.selectedPlatform,
                    toolchainSettings : $settings.toolchainSettings
                }
            };
            this.fileSystem.Write($path, Resources.ClassName() + ".Data(" + JSON.stringify(settings) + ");")
                .Then(($success : boolean) : void => {
                    $callback($success);
                });
        }
    }
}
