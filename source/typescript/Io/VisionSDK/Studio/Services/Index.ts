/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>Vision SDK Studio Library</h1>" +
                "<h3>WUI Framework library focused on services and business logic for Vision SDK Studio.</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Pages</H3>" +
                "<a href=\"#/Designer\">Designer</a>" + StringUtils.NewLine() +
                "<a href=\"#/KernelGenerator\">Kernel Generator</a>" +
                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +
                "<a href=\"#" + RuntimeTests.FunctionalTestCases.CallbackLink() + "\">Functional test cases</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.AgentsTest.CallbackLink() + "\">Agent connection test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.RemoteTargetTest.CallbackLink() + "\">Remote target connection test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.NodesTemplateTest.CallbackLink() + "\">Templates test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.ConnectionValidatorTest.CallbackLink() + "\">Connection validator test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.NodeHelpTest.CallbackLink() + "\">Nodes help test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.AllNodesBuildTest.CallbackLink() + "\">Test all nodes build</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.ExamplesTest.CallbackLink() + "\">Examples test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.KernelCodeGeneratorTest.CallbackLink() + "\">Kernel code generator test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.NamespaceFallbackTest.CallbackLink() + "\">Namespace fallback test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.ProjectManagerTest.CallbackLink() + "\">Project manager test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.SettingsPersistenceManagerTest.CallbackLink() + "\">Settings persistence manager test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + RuntimeTests.HistoryPersistenceManagerTest.CallbackLink() + "\">History persistence manager test</a>";
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManager.Clear();
            StaticPageContentManager.Title("Vision SDK Studio Index");
            StaticPageContentManager.FaviconSource("resource/graphics/Io/VisionSDK/Studio/Services/Generator.ico");
            StaticPageContentManager.BodyAppend(output);
            StaticPageContentManager.Draw();
        }
    }
}
