/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import MainPageController = Io.VisionSDK.Studio.Services.Controllers.Pages.MainPageController;

    export class ConnectionLostPage extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver {
        protected resolver() : void {
            MainPageController.NotifyConnectionLost();
        }
    }
}
