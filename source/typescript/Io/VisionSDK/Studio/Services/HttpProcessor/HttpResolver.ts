/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.HttpProcessor {
    "use strict";
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import HttpManager = Com.Wui.Framework.Services.HttpProcessor.HttpManager;

    export class HttpResolver extends Com.Wui.Framework.Services.HttpProcessor.HttpResolver {

        public getEvents() : EventsManager {
            return <EventsManager>super.getEvents();
        }

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();

            resolvers["/Designer"] = Io.VisionSDK.Studio.Services.Controllers.Pages.MainPageController;
            resolvers["/KernelGenerator"] = Io.VisionSDK.Studio.Services.Controllers.Pages.KernelGeneratorController;

            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                resolvers["/"] = this.getManager().getRequest().IsWuiHost() ?
                    Io.VisionSDK.Studio.Services.Controllers.Pages.MainPageController : Index;
                resolvers["/web/"] = Index;
                resolvers["/index"] = Index;
                resolvers["/about/Package"] = Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.AboutPackage;
            } else {
                resolvers["/"] = Io.VisionSDK.Studio.Services.Controllers.Pages.MainPageController;
                resolvers["/web/"] = null;
                resolvers["/index"] = null;
            }
            resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] =
                Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.ErrorResolver;
            resolvers["/ServerError/Http/ConnectionLost"] = Io.VisionSDK.Studio.Services.ErrorPages.ConnectionLostPage;
            resolvers["/ServerError/Http/NotFound"] = Io.VisionSDK.Studio.Services.ErrorPages.ConnectionLostPage;
            resolvers["/ServerError/Browser"] = Io.VisionSDK.Studio.Services.ErrorPages.BrowserErrorPage;
            return resolvers;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }
    }
}
