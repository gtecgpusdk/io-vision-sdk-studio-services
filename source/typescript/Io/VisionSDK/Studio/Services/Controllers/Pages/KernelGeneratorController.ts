/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Controllers.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import PersistenceFactory = Com.Wui.Framework.Gui.PersistenceFactory;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import KernelEditorPanel = Io.VisionSDK.Studio.Gui.BaseInterface.Panels.KernelGenerator.KernelEditorPanel;
    import KernelEditorPanelViewer = Io.VisionSDK.Studio.Gui.BaseInterface.Viewers.Panels.KernelGenerator.KernelEditorPanelViewer;
    import KernelEditorPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.KernelGenerator.KernelEditorPanelViewerArgs;
    import KernelGeneratorDAO = Io.VisionSDK.Studio.Services.DAO.Pages.KernelGeneratorDAO;
    import IKernelGeneratorLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IKernelGeneratorLocalization;
    import KernelCodeGenerator = Io.VisionSDK.Studio.Services.CodeGenerator.Core.KernelCodeGenerator;
    import IKernelCodeAttributes = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelCodeAttributes;
    import InputLabel = Io.VisionSDK.UserControls.BaseInterface.UserControls.InputLabel;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowHandlerConnector = Com.Wui.Framework.Services.Connectors.WindowHandlerConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class KernelGeneratorController extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();
            this.setDao(new KernelGeneratorDAO());
            this.setModelClassName(KernelEditorPanelViewer);
        }

        public getModel() : KernelEditorPanelViewer {
            return <KernelEditorPanelViewer>super.getModel();
        }

        public getModelArgs() : KernelEditorPanelViewerArgs {
            return <KernelEditorPanelViewerArgs>this.getDao().getModelArgs();
        }

        public getPageConfiguration() : IKernelGeneratorLocalization {
            return <IKernelGeneratorLocalization>super.getPageConfiguration();
        }

        protected getDao() : KernelGeneratorDAO {
            return <KernelGeneratorDAO>super.getDao();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_COMPLETE, () : void => {
                const appPersistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());

                const model : KernelEditorPanelViewer = this.getModel();
                const instance : KernelEditorPanel = model.getInstance();
                const dao : KernelGeneratorDAO = this.getDao();

                const pathKey : string = "TargetPath";
                if (appPersistence.Exists(pathKey)) {
                    instance.projectPath.Value(appPersistence.Variable(pathKey));
                }
                instance.projectName.MaxVisibleItemsCount(10);
                instance.projectName.getEvents().setOnSelect(() : void => {
                    instance.projectNamespace.Value(dao.getRegister().getProjects(dao.getPageConfiguration().companyId)
                        .getItem(instance.projectName.Value()).namespace);
                });
                instance.generateButton.getEvents().setOnClick(() : void => {
                    const kernelAttributes : IKernelCodeAttributes = {
                        /* tslint:disable: object-literal-sort-keys */
                        companyId  : dao.getPageConfiguration().companyId,
                        project    : instance.projectNamespace.Value(),
                        target     : instance.projectName.Value(),
                        namespace  : instance.kernelNamespace.Value(),
                        name       : instance.kernelName.Value(),
                        description: instance.description.Value(),
                        params     : []
                        /* tslint:enable */
                    };

                    instance.parameters.forEach(($columns : InputLabel[]) : void => {
                        const name : string = StringUtils.Remove($columns[1].Text(), " ");
                        const description : string = $columns[3].Text().trim();
                        if (!ObjectValidator.IsEmptyOrNull(name)) {
                            kernelAttributes.params.push({
                                /* tslint:disable: object-literal-sort-keys */
                                direction: $columns[0].Text(),
                                name,
                                type     : $columns[2].Text(),
                                description,
                                state    : "VX_PARAMETER_STATE_REQUIRED"
                                /* tslint:enable */
                            });
                        }
                    });

                    const generator : KernelCodeGenerator = new KernelCodeGenerator(dao.getTemplates());
                    generator.ProjectDirectory(instance.projectPath.Value());
                    generator.LoadKernels(
                        dao.getRegister().getProjects(kernelAttributes.companyId).getItem(kernelAttributes.target),
                        ($status : boolean, $kernels : IKernelCodeAttributes[]) : void => {
                            if (!$status) {
                                instance.notificationPanel.AddWarning("Unable to parse kernels register.");
                            }
                            dao.getRegister().AddKernels(kernelAttributes.companyId, kernelAttributes.target, $kernels);
                            if (dao.getRegister().ValidateKernel(kernelAttributes)) {
                                generator.GenerateKernel(
                                    dao.getRegister().getProjects(kernelAttributes.companyId).getItem(kernelAttributes.target),
                                    kernelAttributes,
                                    ($status : boolean) : void => {
                                        if ($status) {
                                            instance.notificationPanel.AddSuccess("Kernel has been generated successfully");
                                        } else {
                                            instance.notificationPanel.AddError("Kernel generation has failed");
                                        }
                                    });
                            } else {
                                instance.notificationPanel.AddWarning("Kernel validation has failed");
                            }
                        });
                });

                if (this.getRequest().IsWuiJre()) {
                    const windowHandler : WindowHandlerConnector = new WindowHandlerConnector();
                    instance.getEvents().setOnPathRequest(() : void => {
                        windowHandler.ShowFileDialog({
                            folderOnly: true,
                            openOnly  : true,
                            path      : instance.projectPath.Value(),
                            title     : "Select target project"
                        }).Then(($status : boolean, $path : string) : void => {
                            if ($status) {
                                $path = StringUtils.Replace($path, "\\", "/");
                                appPersistence.Variable(pathKey, $path);
                                instance.projectPath.Value($path);
                            }
                        });
                    });
                } else {
                    instance.projectPath.ReadOnly(false);
                }
            });
            super.resolver();
        }
    }
}
