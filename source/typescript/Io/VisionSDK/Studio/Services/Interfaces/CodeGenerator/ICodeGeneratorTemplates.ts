/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator {
    "use strict";
    import VXContext = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXContext;
    import VXGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXGraph;

    export abstract class ICodeGeneratorTemplates extends Com.Wui.Framework.Services.Interfaces.DAO.IBaseConfiguration {
        public helpConfig : string;
        public vxAPI : IVXNodeTemplates;
        public cpp : ICodeGeneratorTemplatesCpp;
        public cmake : ($projectName : string, $includes : string[], $sources : string[]) => string;
        public jsonp : IJsonpTemplates;
        public model : IModelTemplates;
        public scripts : IScriptsTemplates;
        public utils : ICodeGeneratorUtils;
        public notifications : ICodeGeneratorNotifications;
    }

    export abstract class IVXNodeCppTemplate {
        public property? : ($prefix : string, $item : IVisualGraphNode) => string;
        public create? : ($prefix : string, $item : IVisualGraphNode, ...$args : string[]) => string;
        public process? : ($prefix : string, $item : IVisualGraphNode, ...$args : string[]) => string;
    }

    export abstract class IVXNodeJsonpTemplate {
        public node? : ($prefix : string, $item : IVisualGraphNode) => string;
        public data? : ($prefix : string, $item : IVisualGraphNode) => string[];
    }

    export abstract class ICodeGeneratorTemplatesCpp {
        public license : () => string;
        public main : ($namespace : string, $displayExists : boolean) => string;
        public reference : ($namespace : string, $includes : string[]) => string;
        public visualGraph : IVisualGraphTemplates;
        public baseContext : IVisualGraphTemplates;
        public baseGraph : IVisualGraphTemplates;
        public manager : IVisualGraphTemplates;
        public imanager : IVisualGraphTemplates;
        public vxContext : IVisualGraphTemplates;
        public vxGraph : IVisualGraphTemplates;
    }

    export abstract class IVisualGraphTemplates {
        public construction : ($item : IVisualGraphNode) => string;
        public create : ($item : IVisualGraphNode) => string;
        public validate : ($item : IVisualGraphNode) => string;
        public beforeProcess : ($item : IVisualGraphNode) => string;
        public process : ($item : IVisualGraphNode) => string;
        public afterProcess : ($item : IVisualGraphNode) => string;
        public header : ($namespace : string, $className : string, $includes : string[], $properties : string) => string;
        public source : ($namespace : string, $className : string, $includes : string[], $create : string, $process : string) => string;
    }

    export abstract class IJsonpTemplates {
        public resourceData : ($data : string) => string;
        public visualGraph : ($id : string, $name : string, $ioComs : string, $vxContexts : string) => string;
        public vxContext : ($prefix : string, $item : VXContext, $graphs : string) => string;
        public vxGraph : ($prefix : string, $item : VXGraph, $nodes : string, $data : string) => string;
    }

    export abstract class IModelTemplates {
        // Runtime generated model templates.
    }

    export abstract class ICodeGeneratorUtils {
        // Runtime generated utilities.
    }

    export abstract class ICodeGeneratorNotifications {
        // Localizable notifications specified by templates.
    }

    export abstract class IScriptsTemplates {
        public winGCC : IScriptTemplates;
        public linux : IScriptTemplates;
        public imx : IScriptTemplates;
    }

    export abstract class IScriptTemplates {
        public build : ($executableName : string) => string;
        public run : ($executableName : string, $inLoop) => string;
        public clean : ($executableName : string) => string;
        public readMe : ($projectName : string, $description : string) => string;
    }
}
