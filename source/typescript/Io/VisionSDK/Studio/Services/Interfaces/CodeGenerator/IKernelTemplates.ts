/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator {
    "use strict";

    export abstract class IKernelTemplates extends Com.Wui.Framework.Services.Interfaces.DAO.IBaseConfiguration {
        public typesMapping : string[];
        public main : ($types : string[]) => string;
        public vxKernels : ($namespace : string, $className : string, $types : string[]) => string;
        public type : ($namespace : string, $className : string, $companyId : string, $projectIndex : number, $types : string[]) => string;
        public cpp : IKernelCodeTemplate;
        public c : IKernelCodeTemplate;
        public test : ($namespace : string, $className : string) => string;
        public register : ($kernels : IKernelCodeAttributes[]) => string;
    }

    export abstract class IKernelCodeTemplate {
        public header : ($namespace : string, $className : string, $description : string, $parameters : IKernelCodeParameter) => string;
        public source : ($project : string, $namespace : string, $className : string) => string;
    }

    export abstract class IKernelCodeAttributes {
        public companyId? : string;
        public readonly project? : string;
        public target? : string;
        public readonly namespace : string;
        public readonly name : string;
        public description : string;
        public params : IKernelCodeParameter[];
    }

    export abstract class IKernelCodeParameter {
        public readonly direction : string;
        public readonly type : string;
        public readonly state : string;
        public readonly name : string;
        public readonly description : string;
    }
}
