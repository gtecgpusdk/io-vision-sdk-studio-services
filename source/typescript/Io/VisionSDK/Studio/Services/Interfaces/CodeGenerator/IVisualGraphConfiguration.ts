/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator {
    "use strict";
    import IKernelNodeEnumAttributeItem = Io.VisionSDK.UserControls.Interfaces.IKernelNodeEnumAttributeItem;

    export abstract class IVisualGraph extends Com.Wui.Framework.Services.Interfaces.DAO.IBaseConfiguration {
        public id : string;
        public name : string;
        public description : string;
        public vxAPI : IVXNodeTemplates;
        public ioComs : IVXNode[];
        public vxContexts : IVXContext[];
        public utils : IVXUtils;
    }

    export abstract class IVisualGraphNode {
        public type : () => string;
        public name : string;
        public id : string;
        public column : number;
        public row : number;
    }

    export abstract class IVXNode extends IVisualGraphNode {
        public params : () => any[];
        public outputs : () => IVXNode[];
    }

    export abstract class IVXContext extends IVisualGraphNode {
        public vxGraphs : IVXGraph[];
    }

    export abstract class IVXGraph extends IVisualGraphNode {
        public vxNodes : IVXNode[];
        public vxData : IVXNode[];
    }

    export abstract class IVXUtils {
        public floatToString : ($number : number, $decimalPoints? : number) => void;
        public onError : ($message : string) => void;
        public attributeOptionContains : ($options : Array<string | IKernelNodeEnumAttributeItem>, $value : string) => boolean;
    }
}
