/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator {
    "use strict";
    import IconType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.IconType;
    import VxNodesGroupType = Io.VisionSDK.Studio.Services.Enums.VxNodesGroupType;
    import IKernelNodeEnumAttributeItem = Io.VisionSDK.UserControls.Interfaces.IKernelNodeEnumAttributeItem;
    import IKernelNodeAttributeFormat = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttributeFormat;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IKernelNodeHelp = Io.VisionSDK.UserControls.Interfaces.IKernelNodeHelp;

    export abstract class IVXNodeTemplates {
        public version : string;
        public vxTypes : any;
        public vxContext : IVXNodeTemplate;
        public vxGraph : IVXNodeTemplate;
        public vxIoComs : IVXNodeTemplate[];
        public vxNodes : IVXNodeTemplate[];
        public vxData : IVXNodeTemplate[];
    }

    export abstract class IVXNodeModelTemplate {
        public attributes? : ($item : IVisualGraphNode, $config? : IVXNode) => IVXNodeAttribute[];
        public inputs? : () => string[];
        public outputs? : () => string[];
        public params? : ($item : IVisualGraphNode) => IVisualGraphNode[];
        public validator? : ($source : IVXNode, $target : IVXNode, $sourceInputIndex : number, $targetOutputIndex : number,
                             $correctAttributes? : boolean, $applyChanges? : boolean) => IConnectionValidatorStatus;
        public validateAttribute? : ($item : IVXNode, $sourceAttribute : IVXNodeAttribute | IVXNodeAttribute[], $inputIndex : number,
                                     $correctAttributes? : boolean, $applyChanges? : boolean) => IConnectionValidatorStatus;
        public correctAttributes? : ($item : IVXNode, $attributeName : string, $attributeValue : string,
                                     $applyChanges? : boolean) => ICorrectAttributesStatus;
    }

    export abstract class IVXNodeModel {
        public attributes : ($config? : IVXNode) => IVXNodeAttribute[];
        public inputs : () => IVXNode[];
        public outputs : () => IVXNode[];
        public params : () => IVXNode[];
        public validator : ($target : IVXNode, $outputIndex : number, $inputIndex : number,
                            $correctAttributes? : boolean, $applyChanges? : boolean) => IConnectionValidatorStatus;
        public validateAttribute : ($item : IVXNode, $sourceAttribute : IVXNodeAttribute,
                                    $inputIndex : number, $correctAttributes? : boolean,
                                    $applyChanges? : boolean) => IConnectionValidatorStatus;
        public correctAttributes : ($changedAttribute : IVXNodeAttribute, $applyChanges? : boolean) => ICorrectAttributesStatus;
    }

    export abstract class IVXNodeTemplate {
        public icon : IconType;
        public varName : string;
        public groupId : VxNodesGroupType;
        public cpp : IVXNodeCppTemplate;
        public jsonp? : IVXNodeJsonpTemplate;
        public model? : IVXNodeModelTemplate;
        public help? : IKernelNodeHelp;
    }

    export abstract class IVXNodeAttribute {
        public type : string;
        public name : string;
        public varName : string;
        public value : any;
        public options? : Array<string | IKernelNodeEnumAttributeItem>;
        public format? : IKernelNodeAttributeFormat;
    }

    export abstract class IVXNodeCode {
        public property : ($prefix? : string) => string;
        public create : ($prefix? : string) => string;
        public process : ($prefix? : string) => string;
    }

    export abstract class IVXNodeJson {
        public node : ($prefix? : string) => string;
        public data : ($prefix? : string) => IVXDataJson[];
    }

    export abstract class IVXDataJson {
        public id : string;
        public index : number;
        public content : string;
        public type : string;
    }

    export abstract class ICorrectAttributesStatus {
        public corrected : string[];
    }
}
