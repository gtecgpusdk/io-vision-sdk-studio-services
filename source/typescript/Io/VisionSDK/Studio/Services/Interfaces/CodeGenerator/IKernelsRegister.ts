/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator {
    "use strict";

    export abstract class IKernelsRegister extends Com.Wui.Framework.Services.Interfaces.DAO.IBaseConfiguration {
        public projects : IKernelsCodeProject[];
    }

    export abstract class IKernelsCodeProject {
        public readonly companyId : string;
        public index? : number;
        public readonly name : string;
        public readonly namespace : string;
        public readonly repository : string;
        public kernels : IKernelCodeAttributes[];
    }
}
