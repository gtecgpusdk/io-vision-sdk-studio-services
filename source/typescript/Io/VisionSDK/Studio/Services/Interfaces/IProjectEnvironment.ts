/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces {
    "use strict";
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;

    export abstract class IProjectEnvironment {
        public dao : MainPageDAO;
        public terminal : TerminalConnector;
        public fileSystem : FileSystemHandlerConnector;
        public environment : EnvironmentArgs;
    }
}
