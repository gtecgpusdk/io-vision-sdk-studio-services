/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces {
    "use strict";
    import ProjectManager = Io.VisionSDK.Studio.Services.Utils.ProjectManager;
    import InteractiveMessage = Io.VisionSDK.UserControls.Utils.InteractiveMessage;

    export abstract class IApplicationEnvironment {
        public projectManager : ProjectManager;
        public syncFrontend : ($updateGraph? : boolean, $callback? : () => void) => void;
        public syncBackend : () => void;
        public showDirectoryBrowser : ($callback : ($status : boolean, $path) => void, $description : string,
                                       $openOnly? : boolean, $folderOnly? : boolean, $initPath? : string,
                                       $filter? : string[]) => void;
        public handleButtons : () => void;
        public notifySuccess : ($message : string) => void;
        public notifyError : ($message : string) => void;
        public notifyWarning : ($message : string) => void;
        public notifyInfo : ($message : string) => void;
        public printStatus : ($status : IStatus, $owner? : string) => void;
        public printConsole : ($message : string | InteractiveMessage, $isImportant? : boolean) => void;
        public mapUpdated : boolean;
        public streamCounter : number;
    }
}
