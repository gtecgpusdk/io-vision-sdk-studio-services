/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces {
    "use strict";
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;

    export abstract class INodeRestrictions {
        public types : string[];
        public names : string[];
        public nodes : IKernelNode[];
    }
}
