/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO {
    "use strict";
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import IBaseConfiguration = Com.Wui.Framework.Services.Interfaces.DAO.IBaseConfiguration;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import IBoundingRectangle = Io.VisionSDK.Studio.Gui.Interfaces.IBoundingRectangle;

    export abstract class IAppWizardItem {
        public header : string;
        public text : string;
        public dimensions : IBoundingRectangle;
        public before : ($instance : BasePanel, $dao? : MainPageDAO) => IGuiCommons;
        public after : ($instance : BasePanel, $dao? : MainPageDAO) => void;
    }

    export abstract class IAppWizardLocalization extends IBaseConfiguration {
        public beforeOpen : ($callback : () => void) => void;
        public beforeClose : ($callback : () => void) => void;
        public items : IAppWizardItem[];
        public mouseHide : () => void;
        public mouseClick : ($x : number, $y : number, $tooltip? : string, $callback? : () => void) => void;
        public mouseDoubleClick : ($x : number, $y : number, $tooltip? : string, $callback? : () => void) => void;
        public mouseDrag : ($x : number, $y : number, $tooltip? : string, $callback? : () => void) => void;
        public mouseMoveTo : ($dx : number, $dy : number, $animationMath? : ($current : number, $end : number) => number,
                              $callback? : () => void) => void;
    }
}
