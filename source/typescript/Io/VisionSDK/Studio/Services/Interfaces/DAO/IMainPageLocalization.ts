/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;
    import IKernelNodeNotifications = Io.VisionSDK.UserControls.Interfaces.IKernelNodeNotifications;

    export abstract class IMainPageToolBarPanel {
        public projectMenu : string;
        public projectMenuItems : IMainPageToolBarProjectMenu;
        public save : string;
        public undo : string;
        public redo : string;
        public open : string;
        public console : string;
        public help : string;
        public buildBar : IMainPageToolBarBuildBar;
        public update : string;
        public updateHint : string;
    }

    export abstract class IMainPageToolBarProjectMenu {
        public newProject : string;
        public projectSettings : string;
        public examples;
        public saveAs : string;
        public exportProject : string;
        public load : string;
    }

    export abstract class IMainPageToolBarBuildBar {
        public run : string;
        public stop : string;
        public configureBuild : string;
        public emulator : string;
    }

    export abstract class IMainPagePropertiesPanel {
        public name : string;
        public type : string;
    }

    export abstract class IDirectoryBrowserDialog {
        public close : string;
        public save : string;
        /* tslint:disable: variable-name */
        public select_target_directory : string;
        public select_input_file : string;
        public select_visualGraph_project : string;
        public specify_export_package_path : string;
        /* tslint:enable */
    }

    export class IKernelNodeHelpDialog {
        public header : string;
        public description : string;
        public link : string;
    }

    export abstract class IProjectPickerDialog {
        public header : string;
        public load : string;
        public defaultPageHeader : string;
        public defaultPageDescription : string;
    }

    export abstract class IGlobalSettings {
        public msysPath : string;
        public cmakePath : string;
        public hubUrl : string;
        public runtimeSdkVersion : string;
        public buildCacheEnabled : boolean;
        public forceWizardShow : boolean;
        public templates : string[];
        public nodeRestrictions : string[][];
        public hostTargetPath : string;
    }

    export abstract class IExportPlatforms {
        public winGCC : string;
        public linux : string;
        public imx : string;
    }

    export abstract class IExportDialog {
        public header : string;
        public targetPlatform : string;
        public exportPath : string;
        public exportButton : string;
        public close : string;
        public platforms : IExportPlatforms;
    }

    export abstract class IMainPageLocalization extends IBasePageLocalization {
        public toolBarPanel : IMainPageToolBarPanel;
        public propertiesPanel : IMainPagePropertiesPanel;
        public kernelNodeNotifications : IKernelNodeNotifications;
        public directoryBrowserDialog : IDirectoryBrowserDialog;
        public kernelNodeHelpDialog : IKernelNodeHelpDialog;
        public examplesProjectPickerDialog : IProjectPickerDialog;
        public exportDialog : IExportDialog;
        public projectsList : string[];
        public globalSettings : IGlobalSettings;
        public projectCreationPanel : IProjectSettingsPanelLocalization;
        public updaters : string[];
        public notifications : IMainPageNotifications;
    }

    export abstract class IMainPageNotifications {
        /* tslint:disable: variable-name */
        public updating_application__please_wait : string;
        public application_update_is_available_and_will_be_executed_on_next_launch : string;
        public failed_to_launch_application_update : string;
        public validating_build_dependencies : string;
        public validating_graph : string;
        public toolchain_validation : string;
        public build_dependencies_are_missing_or_invalid__starting_installation : string;
        public build_dependencies_are_up_to_date : string;
        public visualGraph__0__was_loaded : string;
        public creating_project : string;
        public loading_project : string;
        public loading_example : string;
        public unable_to_save_runtime_config : string;
        public saving_visual_graph : string;
        public project__0__was_saved_successfully : string;
        public project__0__was_saved_successfully__failed_to_save_metadata : string;
        public failed_to_save_project__0__ : string;
        public project__0__was_created_successfully : string;
        public failed_to_create__project__0 : string;
        public example__0__was_loaded_successfully : string;
        public failed_to_load__example__0 : string;
        public project__0__was_loaded_successfully : string;
        public failed_to_load_project : string;
        public changes_made_in_example_project_are_not_persistent_and_will_not_be_loaded_for_future_runtime : string;
        public connection_with_name__0__already_exists : string;
        public generating_code_sources : string;
        public unable_to_clean_sources : string;
        public unable_to_clean_CMake_cache : string;
        public unable_to_generate_code : string;
        public unable_to_build_generated_code : string;
        public connected_to_target__0__ : string;
        public failed_to_connect_to_target__0__ : string;
        public collecting_export_information : string;
        public downloading_vision_sdk : string;
        public preparing_vision_sdk : string;
        public unable_to_copy_sdk : string;
        public creating_package : string;
        public project_was_successfully_exported : string;
        public unable_to_generate_scripts : string;
        public unable_to_generate_code_for_export : string;
        public failed_to_save_generated_code : string;
        public started_exporting_current_project : string;
        public unable_to_detect_path_for_vision_sdk_tool_chain : string;
        public creating_initial_workspace : string;
        public initial_workspace_created : string;
        public build_platform_changed_successfully : string;
        public connecting : string;
        public data_was_saved_successfully : string;
        public failed_to_save_data : string;
        public build_failed_connection_errors_found : string;
        public build_disabled___nodes_unsupported_in_platform__0__are_present : string;
        public error_occurred__see_console_output_for_further_details : string;
        public generating_cpp_make_files : string;
        public building_cpp_code : string;
        public cpp_code_was_successfully_generated : string;
        public unable_to_build_cpp_code : string;
        public run_successfully : string;
        public failed_to_clean_up_cache : string;
        public run_failed : string;
        public unable_to_save_runtime_configuration : string;
        public visual_graph_process_failed : string;
        public failed_to_validate_visual_graph : string;
        public unable_to_create_visual_graph_instance : string;
        public unable_to_load_runtime_library : string;
        public stopped_successfully : string;
        public force_stop_send_successfully : string;
        public force_stop_failed : string;
        public stop_failed : string;
        public synchronizing__0__with__1__succeeded : string;
        public synchronizing__0__with__1__failed : string;
        public runtime_sdk_is_up_to_date__starting_synchronization : string;
        public unknown_platform : string;
        public unable_to_generate_cpp_make_files : string;
        public unable_to_create_build_directory : string;
        public finished_preparing_remote_runtime_sdk : string;
        public unable_to_clean_up_sync_folder : string;
        public preparation_of_runtime_sdk_has_failed : string;
        public runtime_sdk_already_exists : string;
        public runtime_sdk_is_locked_on_target_due_to_running_download_task_initiated_by_another_client : string;
        public downloading_runtime_sdk : string;
        public downloaded__0__ : string;
        public finished_preparing_runtime_sdk : string;
        public unable_to_upload_runtime_sdk : string;
        public connection_has_been_lost : string;
        /* tslint:enable */
    }
}
