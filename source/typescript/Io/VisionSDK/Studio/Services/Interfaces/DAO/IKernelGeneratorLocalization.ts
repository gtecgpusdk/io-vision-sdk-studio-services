/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;

    export abstract class IKernelGeneratorLocalization extends IBasePageLocalization {
        public header : string;
        public companyId : string;
        public projectPath : string;
        public projectName : string;
        public projectNamespace : string;
        public kernelNamespace : string;
        public defaultKernelNamespace : string;
        public kernelName : string;
        public defaultKernelName : string;
        public description : string;
        public parameters : string;
        public parameterDirection : string;
        public parameterName : string;
        public parameterType : string;
        public parameterDescription : string;
        public generateButton : string;
    }
}
