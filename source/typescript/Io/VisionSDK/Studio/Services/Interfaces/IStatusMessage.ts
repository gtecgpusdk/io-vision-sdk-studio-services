/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Interfaces {
    "use strict";
    import StatusType = Io.VisionSDK.Studio.Services.Enums.StatusType;

    export abstract class IStatus {
        public success? : boolean;
        public message? : string;
        public type? : StatusType;
    }
}
