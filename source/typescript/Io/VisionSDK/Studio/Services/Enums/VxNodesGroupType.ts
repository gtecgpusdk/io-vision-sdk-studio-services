/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Enums {
    "use strict";

    export class VxNodesGroupType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly IOCOMS : string = "Inputs and Outputs";
        public static readonly ELEMENTARY : string = "Elementary Math";
        public static readonly FILTERS : string = "Filters";
        public static readonly TRANSFORMATIONS : string = "Image Transformation";
        public static readonly ADVANCED : string = "Advanced Math";
        public static readonly UTILS : string = "Utilities";
        public static readonly USER : string = "User Kernels";
        public static readonly DEFAULT : string = "Nodes";
        public static readonly TENSOR : string = "Tensor operations";
        public static readonly NEURAL_NETWORK : string = "Neural Network Nodes";
    }
}
