/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Enums {
    "use strict";

    export class StatusType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly SUCCESS : string = "Success";
        public static readonly WARNING : string = "Warning";
        public static readonly ERROR : string = "Error";
        public static readonly INFO : string = "Info";
        public static readonly PROGRESS : string = "Progress";
        public static readonly LOG : string = "Log";
    }
}
