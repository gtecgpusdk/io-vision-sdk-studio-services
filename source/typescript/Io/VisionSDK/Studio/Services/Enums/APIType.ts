/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Enums {
    "use strict";

    export class APIType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly OVX_1_0_1 : string = "1.0.1";
        public static readonly OVX_1_1_0 : string = "1.1.0";
        public static readonly OVX_1_2_0 : string = "1.2.0";
    }
}
