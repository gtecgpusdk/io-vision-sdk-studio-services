/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.Enums {
    "use strict";

    export class PlatformType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly MSVC : string = "MSVC";
        public static readonly WIN_GCC : string = "WinGCC";
        public static readonly LINUX : string = "Linux";
        public static readonly IMX : string = "iMX";
    }
}
