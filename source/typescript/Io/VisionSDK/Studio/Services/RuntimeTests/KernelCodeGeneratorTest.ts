/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import KernelCodeGenerator = Io.VisionSDK.Studio.Services.CodeGenerator.Core.KernelCodeGenerator;
    import KernelTemplatesDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.KernelTemplatesDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import IKernelCodeAttributes = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelCodeAttributes;
    import KernelsRegisterDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.KernelsRegisterDAO;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class KernelCodeGeneratorTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private readonly testProjectPath : string;
        private fileSystem : FileSystemHandlerConnector;
        private readonly goldenData : string[];
        private readonly goldenFolder : string;
        private readonly kernelAttributes : IKernelCodeAttributes;
        private readonly kernelsRegister : KernelsRegisterDAO;

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector();
            this.kernelsRegister = new KernelsRegisterDAO();
            this.kernelsRegister
                .setConfigurationPath("test/resource/data/Io/VisionSDK/Studio/Services/Configuration/KernelsRegister.jsonp");
            this.testProjectPath = this.getAbsoluteRoot() + "/test-kernel-project";

            this.kernelAttributes = {
                /* tslint:disable: object-literal-sort-keys */
                companyId  : "VX_ID_NXP",
                project    : "Io.VisionSDK.Kernels",
                target     : "io-vision-sdk-kernels",
                namespace  : "Example",
                name       : "Example",
                description: "Enter some algorithm description here.",
                params     : [
                    {
                        direction  : "VX_INPUT",
                        type       : "VX_TYPE_IMAGE",
                        state      : "VX_PARAMETER_STATE_REQUIRED",
                        name       : "input",
                        description: "Specify input image in VX_DF_IMAGE_U8 type."
                    },
                    {
                        direction  : "VX_INPUT",
                        type       : "VX_TYPE_SCALAR",
                        state      : "VX_PARAMETER_STATE_REQUIRED",
                        name       : "shift",
                        description: "Specify vx_uint32 scalar."
                    },
                    {
                        direction  : "VX_OUTPUT",
                        type       : "VX_TYPE_IMAGE",
                        state      : "VX_PARAMETER_STATE_REQUIRED",
                        name       : "output",
                        description: "The output image in VX_DF_IMAGE_U8 type with same size as input image."
                    }
                ]
                /* tslint:enable */
            };

            this.goldenFolder = "KernelGenerator";
            this.goldenData = [
                "/resource/configs/KernelsRegister.json",
                "/source/cpp/LibraryMain.cpp",
                "/source/cpp/VxKernels.hpp",
                "/source/cpp/Io/VisionSDK/Kernels/Enums/KernelType.hpp",
                "/source/cpp/Io/VisionSDK/Kernels/Example/ExampleKernel.cpp",
                "/source/cpp/Io/VisionSDK/Kernels/Example/ExampleKernel.hpp",
                "/source/cpp/Io/VisionSDK/Kernels/Example/ExampleNode.c",
                "/source/cpp/Io/VisionSDK/Kernels/Example/ExampleNode.h",
                "/test/unit/cpp/Io/VisionSDK/Kernels/Example/ExampleNodeTest.cpp"
            ];
        }

        public TestGenerate() : IRuntimeTestPromise {
            const testRoot : string = this.getAbsoluteRoot() +
                "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator/" + this.goldenFolder;
            const targetRoot : string = this.testProjectPath;

            return ($done : () => void) : void => {
                Echo.Printf("<i id=\"test_status\">Loading generator environment...</i>");
                const dao : KernelTemplatesDAO = new KernelTemplatesDAO();
                dao.Load(LanguageType.EN, () : void => {
                    ElementManager.Hide("test_status");

                    const compareNextFile : any = ($index : number) : void => {
                        if ($index < this.goldenData.length) {
                            const file : string = this.goldenData[$index];
                            this.fileSystem.Exists(targetRoot + file).Then(($status : boolean) : void => {
                                if ($status) {
                                    this.fileSystem.Exists(testRoot + file).Then(($status : boolean) : void => {
                                        if ($status) {
                                            this.fileSystem.Read(targetRoot + file).Then(($actual : string) : void => {
                                                this.fileSystem.Read(testRoot + file).Then(($expected : string) : void => {
                                                    if (!ObjectValidator.IsEmptyOrNull($actual) &&
                                                        !ObjectValidator.IsEmptyOrNull($expected)) {
                                                        this.assertEquals(
                                                            JSON.stringify($actual), JSON.stringify($expected), targetRoot + file);
                                                    } else {
                                                        this.assertEquals(
                                                            "", "Not empty file content", targetRoot + file);
                                                    }
                                                    WindowManager.ScrollToBottom();
                                                    compareNextFile($index + 1);
                                                });
                                            });
                                        } else {
                                            this.assertEquals("", testRoot + file, "test data not found");
                                            compareNextFile($index + 1);
                                        }
                                    });
                                } else {
                                    this.assertEquals("", targetRoot + file, "golden data not found");
                                    compareNextFile($index + 1);
                                }
                            });
                        } else {
                            $done();
                        }
                    };

                    const generator : KernelCodeGenerator = new KernelCodeGenerator(dao);
                    generator.ProjectDirectory(this.testProjectPath);
                    generator.LoadKernels(
                        this.kernelsRegister.getProjects(this.kernelAttributes.companyId).getItem(this.kernelAttributes.target),
                        ($status : boolean, $kernels : IKernelCodeAttributes[]) : void => {
                            this.kernelsRegister.AddKernels(this.kernelAttributes.companyId, this.kernelAttributes.target, $kernels);
                            if (this.kernelsRegister.ValidateKernel(this.kernelAttributes)) {
                                generator.GenerateKernel(this.kernelsRegister.getProjects(this.kernelAttributes.companyId)
                                    .getItem(this.kernelAttributes.target), this.kernelAttributes, ($status : boolean) : void => {
                                    if ($status) {
                                        Echo.Println("Kernel has been generated successfully");
                                        compareNextFile(0);
                                    } else {
                                        Echo.Println("Kernel generation has failed");
                                        $done();
                                    }
                                });
                            } else {
                                $done();
                            }
                        },
                        testRoot + "/ProjectRegister.json");
                }, true);
            };
        }

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.kernelsRegister.Load(LanguageType.EN, $done, true);
            };
        }
    }
}
/* dev:end */
