/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProjectManager = Io.VisionSDK.Studio.Services.Utils.ProjectManager;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IStatus = Io.VisionSDK.Studio.Services.Interfaces.IStatus;
    import IProjectSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IProjectSettingsValue;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;

    export class ProjectManagerTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private fileSystem : FileSystemHandlerConnector;
        private testProjectFolder : string;
        private tempPath : string;
        private descriptor : IProjectSettingsValue;
        private onTestCompleteHandler : any;

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector();
        }

        public TestCreateProject() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.CreateProject(this.descriptor, ($status : IStatus) : void => {
                    this.handleStatus($status);
                });
            });
        }

        public TestUpdateProject() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                this.descriptor.name = "UPDATED NAME";
                this.descriptor.path = this.testProjectFolder + "/UPDATED_PATH";
                $manager.UpdateProject(this.descriptor, ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        this.assertEquals($manager.getEnvironment().dao.getVisualGraph().Name(), this.descriptor.name,
                            "DAO name equals");
                        this.assertEquals($manager.getProjectFolder(), this.descriptor.path,
                            "DAO path equals");
                        this.assertEquals($manager.Settings().projectSettings.name, this.descriptor.name,
                            "Settings name equals");
                        this.assertEquals($manager.Settings().projectSettings.path, this.descriptor.path,
                            "Settings path equals");
                        this.handleComplete();
                    });
                });
            });
        }

        public TestSaveProject() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.SaveProject(($status : IStatus) : void => {
                    this.handleStatus($status);
                });
            });
        }

        public TestLoadProject() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                this.descriptor.path = this.testProjectFolder + "/UPDATED_PATH";
                $manager.UpdateProject(this.descriptor, ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        $manager.LoadProject(this.descriptor.path, ($status : IStatus) : void => {
                            this.handleStatus($status);
                        });
                    });
                });
            });
        }

        public TestInitTaskManager() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.InitTaskManager(($status : IStatus) : void => {
                    this.handleStatus($status);
                });
            });
        }

        public TestIsBuildable() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                this.assertEquals($manager.IsBuildable(), false, "Project not buildable");
                $manager.InitTaskManager(($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        this.assertEquals($manager.IsBuildable(), true, "Project buildable");
                        this.handleComplete();
                    });
                });
            });
        }

        public TestLoadExample() : IRuntimeTestPromise {
            this.timeoutLimit(30000);
            return this.initEnv(($manager : ProjectManager) : void => {
                this.assertEquals($manager.IsExampleLoaded(), false, "Example not loaded");
                this.descriptor.path = $manager.getEnvironment().dao.getPageConfiguration().projectsList[4];
                $manager.LoadExample(this.descriptor, ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        this.assertEquals($manager.IsExampleLoaded(), true, "Example loaded");
                        this.handleComplete();
                    });
                });
            });
        }

        public TestSetRemoteBuildTargetPersisted() : IRuntimeTestPromise {
            this.timeoutLimit(20000);
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.setBuildTarget($manager.Settings().connectionSettings.options[0].name, ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        this.assertEquals($manager.Settings().selectedPlatform,
                            $manager.Settings().connectionSettings.options[0].name,
                            "Platform equals");
                        $manager.setBuildTarget($manager.Settings().connectionSettings.options[1].name, ($status : IStatus) : void => {
                            this.handleStatus($status, () : void => {
                                this.getManagerMock(($mock : ProjectManager) : void => {
                                    $mock.CreateInitialWorkspace(($status : IStatus) : void => {
                                        this.handleStatus($status, () : void => {
                                            this.assertEquals($mock.Settings().selectedPlatform,
                                                $manager.Settings().connectionSettings.options[1].name,
                                                "Platform equals");
                                            this.handleComplete();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }

        public TestCreateInitialWorkspaceLoadPersisted() : IRuntimeTestPromise {
            this.timeoutLimit(20000);
            return this.initEnv(($manager : ProjectManager) : void => {
                this.descriptor.name = "UPDATED NAME";
                this.descriptor.path = this.testProjectFolder + "/UPDATED_PATH";
                $manager.UpdateProject(this.descriptor, ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        BaseDAO.UnloadResource(this.descriptor.path + "/VisualGraph.jsonp");
                        this.getManagerMock(($mock : ProjectManager) : void => {
                            $mock.CreateInitialWorkspace(($status : IStatus) : void => {
                                this.handleStatus($status, () : void => {
                                    this.assertEquals($mock.getEnvironment().dao.getVisualGraph().Name(), this.descriptor.name,
                                        "DAO name equals");
                                    this.assertEquals($mock.getProjectFolder(), this.descriptor.path,
                                        "DAO path equals");
                                    this.assertEquals($mock.Settings().projectSettings.name, this.descriptor.name,
                                        "Settings name equals");
                                    this.assertEquals($mock.Settings().projectSettings.path, this.descriptor.path,
                                        "Settings path equals");
                                    this.handleComplete();
                                });
                            }, true);
                        }, false);
                    });
                });
            });
        }

        // WARNING : toolchain dependency install
        public __IgnoreTestSetLocalBuildTarget() : IRuntimeTestPromise {
            this.timeoutLimit(9999999);
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.setBuildTarget($manager.Settings().toolchainSettings.options[0].name, ($status : IStatus) : void => {
                    this.handleStatus($status);
                });
            });
        }

        public TestInspectExportPackage() : IRuntimeTestPromise {
            const expected : string[] = [
                "/VisionSDK",
                "/include",
                "/source",
                "/data/input/bikegray_640x480.png",
                "/data/output",
                "/build.cmd",
                "/clean.cmd",
                "/run.cmd",
                "/CMakeLists.txt",
                "/README.txt",
                "/LICENSE.txt"
            ];
            return this.initEnv(($manager : ProjectManager) : void => {
                const examples : any = {
                    cameraInput       : this.tempPath + "/vision-sdk-studio-examples/56e091aa07996577ad2e50118e3aee12587e174d",
                    edgeDetection     : this.tempPath + "/vision-sdk-studio-examples/10c00e775ad1039df7244819eb4c6df4669bffe0",
                    functionalTestCase: this.getAbsoluteRoot() + "/test/resource/data/Io/VisionSDK/Studio/Services/" +
                        "CodeGenerator/testFunctional",
                    videoProcessing   : this.tempPath + "/vision-sdk-studio-examples/b2c9bdd627bae28e4e2a293a837fd6015da7d7c6"
                };
                $manager.LoadProject(examples.functionalTestCase + "/VisualGraph.jsonp",
                    ($status : IStatus) : void => {
                        this.handleStatus($status, () : void => {
                            const content : string[] = $manager.InspectExportPackage(PlatformType.WIN_GCC);
                            this.assertEquals(JSON.stringify(content), JSON.stringify(expected), "Content equals");
                            this.handleComplete();
                        });
                    }, false);
            });
        }

        // WARNING : vision sdk install
        public __IgnoreTestExportProject() : IRuntimeTestPromise {
            this.timeoutLimit(30000);
            return this.initEnv(($manager : ProjectManager) : void => {
                const examples : any = {
                    cameraInput       : this.tempPath + "/vision-sdk-studio-examples/56e091aa07996577ad2e50118e3aee12587e174d",
                    edgeDetection     : this.tempPath + "/vision-sdk-studio-examples/10c00e775ad1039df7244819eb4c6df4669bffe0",
                    functionalTestCase: this.getAbsoluteRoot() + "/test/resource/data/Io/VisionSDK/Studio/Services/" +
                        "CodeGenerator/testFunctional",
                    videoProcessing   : this.tempPath + "/vision-sdk-studio-examples/b2c9bdd627bae28e4e2a293a837fd6015da7d7c6"
                };
                const ignoreList : string[] = [
                    // "/VisionSDK",
                    // "/data/input/bikegray_640x480.png"
                ];
                $manager.LoadProject(examples.functionalTestCase + "/VisualGraph.jsonp", ($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        $manager.ExportProject(PlatformType.WIN_GCC, ignoreList, ($status : IStatus) : void => {
                                this.handleStatus($status);
                            },
                            this.tempPath + "/vision-sdk-studio-export/testExport.zip",
                            true
                        );
                    });
                }, false);
            });
        }

        public TestBuildInitialWorkspace() : IRuntimeTestPromise {
            this.timeoutLimit(9999999);
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.InitTaskManager(($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        $manager.BuildProject(($status : IStatus) : void => {
                            this.handleStatus($status);
                        }, true);
                    });
                });
            });
        }

        public TestBuildExample() : IRuntimeTestPromise {
            this.timeoutLimit(9999999);
            return this.initEnv(($manager : ProjectManager) : void => {
                $manager.InitTaskManager(($status : IStatus) : void => {
                    this.handleStatus($status, () : void => {
                        this.descriptor.path = $manager.getEnvironment().dao.getPageConfiguration().projectsList[1];
                        $manager.LoadExample(this.descriptor, ($status : IStatus) : void => {
                            this.handleStatus($status, () : void => {
                                $manager.BuildProject(($status : IStatus) : void => {
                                    this.handleStatus($status);
                                }, true);
                            });
                        });
                    });
                });
            });
        }

        public TestOpenProjectFolder() : IRuntimeTestPromise {
            return this.initEnv(($manager : ProjectManager) : void => {
                Echo.Println("Opening project folder");
                $manager.OpenProjectFolder();
                this.handleComplete();
            });
        }

        protected setUp() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.fileSystem.getTempPath().Then(($path : string) : void => {
                    this.tempPath = $path;
                    this.testProjectFolder = this.getAbsoluteRoot() + "/project-manager-test-project";
                    this.tearDown()(() : void => {
                        $done();
                    });
                });
            };
        }

        protected tearDown() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Delete(this.testProjectFolder).Then(() : void => {
                    $done();
                });
            };
        }

        private getManagerMock($callback : ($mock : ProjectManager) => void, $force : boolean = true) : void {
            const dao = new MainPageDAO();
            dao.Load(LanguageType.EN, () : void => {
                dao.getModelArgs();
                const mock = new ProjectManager(this.getAbsoluteRoot(), {
                    dao,
                    environment: this.getEnvironmentArgs(),
                    fileSystem : new FileSystemHandlerConnector(),
                    terminal   : new TerminalConnector()
                });
                mock.setOnPrint(($message : string) : void => {
                    LogIt.Debug($message);
                    Echo.Print(".");
                });
                $callback(mock);
            }, $force);
        }

        private initEnv($callback : ($manager : ProjectManager) => void, $createInitialWorkspace : boolean = true) : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Delete(this.testProjectFolder).Then(() : void => {
                    this.onTestCompleteHandler = () => {
                        this.onTestCompleteHandler = () => {
                            // empty handler
                        };
                        $done();
                    };
                    this.getManagerMock(($mock : ProjectManager) : void => {
                        this.descriptor = <IProjectSettingsValue>{
                            createNew    : true,
                            description  : "TestProjectDescription TestProjectDescription TestProjectDescription TestProjectDescription ",
                            name         : "TestProjectName",
                            path         : this.testProjectFolder,
                            versionPrefix: "OpenVX ",
                            vxVersion    : Object.keys($mock.getEnvironment().dao.getPageConfiguration().globalSettings.templates)[1]
                        };
                        if ($createInitialWorkspace) {
                            $mock.CreateInitialWorkspace(($status : IStatus) : void => {
                                this.handleStatus($status, () : void => {
                                    $callback($mock);
                                });
                            }, false);
                        } else {
                            $callback($mock);
                        }
                    });
                });
            };
        }

        private handleStatus($status : IStatus, $onContinue? : () => void) : void {
            if (ObjectValidator.IsSet($status.success)) {
                if ($status.success) {
                    if (ObjectValidator.IsSet($onContinue)) {
                        Echo.Println("<i style=\"line-height: 50px; text-decoration: underline\">" + $status.message + "</i>");
                        $onContinue();
                    } else {
                        this.assertEquals($status.success, true, "Status: " + $status.message);
                        this.handleComplete();
                    }
                } else {
                    this.assertEquals($status.success, true, "Status: " + $status.message);
                    this.handleComplete();
                }
            } else {
                Echo.Println($status.message);
            }
            WindowManager.ScrollToBottom();
        }

        private handleComplete() : void {
            this.fileSystem.Delete(this.testProjectFolder).Then(() : void => {
                this.onTestCompleteHandler();
            });
        }
    }
}
/* dev:end */
