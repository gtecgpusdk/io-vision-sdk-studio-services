/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import RegressionTestRunner = Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RegressionTestRunner;

    export class FunctionalTestCases extends RegressionTestRunner {

        constructor() {
            super();
            this.setMethodFilter(
                "testWinGCC_1_1",
                "testWinGCC_1_2"
                // "testLinux_1_1",
                // "testLinux_1_2",
                // "testNN"
            );
        }

        public testWinGCC_1_1() : void {
            this.setPlatform(PlatformType.WIN_GCC, "win-connector-agent");
            // this.setSDKVersion("1.2.0");
            this.setRuntimeVersion("2019.1.0");
            this.generateUseCaseScope("OpenVX_1_1/testFunctional");
        }

        public testWinGCC_1_2() : void {
            this.setPlatform(PlatformType.WIN_GCC, "win-connector-agent");
            this.setSDKVersion("1.2.0");
            this.setRuntimeVersion("2019.1.0");
            this.generateUseCaseScope("OpenVX_1_2/testFunctional");
        }

        public testLinux_1_1() : void {
            this.setPlatform(PlatformType.LINUX, "linux-connector-agent");
            this.setSDKPath("/home/wuibox/WUIFramework/Projects/b48779/visionSDK/" +
                "io-vision-sdk-studio-libs/build/releases/lib/target/resource/libs/VisionSDK");
            this.setCMakePath("/home/wuibox/WUIFramework/com-wui-framework-builder/external_modules/cmake/bin");
        }

        public testLinux_1_2() : void {
            this.setPlatform(PlatformType.LINUX, "linux-connector-agent");
            this.setSDKPath("/home/wuibox/WUIFramework/Projects/b48779/visionSDK/" +
                "io-vision-sdk-studio-libs/build/releases/lib/target/resource/libs/VisionSDK");
            this.setCMakePath("/home/wuibox/WUIFramework/com-wui-framework-builder/external_modules/cmake/bin");
        }

        public testNN() : void {
            this.setSDKVersion("1.2.0");
            this.setRuntimeVersion("2019.1.0");
            const testCaseName : string = "OpenVX_1_2/testXorNN";
            this.setUseCaseScope(
                testCaseName,
                [
                    "/" + testCaseName + "/include/Manager.hpp",
                    "/" + testCaseName + "/include/Reference.hpp",
                    "/" + testCaseName + "/include/VisualGraph.hpp",
                    "/" + testCaseName + "/include/vxContext0.hpp",
                    "/" + testCaseName + "/include/vxGraph0.hpp",
                    "/" + testCaseName + "/source/main.cpp",
                    "/" + testCaseName + "/source/Manager.cpp",
                    "/" + testCaseName + "/source/VisualGraph.cpp",
                    "/" + testCaseName + "/source/vxContext0.cpp",
                    "/" + testCaseName + "/source/vxGraph0.cpp",
                    "/" + testCaseName + "/CMakeLists.txt",
                    "/" + testCaseName + "/LICENSE.txt",
                    "/" + testCaseName + "/VisualGraph.jsonp",
                    "/" + testCaseName + "/data/output/xorNNOutput.txt"
                ],
                "VisualGraph.jsonp");
        }

        protected setUp() : void {
            super.setUp();
            this.generateUseCaseScope("testFunctional");
            this.EnableCppBuild();
            this.DisableCppBuildCache();
        }

        private generateUseCaseScope($testCaseName : string, $concatData : string[] = []) : void {
            this.setUseCaseScope(
                $testCaseName,
                [
                    "/" + $testCaseName + "/include/VisualGraph.hpp",
                    "/" + $testCaseName + "/include/Reference.hpp",
                    "/" + $testCaseName + "/include/Context0.hpp",
                    "/" + $testCaseName + "/include/Graph0.hpp",
                    "/" + $testCaseName + "/include/Manager.hpp",
                    "/" + $testCaseName + "/source/VisualGraph.cpp",
                    "/" + $testCaseName + "/source/Context0.cpp",
                    "/" + $testCaseName + "/source/Graph0.cpp",
                    "/" + $testCaseName + "/source/Manager.cpp",
                    "/" + $testCaseName + "/source/main.cpp",
                    "/" + $testCaseName + "/CMakeLists.txt",
                    "/" + $testCaseName + "/LICENSE.txt",
                    "/" + $testCaseName + "/VisualGraph.jsonp",
                    "/" + $testCaseName + "/data/output/obikeaccq_640x480_P400_16b.png",
                    "/" + $testCaseName + "/data/output/obikeaccu_640x480_P400_16b.png",
                    "/" + $testCaseName + "/data/output/obikeaccw_640x480_P400_16b.png"
                ].concat($concatData),
                "VisualGraph.jsonp");
        }
    }
}
/* dev:end */
