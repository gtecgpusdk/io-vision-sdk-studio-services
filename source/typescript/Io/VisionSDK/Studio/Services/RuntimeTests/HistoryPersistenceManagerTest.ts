/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HistoryPersistenceManager = Io.VisionSDK.Studio.Services.Utils.HistoryPersistenceManager;
    import HistoryManager = Io.VisionSDK.Studio.Gui.Utils.HistoryManager;
    import IHistoryCache = Io.VisionSDK.Studio.Gui.Interfaces.IHistoryCache;
    import GraphChangeType = Io.VisionSDK.Studio.Gui.Enums.GraphChangeType;

    export class HistoryPersistenceManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private readonly fileSystem : FileSystemHandlerConnector;
        private manager : HistoryPersistenceManager;
        private projectId : string;
        private testPath : string;

        constructor() {
            super();
            this.fileSystem = new FileSystemHandlerConnector();
        }

        public TestInitEmptyHistory() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.Init(this.projectId, this.testPath, true, () : void => {
                        this.assertEquals(this.manager.HistoryManager().HistoryCache().id, this.projectId, "Project id equals");
                        $done();
                    });
                });
            };
        }

        public TestInitSavedHistory() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.HistoryManager().HistoryCache(this.getHistoryCacheMock());
                    this.manager.Save(this.projectId, this.testPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Manager save success");
                        if ($status) {
                            this.manager = new HistoryPersistenceManager(this.fileSystem);
                            this.manager.HistoryManager(this.getHistoryManagerMock());
                            this.manager.Init(this.projectId, this.testPath, true, () : void => {
                                this.assertEquals(this.manager.HistoryManager().HistoryCache().graphChanges.length, 3,
                                    "Graph changes length updated");
                                $done();
                            });
                        } else {
                            $done();
                        }
                    });
                });
            };
        }

        public TestInitDifferentIdSamePath() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    const savedCache : IHistoryCache = this.getHistoryCacheMock();
                    this.manager.HistoryManager().HistoryCache(savedCache);
                    this.manager.Save(this.projectId, this.testPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Manager save success");
                        if ($status) {
                            this.manager.Init(this.projectId + this.projectId, this.testPath, true, () : void => {
                                this.assertEquals(this.manager.HistoryManager().HistoryCache().graphChanges.length, 0,
                                    "Graph changes cleaned");
                                $done();
                            });
                        } else {
                            $done();
                        }
                    });
                });
            };
        }

        public TestSave() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.initEnv(() : void => {
                    this.manager.HistoryManager().HistoryCache(this.getHistoryCacheMock());
                    this.manager.Save(this.projectId, this.testPath, ($status : boolean) : void => {
                        this.assertEquals($status, true, "Save status : " + $status);
                        $done();
                    });
                });
            };
        }

        protected setUp() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.testPath = this.getAbsoluteRoot() + "/target/test/history-persistence-manager-test/";
                this.projectId = "TEST-PROJECT-ID";
                $done();
            };
        }

        protected tearDown() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.fileSystem.Delete(this.testPath)
                    .Then(() : void => {
                        $done();
                    });
            };
        }

        private initEnv($callback : () => void) : void {
            this.manager = new HistoryPersistenceManager(this.fileSystem);
            this.manager.HistoryManager(this.getHistoryManagerMock());
            this.fileSystem.Delete(this.testPath)
                .Then(() : void => {
                    $callback();
                });
        }

        private getHistoryCacheMock() : IHistoryCache {
            const cache : IHistoryCache = new IHistoryCache();
            cache.id = this.projectId;
            cache.index = 1;
            cache.graphChanges = [
                <any>{
                    type: GraphChangeType.CREATE
                },
                <any>{
                    type: GraphChangeType.MOVE
                },
                <any>{
                    type: GraphChangeType.REMOVE
                }
            ];
            return cache;
        }

        private getHistoryManagerMock() : HistoryManager {
            let cache : IHistoryCache = new IHistoryCache();
            return <HistoryManager>{
                HistoryCache: ($value? : IHistoryCache) : IHistoryCache => {
                    if (ObjectValidator.IsSet($value)) {
                        cache = $value;
                    }
                    return cache;
                }
            };
        }
    }
}
/* dev:end */
