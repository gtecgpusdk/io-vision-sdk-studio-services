/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import RegressionTestRunner = Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RegressionTestRunner;

    export class AllNodesBuildTest extends RegressionTestRunner {

        constructor() {
            super();
            this.setMethodFilter(
                // "testOVX_1_0_1",
                "testOVX_1_1_0"
            );
        }

        public testOVX_1_0_1() : void {
            const testCaseName : string = "testAllNodes";
            this.generateUseCaseScope(testCaseName);
            this.EnableCppBuild();
            this.DisableCppBuildCache();
            this.DisableRun();
            // this.setSDKPath("D:/__TEMP__/__VSS_GlobalDep/VisionSDK_OVX-1-0-1_2018-0-1");
            // this.setPlatform(PlatformType.LINUX, "http://192.168.56.101:88/connector.config.jsonp");
            // this.setSDKPath("/home/wuibox/WUIFramework/Projects/b48779/visionSDK/" +
            //     "io-vision-sdk-studio-libs/build/releases/lib/target/resource/libs/VisionSDK");
            // this.setCMakePath("/home/wuibox/WUIFramework/com-wui-framework-builder/external_modules/cmake/bin");
        }

        public testOVX_1_1_0() : void {
            const testCaseName : string = "testAllNodes";
            this.generateUseCaseScope("OpenVX_1_1/" + testCaseName);
            this.EnableCppBuild();
            this.DisableCppBuildCache();
            this.DisableRun();
            // this.setRuntimeVersion("2018.1.2-alpha");
            this.setRuntimeVersion("2019.0.0-alpha");
            this.setHubLocation("https://hub.visionstudio.io");
            // this.setSDKPath("D:/__TEMP__/__VSS_GlobalDep/VisionSDK_OVX-1-1-0_2018-0-2");
            // this.setPlatform(PlatformType.LINUX, "http://192.168.56.101:88/connector.config.jsonp");
            // this.setSDKPath("/home/wuibox/WUIFramework/Projects/b48779/visionSDK/" +
            //     "io-vision-sdk-studio-libs/build/releases/lib/target/resource/libs/VisionSDK");
            // this.setCMakePath("/home/wuibox/WUIFramework/com-wui-framework-builder/external_modules/cmake/bin");
        }

        protected generateUseCaseScope($testCaseName : string, $concatData : string[] = []) : void {
            this.setUseCaseScope(
                $testCaseName,
                [
                    "/" + $testCaseName + "/include/VisualGraph.hpp",
                    "/" + $testCaseName + "/include/Reference.hpp",
                    "/" + $testCaseName + "/include/Manager.hpp",
                    "/" + $testCaseName + "/include/Context0.hpp",
                    "/" + $testCaseName + "/include/Graph0.hpp",
                    "/" + $testCaseName + "/source/VisualGraph.cpp",
                    "/" + $testCaseName + "/source/Context0.cpp",
                    "/" + $testCaseName + "/source/Graph0.cpp",
                    "/" + $testCaseName + "/source/Manager.cpp",
                    "/" + $testCaseName + "/source/main.cpp",
                    "/" + $testCaseName + "/CMakeLists.txt",
                    "/" + $testCaseName + "/LICENSE.txt",
                    "/" + $testCaseName + "/VisualGraph.jsonp"
                ].concat($concatData),
                "VisualGraph.jsonp");
        }
    }
}
/* dev:end */
