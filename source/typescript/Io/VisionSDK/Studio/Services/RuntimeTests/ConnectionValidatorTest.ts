/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import IVXNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNode;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class ConnectionValidatorTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private dataSets : any = [
            {
                data    : "test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/" +
                    "OpenVX_1_0_1/ConnectionRegressionData.jsonp",
                template: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/" +
                    "OpenVX_1_0_1/BlankVisualGraph.jsonp",
                version : APIType.OVX_1_0_1
            },
            {
                data    : "test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/" +
                    "OpenVX_1_1_0/ConnectionRegressionData.jsonp",
                template: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/" +
                    "OpenVX_1_1_0/BlankVisualGraph.jsonp",
                version : APIType.OVX_1_1_0
            }
        ];

        private standardsFilter : string[] = [
            APIType.OVX_1_0_1,
            APIType.OVX_1_1_0
        ];
        private testNodesList : string[] = [
            "IMAGE_INPUT_to_VX_ABSOLUTE_DIFFERENCE",
            "CAMERA_to_VX_ABSOLUTE_DIFFERENCE",
            "IMAGE_INPUT_to_VX_ACCUMULATE",
            "VX_ABSOLUTE_DIFFERENCE_to_IMAGE_OUTPUT",
            "VX_ABSOLUTE_DIFFERENCE_to_DISPLAY",
            "VX_ABSOLUTE_DIFFERENCE_to_VX_ACCUMULATE",
            "VX_ACCUMULATE_to_VX_ABSOLUTE_DIFFERENCE",
            "VX_ABSOLUTE_DIFFERENCE_to_VX_ACCUMULATE_SQUARED",
            "VX_ABSOLUTE_DIFFERENCE_to_VX_ACCUMULATE_WEIGHTED",
            "VX_ABSOLUTE_DIFFERENCE_to_VX_ARITHMETIC_ADDITION",
            "VX_ARITHMETIC_ADDITION_to_VX_ARITHMETIC_SUBTRACTION",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_BITWISE_AND",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_BITWISE_EXCLUSIVE_OR",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_BITWISE_INCLUSIVE_OR",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_BITWISE_NOT",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_BOX_FILTER",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_CANNY_EDGE_DETECTOR",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_CHANNEL_COMBINE",
            "VX_CHANNEL_COMBINE_to_VX_CHANNEL_EXTRACT",
            "VX_COLOR_CONVERT_to_VX_COLOR_CONVERT",
            "IMAGE_INPUT_to_VX_CUSTOM_CONVOLUTION",
            "VX_CUSTOM_CONVOLUTION_to_VX_DILATE_IMAGE",
            "VX_CUSTOM_CONVOLUTION_to_VX_EQUALIZE_HISTOGRAM",
            "VX_CUSTOM_CONVOLUTION_to_VX_ERODE_IMAGE",
            "VX_CUSTOM_CONVOLUTION_to_VX_FAST_CORNER",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_GAUSSIAN_BLUR",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_GAUSSIAN_IMAGE_PYRAMID",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_HARRIS_CORNER",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_HISTOGRAM",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_HALF_SCALE_GAUSSIAN",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_INTEGRAL_IMAGE",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_MAGNITUDE",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_MEAN_AND_STANDARD",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_MEDIAN_FILTER",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_MIN_AND_MAX_LOCATION",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_PHASE",
            "VX_ARITHMETIC_SUBTRACTION_to_VX_PIXEL_WISE_MULTIPLICATION",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_REMAP",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_SCALE_IMAGE",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_SOBEL",
            "VX_SOBEL_to_VX_MAGNITUDE",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_TABLE_LOOK_UP",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_THRESHOLD",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_WARP_AFFINE",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_WARP_PERSPECTIVE",
            "VX_PIXEL_WISE_MULTIPLICATION_to_VX_BIT_DEPTH_CONVERT",
            "VX_GAUSSIAN_IMAGE_PYRAMID_to_VX_OPTICAL_FLOW"
        ];

        protected after() : void {
            WindowManager.ScrollToBottom();
        }

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                const dao : MainPageDAO = new MainPageDAO();
                dao.Load(LanguageType.EN, () : void => {
                    dao.getModelArgs();
                    dao.getVisualGraphEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                        ExceptionsManager.Throw(this.getClassName(), $eventArgs.Exception());
                    });
                    dao.getVisualGraph().Configuration().utils.onError = ($message : string) : void => {
                        ExceptionsManager.Throw(this.getClassName(), $message);
                    };
                    dao.getVisualGraphEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                        Echo.Printf($eventArgs.Exception());
                        LogIt.Error($eventArgs.Exception().ToString("", false));
                    });

                    const getNextDataSet : any = ($index : number) : void => {
                        if ($index < this.dataSets.length) {
                            const dataSet : any = this.dataSets[$index];
                            if (this.standardsFilter.indexOf(dataSet.version) !== -1) {
                                dao.LoadProject(dataSet.template, () : void => {
                                    JsonpLoader.Load(dataSet.data, ($data : IConnectionConfiguration[]) : void => {
                                        const connectionConfigurations : IConnectionConfiguration[] = $data;

                                        this.testNodesList.forEach(($nodeType : string) : void => {
                                            if (!connectionConfigurations.hasOwnProperty($nodeType)) {
                                                ExceptionsManager.Throw(this.getClassName(), "Node type \"" + $nodeType +
                                                    "\" has not been found at regression data.");
                                            }

                                            const prepareTest : any = ($item : string, $count : number, $from : number) : void => {
                                                let name : string = "test" + $item + "_" + dataSet.version;
                                                if ($from !== 0) {
                                                    name += " - <b>" + ($count + 1) + "/" + $from + "</b>";
                                                }
                                                this[name] = () : void => {
                                                    this.validateConnection($item, $count, dao, connectionConfigurations);
                                                };
                                            };

                                            if (ObjectValidator.IsArray(connectionConfigurations[$nodeType])) {
                                                for (let i = 0; i < connectionConfigurations[$nodeType].length; i++) {
                                                    prepareTest($nodeType, i, connectionConfigurations[$nodeType].length);
                                                }
                                            } else {
                                                prepareTest($nodeType, 0, 0);
                                            }
                                        });
                                        getNextDataSet($index + 1);
                                    });
                                });
                            } else {
                                getNextDataSet($index + 1);
                            }
                        } else {
                            $done();
                        }
                    };
                    getNextDataSet(0);
                }, true);
            };
        }

        private createNode($config : IVXNode, $name : string, $dao : MainPageDAO) : KernelNode {
            const vxNode : VXNode = new VXNode();
            vxNode.Configuration($dao.getVisualGraph().Configuration());
            vxNode.Name($name);
            vxNode.FromNodeConfiguration($config);

            const node : KernelNode = vxNode.ToKernelNode();

            $dao.getModelArgs().EditorPanelViewerArgs().VisualGraphPanelArgs()
                .GraphRoot().ChildNodes().getFirst().ChildNodes().getFirst().ChildNodes().Add(node);

            return node;
        }

        private validateConnection($nodeType : string, $index : number, $dao : MainPageDAO,
                                   $connectionConfigurations : IConnectionConfiguration[]) : void {
            const processConfig : any = ($config : IConnectionConfiguration) : void => {
                let ruleIndex : number = 0;
                const validateSingle : any = ($rule : IConnectionRule) : void => {
                    const kernelNodes : ArrayList<IKernelNode> = $dao.getModelArgs()
                        .EditorPanelViewerArgs().VisualGraphPanelArgs().GraphRoot().ChildNodes().getFirst()
                        .ChildNodes().getFirst().ChildNodes();
                    while (kernelNodes.Length() !== 0) {
                        kernelNodes.RemoveLast();
                    }

                    let prefix = $nodeType;
                    if (!ObjectValidator.IsEmptyOrNull($index)) {
                        prefix += "[" + $index + "]";
                    }
                    prefix += ".rule[" + ruleIndex + "]";

                    const node1 : KernelNode = this.createNode($config.node1, prefix + "_node1", $dao);
                    const node2 : KernelNode = this.createNode($config.node2, prefix + "_node2", $dao);

                    $dao.FromGraphRoot($dao.getModelArgs().EditorPanelViewerArgs().VisualGraphPanelArgs().GraphRoot(), true);

                    const status : boolean = $dao.Validator(node1, node2, $rule.outputIndex, $rule.inputIndex, true, true).status;
                    this.assertEquals(status, $rule.valid);
                    if (status !== $rule.valid) {
                        Echo.Println("<u>rule index: </u>" + ruleIndex.toString());
                        Echo.Println("<u>source: </u>");
                        Echo.Printf("node: {0}, outputIndex: {1}", node1.Name(), $rule.outputIndex.toString());
                        Echo.Println("<u>target: </u>");
                        Echo.Printf("node: {0}, inputIndex: {1}", node2.Name(), $rule.inputIndex.toString());
                    }

                    if (!ObjectValidator.IsEmptyOrNull($rule.node2AttributeValue)) {
                        const validateAttribute : any = ($attrCond : IAttributeCondition) : void => {
                            const attr : IKernelNodeAttribute = node2.getAttributes().getItem($attrCond.varName);
                            if (ObjectValidator.IsEmptyOrNull(attr)) {
                                ExceptionsManager.Throw(this.getClassName(), "Attribute \"" + $attrCond.varName +
                                    "\" to check \"" + node2.Type() + "\" has not been found for test " + $nodeType);
                            } else {
                                this.assertEquals(attr.value, $attrCond.value);
                                if (attr.value !== $attrCond.value) {
                                    Echo.Println("<u>rule index: </u>" + ruleIndex.toString());
                                    Echo.Println("<u>attribute: </u>" + attr.name);
                                    Echo.Println("<u>actual: </u>");
                                    Echo.Printf("node: {0}, outputIndex: {1}", node1.Type(), $rule.outputIndex.toString());
                                    Echo.Println("<u>expected: </u>");
                                    Echo.Printf("node: {0}, inputIndex: {1}", node2.Type(), $rule.inputIndex.toString());
                                }
                            }
                        };

                        if (ObjectValidator.IsArray($rule.node2AttributeValue)) {
                            (<IAttributeCondition[]>$rule.node2AttributeValue).forEach(validateAttribute);
                        } else {
                            validateAttribute(<IAttributeCondition>$rule.node2AttributeValue);
                        }
                    }
                    ruleIndex++;
                };

                $config.rules.forEach(validateSingle);
            };

            if (ObjectValidator.IsArray($connectionConfigurations[$nodeType])) {
                processConfig($connectionConfigurations[$nodeType][$index]);
            } else {
                processConfig($connectionConfigurations[$nodeType]);
            }
        }
    }

    class IAttributeCondition {
        public varName : string;
        public value : any;
    }

    class IConnectionRule {
        public outputIndex : number;
        public inputIndex : number;
        public valid : boolean;
        public node2AttributeValue? : IAttributeCondition | IAttributeCondition[];
    }

    class IConnectionConfiguration {
        public node1 : IVXNode;
        public node2 : IVXNode;
        public rules : IConnectionRule[];
    }
}
/* dev:end */
