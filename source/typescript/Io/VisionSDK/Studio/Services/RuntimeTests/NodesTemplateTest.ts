/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import VXContext = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXContext;
    import VXGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXGraph;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import IVXNodeModel = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeModel;
    import IVXDataJson = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXDataJson;
    import IVXNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNode;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class NodesTemplateTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private dataSets : any = [
            {
                data    : "../../test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/" +
                    "OpenVX_1_0_1/NodesRegressionData.jsonp",
                template: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/" +
                    "OpenVX_1_0_1/BlankVisualGraph.jsonp",
                version : APIType.OVX_1_0_1
            },
            {
                data    : "../../test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/" +
                    "OpenVX_1_1_0/NodesRegressionData.jsonp",
                template: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/" +
                    "OpenVX_1_1_0/BlankVisualGraph.jsonp",
                version : APIType.OVX_1_1_0
            },
            {
                data    : "../../test/resource/data/Io/VisionSDK/Studio/Services/DAO/CodeGenerator/" +
                    "OpenVX_1_2_0/NodesRegressionData.jsonp",
                template: "../../resource/data/Io/VisionSDK/Studio/Services/Configuration/" +
                    "OpenVX_1_2_0/BlankVisualGraph.jsonp",
                version : APIType.OVX_1_2_0
            }
        ];

        private standardsFilter : string[] = [
            APIType.OVX_1_0_1,
            APIType.OVX_1_1_0,
            APIType.OVX_1_2_0
        ];
        private testNodesList : string[] = [
            "IMAGE_INPUT",
            "IMAGE_OUTPUT",
            "VIDEO_INPUT",
            "VIDEO_OUTPUT",
            "CAMERA",
            "DISPLAY",
            "VX_ABSOLUTE_DIFFERENCE",
            "VX_ACCUMULATE",
            "VX_ACCUMULATE_SQUARED",
            "VX_ACCUMULATE_WEIGHTED",
            "VX_ARITHMETIC_ADDITION",
            "VX_ARITHMETIC_SUBTRACTION",
            "VX_BIT_DEPTH_CONVERT",
            "VX_BITWISE_AND",
            "VX_BITWISE_INCLUSIVE_OR",
            "VX_BITWISE_EXCLUSIVE_OR",
            "VX_BITWISE_NOT",
            "VX_BOX_FILTER",
            "VX_CANNY_EDGE_DETECTOR",
            "VX_CHANNEL_COMBINE",
            "VX_CHANNEL_EXTRACT",
            "VX_COLOR_CONVERT",
            "VX_CUSTOM_CONVOLUTION",
            "VX_DILATE_IMAGE",
            "VX_EQUALIZE_HISTOGRAM",
            "VX_ERODE_IMAGE",
            "VX_FAST_CORNER",
            "VX_GAUSSIAN_BLUR",
            "VX_GAUSSIAN_IMAGE_PYRAMID",
            "VX_HARRIS_CORNER",
            "VX_HISTOGRAM",
            "VX_HALF_SCALE_GAUSSIAN",
            "VX_INTEGRAL_IMAGE",
            "VX_MAGNITUDE",
            "VX_MEAN_AND_STANDARD",
            "VX_MEDIAN_FILTER",
            "VX_MIN_AND_MAX_LOCATION",
            "VX_OPTICAL_FLOW",
            "VX_PHASE",
            "VX_PIXEL_WISE_MULTIPLICATION",
            "VX_REMAP",
            "VX_SCALE_IMAGE",
            "VX_SOBEL",
            "VX_TABLE_LOOK_UP",
            "VX_THRESHOLD",
            "VX_WARP_AFFINE",
            "VX_WARP_PERSPECTIVE"
        ];

        private testNodesExtend : ITestNodeExtend[] = [
            {
                list   : [
                    "VX_NONLINEAR_FILTER",
                    "VX_LAPLACIAN_PYRAMID",
                    "VX_LAPLACIAN_PYRAMID_RECONSTRUCTION"
                ],
                version: APIType.OVX_1_1_0
            },
            {
                list   : [
                    // "VX_DATA_OBJECT_COPY",
                    "VX_NON_MAXIMA_SUPPRESSION",
                    "VX_MIN",
                    "VX_MAX",
                    "VX_MATCH_TEMPLATE",
                    "VX_LBP",
                    "VX_HOUGH_LINES_P"
                ],
                version: APIType.OVX_1_2_0
            },
            {
                // NN extension
                list   : [
                    "DATA_INPUT",
                    "DATA_OUTPUT",
                    "VX_BILATERAL_FILTER",
                    "VX_HOG_CELLS",
                    "VX_HOG_FEATURES",
                    "VX_TENSOR_MULTIPLY",
                    "VX_TENSOR_ADD",
                    "VX_TENSOR_SUBTRACT",
                    "VX_TENSOR_TABLE_LOOKUP",
                    "VX_TENSOR_TRANSPOSE",
                    "VX_TENSOR_CONVERT_BIT_DEPTH",
                    "VX_TENSOR_MATRIX_MULTIPLY",
                    "VX_ACTIVATION_LAYER",
                    "VX_FULLY_CONNECTED_LAYER",
                    "VX_CONVOLUTION_LAYER",
                    "VX_DECONVOLUTION_LAYER",
                    "VX_NORMALIZATION_LAYER",
                    "VX_POOLING_LAYER",
                    "VX_ROI_POOLING_LAYER",
                    "VX_SOFTMAX_LAYER"
                ],
                version: APIType.OVX_1_2_0
            }
        ];

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                const getNextDataSet : any = ($index : number) : void => {
                    if ($index < this.dataSets.length) {
                        const dataSet : any = this.dataSets[$index];
                        if (this.standardsFilter.indexOf(dataSet.version) !== -1) {
                            const dao : VisualGraphDAO = new VisualGraphDAO();
                            dao.setConfigurationPath(dataSet.template);
                            dao.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                                Echo.Printf($eventArgs.Exception());
                                LogIt.Error($eventArgs.Exception().ToString("", false));
                            });
                            dao.Load(LanguageType.EN, () : void => {
                                JsonpLoader.Load(dataSet.data, ($data : INodesGoldenDataGroup[]) : void => {
                                    const nodes : string[] = [];
                                    let nodeType : string;
                                    let extTestNodesList : string[] = this.testNodesList;
                                    let ext : any;
                                    for (ext in this.testNodesExtend) {
                                        if (this.testNodesExtend.hasOwnProperty(ext) &&
                                            StringUtils.ContainsIgnoreCase(this.testNodesExtend[ext].version, dataSet.version)) {
                                            extTestNodesList = extTestNodesList.concat(this.testNodesExtend[ext].list);
                                        }
                                    }

                                    for (nodeType in $data) {
                                        if ($data.hasOwnProperty(nodeType) && nodeType !== "extendsConfig" &&
                                            extTestNodesList.indexOf(nodeType) !== -1) {
                                            nodes.push(nodeType);
                                        }
                                    }
                                    nodes.forEach(($nodeType : string) : void => {
                                        this["test" + $nodeType + "_" + dataSet.version] = () : void => {
                                            this.validateNode($nodeType, dao, $data);
                                        };
                                    });
                                    getNextDataSet($index + 1);
                                });
                            }, true);
                        } else {
                            getNextDataSet($index + 1);
                        }
                    } else {
                        $done();
                    }
                };
                getNextDataSet(0);
                Echo.Print("<style>pre {background-color: #ededed;}</style>");
            };
        }

        protected printFailure($actual : string, $expected : string) : void {
            if (ObjectValidator.IsString($actual) && ObjectValidator.IsString($expected)) {
                const convertToHtml : any = ($value : string) : string => {
                    $value = StringUtils.Replace($value, " ", StringUtils.Space());
                    $value = StringUtils.Replace($value, "<", "&lt;");
                    return StringUtils.Replace($value, ">", "&gt;");
                };
                const printCode : any = ($value : string) : void => {
                    $value = StringUtils.Replace($value, "\r\n", "\\r\\n" + StringUtils.NewLine());
                    Echo.Print("<div style=\"background-color: #ededed;\">" +
                        "<div style=\"font-family: Consolas; width: max-content; margin-top: 16px; margin-bottom: 16px;\">" +
                        $value +
                        "</div>" +
                        "</div>");
                };

                Echo.Println("<u>actual:</u>");
                Echo.PrintCode($actual);

                Echo.Println("<u>expected:</u>");
                let index : number = 0;
                const actualLength : number = StringUtils.Length($actual);
                const expectedLength : number = StringUtils.Length($expected);
                while (index < expectedLength) {
                    if ($actual[index] !== $expected[index] || index >= actualLength) {
                        break;
                    }
                    index++;
                }
                printCode(convertToHtml(StringUtils.Substring($expected, 0, index)) +
                    "<span style=\"background-color: #fac6c9;\">" + convertToHtml(StringUtils.Substring($expected, index)) + "</span>");
            } else {
                super.printFailure($actual, $expected);
            }
        }

        private createNode($type : string, $dao : VisualGraphDAO) : VXNode {
            const context : VXContext = new VXContext();
            context.Index(0);
            context.Configuration($dao.getConfigurationInstance());
            const graph : VXGraph = new VXGraph();
            graph.Index(0);
            graph.Parent(context);
            graph.Configuration(context.Configuration());
            context.getVxGraphs().Add(graph);

            const node : VXNode = new VXNode();
            node.Type($type);
            node.Name("testNode");
            node.Index(0);
            if (context.Configuration().vxAPI.vxIoComs.hasOwnProperty($type)) {
                if (!node.getOwners().Contains(graph)) {
                    node.getOwners().Add(graph);
                }
                node.Parent(context.Parent());
            } else {
                if (!node.getOwners().Contains(graph)) {
                    node.getOwners().Add(graph);
                }
                node.Parent(graph);
            }
            node.Configuration(graph.Configuration());
            node.UniqueId((<any>node).getVarName());
            graph.getVxNodes().Add(node);

            const nodeData : IVXNodeModel = node.ToModel();
            nodeData.inputs().forEach(($input : IVXNode) : void => {
                (<any>$input).Parent(graph);
                (<any>$input).Index(graph.getVxData().Length());
                graph.getVxData().Add((<any>$input));
                node.getInputs().Add((<any>$input));
            });
            nodeData.outputs().forEach(($output : IVXNode) : void => {
                (<any>$output).Parent(graph);
                (<any>$output).Index(graph.getVxData().Length());
                graph.getVxData().Add((<any>$output));
                node.getOutputs().Add((<any>$output));
            });
            nodeData.attributes();
            nodeData.params().forEach(($param : IVXNode) : void => {
                node.getParams().Add($param);
            });

            return node;
        }

        private validateNode($nodeType : string, $dao : VisualGraphDAO, $goldenData : INodesGoldenDataGroup[]) : void {
            if (!$dao.getTemplates().vxAPI.vxIoComs.hasOwnProperty($nodeType) &&
                !$dao.getTemplates().vxAPI.vxNodes.hasOwnProperty($nodeType) &&
                !$dao.getTemplates().vxAPI.vxData.hasOwnProperty($nodeType)) {
                this.assertEquals(false, true, "Node type \"" + $nodeType + "\" has not been found at templates");
            } else {
                const node : VXNode = this.createNode($nodeType, $dao);
                this.assertEquals(node.ToJsonp().node(), $goldenData[$nodeType].jsonp.node, "JSONP");
                this.assertEquals(node.ToJsonp().data().length, $goldenData[$nodeType].jsonp.data.length, "Validate count of VXData");
                let dataIndex : number = 0;
                node.ToJsonp().data().forEach(($data : IVXDataJson) : void => {
                    this.assertEquals(
                        $data.content, $goldenData[$nodeType].jsonp.data[dataIndex], "Validate VXData [" + dataIndex + "]");
                    dataIndex++;
                });
                if (ObjectValidator.IsSet($goldenData[$nodeType].cpp.property)) {
                    this.assertEquals(node.ToCode().property(), $goldenData[$nodeType].cpp.property, "CPP Property");
                }
                this.assertEquals(node.ToCode().create(), $goldenData[$nodeType].cpp.create.node, "CPP Create");
                this.assertEquals(node.ToCode().process(), $goldenData[$nodeType].cpp.process.node, "CPP Process");

                const expectedCreateDataCount : number = $goldenData[$nodeType].cpp.create.data.length;
                const expectedProcessDataCount : number = $goldenData[$nodeType].cpp.process.data.length;
                let createDataIndex : number = 0;
                let processDataIndex : number = 0;
                const validateData : any = ($node : VXNode) : void => {
                    const createData : string = $node.ToCode().create();
                    const processData : string = $node.ToCode().process();
                    if (!ObjectValidator.IsEmptyOrNull(createData) || expectedCreateDataCount > 0) {
                        this.assertEquals(createData,
                            createDataIndex < expectedCreateDataCount ? $goldenData[$nodeType].cpp.create.data[createDataIndex] : "",
                            "Validate Cpp Create Data [" + createDataIndex + "]");
                        createDataIndex++;
                    }
                    if (!ObjectValidator.IsEmptyOrNull(processData) || expectedProcessDataCount > 0) {
                        this.assertEquals(processData,
                            processDataIndex < expectedProcessDataCount ? $goldenData[$nodeType].cpp.process.data[processDataIndex] : "",
                            "Validate Cpp Process Data [" + processDataIndex + "]");
                        processDataIndex++;
                    }
                };
                node.getInputs().foreach(validateData);
                node.getOutputs().foreach(validateData);
                this.assertEquals(createDataIndex, expectedCreateDataCount, "Validate count of CPP Create Data");
                this.assertEquals(processDataIndex, expectedProcessDataCount, "Validate count of CPP Process Data");
            }
        }
    }

    class INodesGoldenDataGroup {
        public jsonp : INodesGoldenData;
        public cpp : INodesRegressionDataCpp;
    }

    class INodesRegressionDataCpp {
        public property? : string;
        public create : INodesGoldenData;
        public process : INodesGoldenData;
    }

    class INodesGoldenData {
        public node : string;
        public data : string[];
    }

    class ITestNodeExtend {
        public version : string;
        public list : string[];
    }
}
/* dev:end */
