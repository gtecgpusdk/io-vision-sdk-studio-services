/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ProjectManager = Io.VisionSDK.Studio.Services.Utils.ProjectManager;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import IProjectEnvironment = Io.VisionSDK.Studio.Services.Interfaces.IProjectEnvironment;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EditorPanelViewerArgs = Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.Designer.EditorPanelViewerArgs;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import IStatus = Io.VisionSDK.Studio.Services.Interfaces.IStatus;
    import IConnectionSettingsValue = Io.VisionSDK.Studio.Gui.Interfaces.IConnectionSettingsValue;
    import ISettings = Io.VisionSDK.Studio.Gui.Interfaces.ISettings;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import StatusType = Io.VisionSDK.Studio.Services.Enums.StatusType;

    export class ExamplesTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private dataSets : any = [
            /* tslint:disable: object-literal-sort-keys */
            {
                name             : "ArithmeticOperations",
                projectsListIndex: 5,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/red_absdiff_green.png",
                    "/data/output/red_add_green.png",
                    "/data/output/red_subtract_green.png"
                ]
            },
            {
                name             : "BitwiseOperations",
                projectsListIndex: 6,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/blue_not.png",
                    "/data/output/green_exclOR_blue.png",
                    "/data/output/green_inclOR_blue.png",
                    "/data/output/red_and_green.png"
                ]
            },
            {
                name             : "CameraInput",
                projectsListIndex: 1,
                files            : [
                    "/CMakeLists.txt"
                ]
            },
            {
                name             : "CannyEdgeDetector",
                projectsListIndex: 13,
                files            : [
                    "/CMakeLists.txt"
                ]
            },
            {
                name             : "ChannelCombine",
                projectsListIndex: 4,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/combined.png"
                ]
            },
            {
                name             : "ChannelExtract",
                projectsListIndex: 3,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/b_channel.png",
                    "/data/output/g_channel.png",
                    "/data/output/r_channel.png"
                ]
            },
            {
                name             : "EdgeDetection",
                projectsListIndex: 7,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/ioCom1.png",
                    "/data/output/ioCom2.png"
                ]
            },
            {
                name             : "ErodeDilate",
                projectsListIndex: 8,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/dilate_erode.png",
                    "/data/output/erode_dilate.png",
                    "/data/output/thresolded.png"
                ]
            },
            {
                name             : "FileReadWrite",
                projectsListIndex: 0,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/ioCom1.png"
                ]
            },
            {
                name             : "Filters",
                projectsListIndex: 10,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/filterBox.png",
                    "/data/output/filterMedian.png",
                    "/data/output/filterNonlinear.png",
                    "/data/output/filterSobelX.png",
                    "/data/output/filterSobelY.png",
                    "/data/output/ioCom.png"
                ]
            },
            {
                name             : "GaussianFilter",
                projectsListIndex: 9,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/filtered.png",
                    "/data/output/noise.png"
                ]
            },
            {
                name             : "InputShare",
                projectsListIndex: 2,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/ioCom1.png",
                    "/data/output/ioCom2.png",
                    "/data/output/ioCom3.png"
                ]
            },
            {
                name             : "MultipleGraphs",
                projectsListIndex: 15,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/c0g0.png",
                    "/data/output/c0g1.png",
                    "/data/output/c1g2.png",
                    "/data/output/c1g3.png"
                ]
            },
            {
                name             : "VideoProcessing",
                projectsListIndex: 12,
                files            : [
                    "/CMakeLists.txt"
                ]
            },
            {
                name             : "Histogram",
                projectsListIndex: 14,
                files            : [
                    "/CMakeLists.txt"
                ]
            },
            {
                name             : "Warp",
                projectsListIndex: 11,
                files            : [
                    "/CMakeLists.txt"
                ]
            },
            {
                name             : "NeuralNetworkXOR",
                projectsListIndex: 16,
                files            : [
                    "/CMakeLists.txt",
                    "/data/output/xorNNOutput.txt"
                ]
            }
            /* tslint:enable */
        ];
        private connections : IConnectionSettingsValue[] = [
            /* tslint:disable: object-literal-sort-keys */
            {
                name     : "Windows Local",
                platform : "WinGCC",
                address  : "https://localhost.wuiframework.com",
                agentName: "test-connector-win",
                cmake    : "%localappdata%/VisionSDK/ExternalModules/cmake/bin",
                compiler : "%localappdata%/VisionSDK/ExternalModules/msys2/mingw64/bin"
            },
            {
                name     : "Linux Local",
                platform : "Linux",
                address  : "https://localhost.wuiframework.com",
                agentName: "test-connector-linux",
                cmake    : "/var/lib/com-wui-framework-builder/appdata/external_modules/cmake/bin"
            }
            /* tslint:enable */
        ];
        private verbose : boolean = true;
        private inLoopDeadline : number = 5; // s
        private readonly targetName : string = "Local GCC";
        private fileSystem : FileSystemHandlerConnector;
        private terminal : TerminalConnector;
        private manager : ProjectManager;

        constructor() {
            super();
            // this.targetName = "Windows Cloud";
            // this.targetName = "Linux Cloud";
            // this.targetName = "i.MX Cloud";
            // this.targetName = "Windows Local";
            // this.targetName = "Linux Local";
            // this.setMethodFilter("ArithmeticOperations");
        }

        protected validate($index : any, $callback : () => void) : void {
            this.compareNextFile(this.dataSets[$index], 0, () : void => {
                $callback();
            });
        }

        protected compareNextFile($data : any, $index : number, $callback : () => void) : void {
            if ($index < $data.files.length) {
                const testRoot : string = this.getAbsoluteRoot() +
                    "/test/resource/data/Io/VisionSDK/Studio/Services/ExamplesGoldenData/" + $data.name;
                const targetRoot : string = this.manager.getProjectFolder();
                const file : string = $data.files[$index];
                this.fileSystem.Exists(targetRoot + file).Then(($status : boolean) : void => {
                    if ($status) {
                        this.fileSystem.Exists(testRoot + file).Then(($status : boolean) : void => {
                            if ($status) {
                                this.fileSystem.Read(targetRoot + file).Then(($actual : string) : void => {
                                    this.fileSystem.Read(testRoot + file).Then(($expected : string) : void => {
                                        if (!ObjectValidator.IsEmptyOrNull($actual) &&
                                            !ObjectValidator.IsEmptyOrNull($expected)) {
                                            this.assertEquals(
                                                JSON.stringify($actual), JSON.stringify($expected), targetRoot + file);
                                        } else {
                                            this.assertEquals(
                                                "", "Not empty file content", targetRoot + file);
                                        }
                                        WindowManager.ScrollToBottom();
                                        this.compareNextFile($data, $index + 1, $callback);
                                    });
                                });
                            } else {
                                this.assertEquals("", testRoot + file, "test data not found");
                                this.compareNextFile($data, $index + 1, $callback);
                            }
                        });
                    } else {
                        this.assertEquals("", targetRoot + file, "golden data not found");
                        this.compareNextFile($data, $index + 1, $callback);
                    }
                });
            } else {
                $callback();
            }
        }

        protected loadExample($index : number, $done : () => void) : void {
            const dao : MainPageDAO = this.manager.getEnvironment().dao;
            this.manager.LoadExample(<any>{
                path: dao.getPageConfiguration().projectsList[this.dataSets[$index].projectsListIndex]
            }, ($status : IStatus) : void => {
                if (ObjectValidator.IsSet($status.success)) {
                    this.assertEquals($status.success, true, "Project load status");
                    if ($status.success) {
                        const args : EditorPanelViewerArgs = dao.getModelArgs().EditorPanelViewerArgs();
                        const root : IKernelNode = args.VisualGraphPanelArgs().GraphRoot();
                        dao.FromGraphRoot(root);

                        const settings : ISettings = this.manager.Settings();
                        this.connections.forEach(($testConnection : IConnectionSettingsValue) : void => {
                            let exists : boolean = false;
                            settings.connectionSettings.options.forEach(($connection : IConnectionSettingsValue) : void => {
                                if ($connection.name === $testConnection.name) {
                                    exists = true;
                                }
                            });
                            if (!exists) {
                                settings.connectionSettings.options.push($testConnection);
                            }
                        });
                        this.manager.Settings(settings);
                        this.manager.setBuildTarget(this.targetName, ($status : IStatus) : void => {
                            if ($status.success) {
                                Echo.Println("Starting build...");
                                this.manager.BuildProject(($status : IStatus) : void => {
                                    if (ObjectValidator.IsSet($status.success)) {
                                        this.assertEquals($status.success, true, "Project build status");
                                        if ($status.success) {
                                            let inLoop : boolean = false;
                                            dao.getVisualGraph().getIoComs().foreach(($ioCom : VXNode) : boolean => {
                                                if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.VIDEO_INPUT ||
                                                    $ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.CAMERA) {
                                                    inLoop = true;
                                                }
                                                return inLoop;
                                            });
                                            let autoStop : boolean = false;
                                            dao.getVisualGraph().getIoComs().foreach(($ioCom : VXNode) : boolean => {
                                                if ($ioCom.Type() === $ioCom.Configuration().vxAPI.vxTypes.CAMERA) {
                                                    autoStop = true;
                                                }
                                                return autoStop;
                                            });
                                            if (inLoop) {
                                                Echo.Println("Starting execution...");
                                            }
                                            this.manager.TaskManager()
                                                .Run(inLoop, ($message : string, $status : boolean) : void => {
                                                    if (ObjectValidator.IsSet($status)) {
                                                        this.assertEquals($status, true, "Project run status");
                                                        if (!autoStop) {
                                                            this.validate($index, $done);
                                                        }
                                                    }
                                                    if (this.verbose) {
                                                        Echo.Println($message);
                                                    }
                                                    WindowManager.ScrollToBottom();
                                                });
                                            if (autoStop) {
                                                this.getEventsManager().FireAsynchronousMethod(() : void => {
                                                    this.manager.TaskManager()
                                                        .Stop(true, ($message : string, $status : boolean) : void => {
                                                            if (ObjectValidator.IsSet($status)) {
                                                                this.assertEquals($status, true, "Project stop status");
                                                                if (this.verbose) {
                                                                    Echo.Println("Project message : " + $message);
                                                                }
                                                                this.validate($index, $done);
                                                            }
                                                        });
                                                }, this.inLoopDeadline * 1000);
                                            }
                                        }
                                    }
                                    if (this.verbose) {
                                        Echo.Println($status.message);
                                    }
                                    WindowManager.ScrollToBottom();
                                }, true);
                            } else {
                                Echo.Println($status.message);
                            }
                        });
                    }
                } else {
                    Echo.Println($status.message);
                }
            });
        }

        protected iterateDataSets() : void {
            const dataLength : number = this.dataSets.length;
            for (let index : number = 0; index < dataLength; index++) {
                this["test" + this.dataSets[index].name] = () : IRuntimeTestPromise => {
                    this.timeoutLimit(-1);
                    return ($done : () => void) : void => {
                        this.loadExample(index, $done);
                    };
                };
            }
        }

        protected createManager($callback : () => void) : void {
            const dao : MainPageDAO = new MainPageDAO();
            dao.Load(LanguageType.EN, () : void => {
                dao.getModelArgs();
                const env : IProjectEnvironment = {
                    dao,
                    environment: this.getEnvironmentArgs(),
                    fileSystem : this.fileSystem,
                    terminal   : this.terminal
                };
                this.manager = new ProjectManager(this.getAbsoluteRoot(), env);
                this.manager.setOnPrint(($message : string) : void => {
                    if (this.verbose) {
                        Echo.Println($message);
                    }
                });
                this.manager.setOnError(($message : string) : void => {
                    Echo.Println($message);
                });
                this.manager.CreateInitialWorkspace(($status : IStatus) : void => {
                    if ($status.type !== StatusType.PROGRESS) {
                        if ($status.success) {
                            $callback();
                        } else {
                            Echo.Println("Failed to create workspace");
                        }
                    }
                });
            }, true);
        }

        protected before() : IRuntimeTestPromise {
            this.timeoutLimit(-1);
            return ($done : () => void) : void => {
                this.fileSystem = new FileSystemHandlerConnector();
                this.terminal = new TerminalConnector();
                this.createManager(() : void => {
                    this.iterateDataSets();
                    $done();
                });
            };
        }
    }
}
/* dev:end */
