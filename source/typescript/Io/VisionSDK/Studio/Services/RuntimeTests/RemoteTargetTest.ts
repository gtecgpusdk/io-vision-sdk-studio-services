/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectDecoderValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import TaskManager = Io.VisionSDK.Studio.Services.Utils.TaskManager;
    import PlatformType = Io.VisionSDK.Studio.Services.Enums.PlatformType;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import RemoteTargetManager = Io.VisionSDK.Studio.Services.Utils.RemoteTargetManager;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import APIType = Io.VisionSDK.Studio.Services.Enums.APIType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import MainPageDAO = Io.VisionSDK.Studio.Services.DAO.Pages.MainPageDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import IMainPageNotifications = Io.VisionSDK.Studio.Services.Interfaces.DAO.IMainPageNotifications;

    export class RemoteTargetTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private msysPath : string;
        private connection : RemoteTargetManager;
        private userName : string;
        private host : string;
        private pass : string;
        private agentName : string;
        private sshPort : number;
        private devices : IDeviceInfo[];
        private deviceList : DropDownList;
        private tfTargetIp : TextField;
        private tfUserName : TextField;
        private tfPass : TextField;
        private tfSshPort : TextField;
        private tfTargetAgent : TextField;
        private notifications : IMainPageNotifications;

        constructor() {
            super();
            this.setMethodFilter("RemoteConnectorInitTest", "LinuxRemoteTest", "iMXRemoteTest");

            this.devices = <IDeviceInfo[]>[
                {
                    ip      : "10.171.89.252",
                    name    : "GPUSDK-testboard3 (i.MX8 QM)",
                    userName: "wuiclient"
                },
                {
                    ip      : "10.171.89.253",
                    name    : "GPUSDK-testboard4 (i.MX8 QM)",
                    userName: "wuiclient"
                },
                {
                    ip      : "10.171.88.252",
                    name    : "GPUSDK-testboard1 (i.MX8 DV)",
                    userName: "wuiclient"
                },
                {
                    ip      : "10.171.88.2",
                    name    : "GPUSDK-testboard2 (i.MX8 DV)",
                    userName: "wuiclient"
                },
                {
                    ip      : "192.168.56.102",
                    name    : "VM Linux",
                    userName: "wui"
                },
                {
                    ip         : "127.0.0.1",
                    name       : "wui-docker",
                    sshPort    : 5001,
                    targetAgent: "test-linux-agent",
                    userName   : "root"
                },
                {
                    ip  : "",
                    name: "Custom"
                }
            ];

            this.deviceList = new DropDownList(DropDownListType.SETTING);
            this.deviceList.Width(350);
            this.deviceList.MaxVisibleItemsCount(10);

            this.devices.forEach(($item : IDeviceInfo) : void => {
                this.deviceList.Add($item.name, $item.ip);
            });

            this.deviceList.Select(this.devices[0].ip);

            this.tfTargetIp = new TextField();
            this.tfTargetIp.Width(350);
            this.tfTargetIp.ReadOnly(true);
            this.tfUserName = new TextField();
            this.tfUserName.Width(350);
            this.tfPass = new TextField();
            this.tfPass.Width(350);
            this.tfPass.setPasswordEnabled();

            this.tfSshPort = new TextField();
            this.tfSshPort.Width(350);

            this.tfTargetAgent = new TextField();
            this.tfTargetAgent.Width(350);

            this.sshPort = 22;
        }

        public RemoteConnectorInitTest() : void {
            this.addButton("Init connector (run after every target config change", () : void => {
                this.updateInputs();
                this.connection = new RemoteTargetManager({
                    agentName: this.agentName,
                    host     : this.host,
                    pass     : this.pass,
                    user     : this.userName
                }, {
                    homePath : this.getAbsoluteRoot() + "/..",
                    msysPath : this.msysPath,
                    serverUrl: "https://hub.dev.visionstudio.io",
                    sshPort  : this.sshPort
                });
                this.connection.setOnError(($message : string) : void => {
                    Echo.Printf($message);
                });
                this.connection.setOnPrint(($message : string) : void => {
                    Echo.PrintCode($message);
                    WindowManager.ScrollToBottom();
                });
                this.connection.Start();
            });
        }

        public LinuxRemoteTest() : void {
            this.generateButtons(() : TaskManager => {
                let instance : TaskManager = null;
                const single : any = () : TaskManager => {
                    if (ObjectValidator.IsEmptyOrNull(instance)) {
                        this.updateInputs();
                        instance = new TaskManager(this.getAbsoluteRoot() +
                            "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator/" +
                            "OpenVX_1_1/testFunctional", "TestCase_1_1",
                            {
                                agentName     : this.agentName,
                                cmakePath     : "/tmp/com-wui-framework-builder/external_modules/cmake/bin",
                                notifications  : this.notifications,
                                platform      : PlatformType.LINUX,
                                runtimeVersion: "2018.2.0-alpha",
                                sdkPath       : "/var/tmp/vision-sdk-studio/io-vision-sdk-studio-libs/" +
                                    "resource/libs/VisionSDK",
                                sdkVersion    : APIType.OVX_1_1_0,
                                serverUrl     : "https://hub.dev.visionstudio.io"
                            },
                            this.connection);
                    }
                    return instance;
                };

                return single();
            });
        }

        public iMXRemoteTest() : void {
            this.generateButtons(() : TaskManager => {
                let instance : TaskManager = null;
                const single : any = () : TaskManager => {
                    if (ObjectValidator.IsEmptyOrNull(instance)) {
                        this.updateInputs();
                        instance = new TaskManager(this.getAbsoluteRoot() +
                            "/test/resource/data/Io/VisionSDK/Studio/Services/CodeGenerator/" +
                            "OpenVX_1_1/testFunctional", "TestCase_1_1",
                            {
                                agentName     : this.agentName,
                                notifications  : this.notifications,
                                platform      : PlatformType.IMX,
                                runtimeVersion: "2018.2.0-alpha",
                                sdkPath       : "/var/tmp/vision-sdk-studio/io-vision-sdk-studio-libs/" +
                                    "resource/libs/VisionSDK",
                                sdkVersion    : APIType.OVX_1_1_0,
                                serverUrl     : "https://hub.dev.visionstudio.io"
                            },
                            this.connection);
                    }
                    return instance;
                };

                return single();
            });
        }

        protected before() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                const dao : MainPageDAO = new MainPageDAO();
                dao.Load(LanguageType.EN, () : void => {
                    this.notifications = dao.getPageConfiguration().notifications;
                    const terminal : TerminalConnector = new TerminalConnector();
                    terminal.Execute("wui", ["--", "where", "msys2.exe"])
                        .Then(($exitCode : number, $stdout : string) : void => {
                            if ($exitCode === 0) {
                                this.msysPath = StringUtils.Substring($stdout, 0, StringUtils.IndexOf($stdout, "msys2.exe") - 1);
                                $done();
                            } else {
                                Echo.Printf("Unable to find WUI Builder external_modules path");
                            }
                        });
                });
            };
        }

        protected after() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.updateControls();
                $done();
            };
        }

        protected resolver() : void {
            super.resolver();

            Echo.Print(
                "<style>.TextField input{z-index: 0 !important;}</style>"
            );

            Echo.Print(
                "<h2>Target credentials</h2>" +
                "Target Device<br>" + this.deviceList.Draw() + "<br>" +
                "Target IP<br>" + this.tfTargetIp.Draw() + "<br>" +
                "Root Username<br>" + this.tfUserName.Draw() + "<br>" +
                "Password<br>" + this.tfPass.Draw() + "<br>" +
                "SshPort<br>" + this.tfSshPort.Draw() + "<br>" +
                "Agent<br>" + this.tfTargetAgent.Draw() + "<br>"
            );

            this.deviceList.getEvents().setOnChange(() : void => {
                LogIt.Debug("Changed to: {0}", this.deviceList.Value());
                this.updateControls();
            });
        }

        private generateButtons($configurer : () => TaskManager) : void {
            this.addButton("Clean up", () : void => {
                const instance : TaskManager = $configurer();
                instance.Init(() : void => {
                    instance.Clean(($status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("Clean up successful");
                        } else {
                            Echo.Println("Clean up failed");
                        }
                    });
                });
            });

            this.addButton("Synchronize", () : void => {
                const instance : TaskManager = $configurer();
                instance.Init(() : void => {
                    instance.Synchronize(($message : string, $status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("OK: " + $message);
                        } else {
                            Echo.Println("Fail: " + $message);
                        }
                    });
                });
            });

            this.addButton("Build", () : void => {
                const instance : TaskManager = $configurer();
                instance.Init(() : void => {
                    instance.Build(($message : string, $status? : boolean) : void => {
                        Echo.Println($message);
                        if (!ObjectDecoderValidator.IsEmptyOrNull($status)) {
                            if ($status === true) {
                                Echo.Println("build successful");
                            } else {
                                Echo.Println("build failed");
                            }
                        }
                    });
                });
            });

            this.addButton("Run", () : void => {
                const instance : TaskManager = $configurer();
                instance.Init(() : void => {
                    instance.Run(false, ($message : string, $status : boolean) : void => {
                        if ($status === true) {
                            Echo.Println("run successful");
                        } else {
                            Echo.Println("run failed");
                        }
                    });
                });
            });
        }

        private updateControls() : void {
            let selectedItem : IDeviceInfo = null;
            this.devices.forEach(($value : IDeviceInfo) : void => {
                if ($value.ip === this.deviceList.Value()) {
                    selectedItem = $value;
                }
            });

            if (!ObjectValidator.IsEmptyOrNull(selectedItem) && selectedItem.ip !== "") {
                this.tfTargetIp.Value(selectedItem.ip);
                this.tfUserName.Value(selectedItem.userName);
                this.tfSshPort.Value((ObjectValidator.IsInteger(selectedItem.sshPort) ? selectedItem.sshPort : this.sshPort).toString());
                this.tfTargetAgent.Value(ObjectValidator.IsEmptyOrNull(selectedItem.targetAgent) ?
                    this.agentName : selectedItem.targetAgent);
                this.tfTargetIp.ReadOnly(true);
            } else {
                this.tfTargetIp.ReadOnly(false);
            }
        }

        private updateInputs() : void {
            this.userName = this.tfUserName.Value();
            this.host = this.tfTargetIp.Value();
            this.pass = this.tfPass.Value();
            this.sshPort = !ObjectValidator.IsEmptyOrNull(this.tfSshPort.Value()) ?
                StringUtils.ToInteger(this.tfSshPort.Value()) : this.sshPort;
            this.agentName = this.tfTargetAgent.Value();

            LogIt.Debug("inputs: {0},{1}, [{2}, {3}]", this.userName, this.host, this.sshPort, this.agentName);
        }
    }

    class IDeviceInfo {
        public name : string;
        public ip : string;
        public userName? : string;
        public targetAgent? : string;
        public sshPort? : number;
    }
}
/* dev:end */
