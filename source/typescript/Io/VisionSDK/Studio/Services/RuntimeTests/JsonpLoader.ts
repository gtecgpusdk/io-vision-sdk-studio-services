/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Resources = Io.VisionSDK.Studio.Services.DAO.Resources;

    export class JsonpLoader extends Resources {
        public static Load($filePath : string, $successHandler : ($data : any, $filePath? : string) => void,
                           $errorHandler? : ($eventArgs? : ErrorEvent, $filePath? : string) => void) : void {
            const configs : any[] = [];
            let mergedData : any;
            const loadNext : any = ($path : string, $callback : () => void) : void => {
                Resources.Load($path, ($data : any) : void => {
                    configs.unshift($data);
                    if ($data.hasOwnProperty("extendsConfig")) {
                        loadNext($data.extendsConfig, $callback);
                    } else {
                        $callback();
                    }
                }, $errorHandler);
            };

            loadNext($filePath, () : void => {
                if (configs.length > 0) {
                    mergedData = configs[0];
                    let index : number;
                    for (index = 0; index < configs.length - 1; index++) {
                        Resources.Extend(mergedData, Resources.DeepClone(configs[index + 1]));
                    }
                    $successHandler(mergedData);
                } else {
                    $errorHandler(new ErrorEvent("Failed to load and merge configuration."));
                }
            });
        }
    }
}
/* dev:end */
