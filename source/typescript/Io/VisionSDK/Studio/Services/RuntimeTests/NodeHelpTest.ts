/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.Studio.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ICodeGeneratorTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICodeGeneratorTemplates;
    import IVXNodeTemplate = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeTemplate;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import VisualGraphDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.VisualGraphDAO;

    export class NodeHelpTest extends Io.VisionSDK.Studio.Services.HttpProcessor.Resolvers.RuntimeTestRunner {
        private nodesFilter : string[] = [
            // "VX_ABSOLUTE_DIFFERENCE"
        ];

        public testAllVxNodes110() : IRuntimeTestPromise {
            return ($done : any) : void => {
                this.buildVxNodesHelp("OpenVX_1_1_0", () : void => {
                    $done();
                });
            };
        }

        public testAllVxNodes120() : IRuntimeTestPromise {
            return ($done : any) : void => {
                this.buildVxNodesHelp("OpenVX_1_2_0", () : void => {
                    $done();
                });
            };
        }

        private buildVxNodesHelp($version : string, $done : any) {
            const graph : VisualGraphDAO = new VisualGraphDAO();
            graph.setConfigurationPath(
                "resource/data/Io/VisionSDK/Studio/Services/Configuration/" + $version + "/BlankVisualGraph.jsonp");
            graph.Load(LanguageType.EN, () : void => {
                const data : ICodeGeneratorTemplates = graph.getTemplates();
                let nodeName : string;
                for (nodeName in data.vxAPI.vxNodes) {
                    if ((this.nodesFilter.length === 0 ||
                        this.nodesFilter.length !== 0 && this.nodesFilter.indexOf(nodeName) !== -1) &&
                        data.vxAPI.vxNodes.hasOwnProperty(nodeName)) {
                        const template : IVXNodeTemplate = data.vxAPI.vxNodes[nodeName];
                        Echo.Print("" +
                            "<div style=\"width: 450px; height: 600px; padding: 5px; overflow: auto; " +
                            "float: left; margin: 10px; border: 1px solid silver;\">" +
                            "<h2>" + template.help.name + " [" + nodeName + "]</h2>" +
                            "<a href=\"" + template.help.link + "\" target=\"_blank\">more info</a>" +
                            "<div style=\"width: 400px; text-align: justify; padding: 10px; background-color: ghostwhite;\">" +
                            StringUtils.Replace(template.help.description, "  ", StringUtils.Space()) +
                            "</div>" +
                            "</div>");
                    }
                }
                Echo.Println("<div style=\"clear: both;\"></div>");
                $done();
            });
        }
    }
}
/* dev:end */
