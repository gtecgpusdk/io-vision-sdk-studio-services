/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.CodeGenerator {
    "use strict";
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BaseGeneratorDAO extends BaseDAO {

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IVisualGraph":
                return VisualGraphDAO;
            case "IKernelTemplates":
                return KernelTemplatesDAO;
            case "IKernelsRegister":
                return KernelsRegisterDAO;
            case "ICodeGeneratorTemplates":
                return BaseGeneratorDAO;
            default:
                break;
            }
            return BaseGeneratorDAO;
        }

        protected getResourcesHandler() : any {
            return Resources;
        }
    }
}
