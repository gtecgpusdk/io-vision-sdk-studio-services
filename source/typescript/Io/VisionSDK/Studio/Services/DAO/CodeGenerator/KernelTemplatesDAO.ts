/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.CodeGenerator {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IKernelTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelTemplates;

    export class KernelTemplatesDAO extends BaseGeneratorDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Configuration/KernelTemplates.jsonp";

        public getStaticConfiguration() : IKernelTemplates {
            return <IKernelTemplates>super.getStaticConfiguration();
        }
    }
}
