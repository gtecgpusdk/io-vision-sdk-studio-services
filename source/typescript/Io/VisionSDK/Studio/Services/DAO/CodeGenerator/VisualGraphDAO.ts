/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.CodeGenerator {
    "use strict";
    import VisualGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraph;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import VXNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXNode;
    import VXContext = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXContext;
    import IVisualGraphNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraphNode;
    import IVisualGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVisualGraph;
    import IVXNode = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNode;
    import IVXContext = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXContext;
    import IVXGraph = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXGraph;
    import VXGraph = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VXGraph;
    import IJsonpTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IJsonpTemplates;
    import VisualGraphNode = Io.VisionSDK.Studio.Services.CodeGenerator.Models.VisualGraphNode;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import IErrorEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IErrorEventsHandler;
    import IEventsManager = Com.Wui.Framework.Commons.Interfaces.IEventsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ICodeGeneratorTemplates = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.ICodeGeneratorTemplates;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IVXNodeJson = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXNodeJson;
    import IVXDataJson = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IVXDataJson;

    export class VisualGraphDAO extends BaseGeneratorDAO {
        private graph : VisualGraph;
        private connectionsMap : IVisualGraphNode[];
        private readonly nodesMap : VisualGraphNode[];
        private connectionsRegister : any[];
        private templates : ICodeGeneratorTemplates;
        private events : IEventsManager;

        constructor() {
            super();
            this.connectionsMap = [];
            this.nodesMap = [];
            this.connectionsRegister = [];
            this.events = Loader.getInstance().getHttpResolver().getEvents();
        }

        public getEvents() : IVisualGraphDAOEvents {
            return {
                setOnError: ($handler : IErrorEventsHandler) : void => {
                    this.events.setEvent(VisualGraphNode.ClassName(), EventType.ON_ERROR, $handler);
                }
            };
        }

        public getStaticConfiguration() : IVisualGraph {
            return <IVisualGraph>super.getStaticConfiguration();
        }

        public getVisualGraph() : VisualGraph {
            const config : IVisualGraph = this.getConfigurationInstance();

            this.graph = new VisualGraph();
            this.graph.Configuration(config);
            this.graph.Index(0);
            this.graph.Name(config.name);
            this.graph.Description(config.description);
            if (ObjectValidator.IsEmptyOrNull(config.id)) {
                config.id = this.getUID();
            }
            this.graph.Id(config.id);

            let nodeIndex : number = 0;
            config.ioComs.forEach(($data : IVXNode) : void => {
                const model : VXNode = this.getVXNodeModel($data);
                model.Parent(this.graph);
                model.Index(nodeIndex);
                this.graph.getIoComs().Add(model);
                nodeIndex++;
            });

            nodeIndex = 0;
            config.vxContexts.forEach(($data : IVXContext) : void => {
                const model : VXContext = this.getVXContextModel($data);
                model.Parent(this.graph);
                model.Index(nodeIndex);
                this.graph.getVxContexts().Add(model);
                nodeIndex++;
            });

            this.connectionsRegister.forEach(($data : any) : void => {
                $data.handler($data.index);
            });
            return this.graph;
        }

        public ToJsonp() : string {
            const templates : IJsonpTemplates = this.templates.jsonp;
            if (ObjectValidator.IsEmptyOrNull(this.graph)) {
                this.getVisualGraph();
            }

            const appendData : any = ($input : string, $data : string) : string => {
                if (!ObjectValidator.IsEmptyOrNull($data)) {
                    if (!ObjectValidator.IsEmptyOrNull($input)) {
                        $input += ",\r\n";
                    }
                    $input += $data;
                }
                return $input;
            };

            const generatedData : string[] = [];
            const getVxData : any = ($template : IVXNodeJson, $dataList : ArrayList<string>) : void => {
                const vxData : IVXDataJson[] = $template.data("                        ");
                vxData.forEach(($data : IVXDataJson) : void => {
                    if ((generatedData.indexOf($data.id) === -1 || $data.type === "OUTPUT") && $data.index !== -1) {
                        generatedData.push($data.id);
                        $dataList.Add($data.content, $data.index);
                    }
                });
            };

            let ioComs : string = "";
            this.graph.getIoComs().foreach(($node : VXNode) : void => {
                const template : IVXNodeJson = $node.ToJsonp();
                ioComs = appendData(ioComs, template.node("        "));
            });
            let contexts : string = "";
            this.graph.getVxContexts().foreach(($context : VXContext) : void => {
                let graphs : string = "";
                $context.getVxGraphs().foreach(($graph : VXGraph) : void => {
                    let nodes : string = "";
                    const dataList : ArrayList<string> = new ArrayList<string>();
                    let data : string = "";
                    $graph.getVxNodes().foreach(($node : VXNode) : void => {
                        const template : IVXNodeJson = $node.ToJsonp();
                        nodes = appendData(nodes, template.node("                        "));
                        getVxData(template, dataList);
                    });
                    this.graph.getIoComs().foreach(($node : VXNode) : void => {
                        $node.getInputs().foreach(($input : VXNode) : boolean => {
                            if ($input.Parent() === $graph) {
                                getVxData($node.ToJsonp(), dataList);
                                return false;
                            }
                            return true;
                        });
                        $node.getOutputs().foreach(($output : VXNode) : boolean => {
                            if ($output.Parent() === $graph) {
                                getVxData($node.ToJsonp(), dataList);
                                return false;
                            }
                            return true;
                        });
                    });
                    const sortDataListByKeyNumber : any = () : void => {
                        let current : number;
                        let next : number;
                        let tmp : string;
                        const data : any = dataList.ToArray();
                        const keys : any = dataList.getKeys();
                        const length : number = data.length - 1;
                        let index : number;
                        for (index = 0; index < length; index++) {
                            current = keys[index];
                            next = keys[index + 1];
                            if (current > next) {
                                tmp = data[index];
                                keys[index] = next;
                                data[index] = data[index + 1];
                                keys[index + 1] = current;
                                data[index + 1] = tmp;
                                index = -1;
                            }
                        }
                    };
                    sortDataListByKeyNumber();
                    dataList.foreach(($data : string) : void => {
                        data = appendData(data, $data);
                    });
                    graphs = appendData(graphs, templates.vxGraph("                ", $graph, nodes, data));
                });
                contexts = appendData(contexts, templates.vxContext("        ", $context, graphs));
            });
            return templates.resourceData(templates.visualGraph.apply(this.templates, [
                this.graph.Id(), this.graph.Name(), this.graph.Description(), ioComs, contexts, this.graph.GridDimX(), this.graph.GridDimY()
            ]));
        }

        public getTemplates() : ICodeGeneratorTemplates {
            return this.templates;
        }

        public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
            super.Load($language, () : void => {
                const templates : BaseGeneratorDAO = <BaseGeneratorDAO>this.getImportedDAO("vxAPI");
                let importedTemplate : string;
                const allImports : any = templates.getStaticConfiguration().imports;
                for (importedTemplate in allImports) {
                    if (allImports.hasOwnProperty(importedTemplate)) {
                        Resources.Extend(templates.getStaticConfiguration(),
                            templates.getImportedDAO(importedTemplate).getStaticConfiguration());
                    }
                }
                const help : BaseGeneratorDAO = new BaseGeneratorDAO();
                help.setConfigurationPath((<ICodeGeneratorTemplates>templates.getStaticConfiguration()).helpConfig);
                help.Load($language, () : void => {
                    Resources.Extend(templates.getStaticConfiguration(), help.getStaticConfiguration());
                    this.templates = templates.getConfigurationInstance();
                    const instance : ICodeGeneratorTemplates = this.getConfigurationInstance();
                    instance.vxAPI = this.templates.vxAPI;
                    instance.jsonp = this.templates.jsonp;
                    instance.model = this.templates.model;
                    instance.utils = this.templates.utils;
                    instance.notifications = this.templates.notifications;
                    (<any>instance.model).utils = instance.utils;
                    (<any>instance.model).notifications = instance.notifications;
                    $asyncHandler();
                }, $force);
            }, $force);
        }

        private registerEntity($config : IVisualGraphNode, $node : VisualGraphNode) : void {
            this.connectionsMap.push($config);
            this.nodesMap.push($node);
        }

        private getVXNodeModel($data : IVXNode) : VXNode {
            const model : VXNode = new VXNode();
            model.Configuration(this.getConfigurationInstance());
            model.FromNodeConfiguration($data);
            this.registerEntity($data, model);
            const params : any[] = this.getNodeReference($data.params);
            if (!ObjectValidator.IsEmptyOrNull(params)) {
                let index : number = 0;

                const outputs : IVXNode[] = this.getNodeReference($data.outputs);
                if (!ObjectValidator.IsEmptyOrNull(outputs)) {
                    let index : number = 0;
                    outputs.forEach(($data : IVXNode) : void => {
                        this.registerConnection(($index : number) : void => {
                            const output : VXNode = <VXNode>this.getEntity($data);
                            model.getOutputs().Add(output, $index);
                            const modelOutput : VXNode = model.getOutputs().getItem($index);
                            if (model.Configuration().vxAPI.vxData.hasOwnProperty(modelOutput.Type())) {
                                modelOutput.getInputs().Add(model, 0);
                            }
                        }, index);
                        index++;
                    });
                }

                params.forEach(($data : any) : void => {
                    if (ObjectValidator.IsObject($data)) {
                        this.registerConnection(($index : number) : void => {
                            const entity : VXNode = <VXNode>this.getEntity($data);
                            model.getParams().Add(entity, $index);
                            if (!model.getOutputs().Contains(entity) && !(entity instanceof VXGraph) && !(entity instanceof VXContext)) {
                                model.getInputs().Add(entity);
                            }
                        }, index);
                    } else {
                        model.getParams().Add($data, index);
                    }
                    index++;
                });
            }
            return model;
        }

        private getVXGraphModel($data : IVXGraph) : VXGraph {
            const model : VXGraph = new VXGraph();
            model.Configuration(this.getConfigurationInstance());
            model.FromNodeConfiguration($data);
            this.registerEntity($data, model);

            let nodeIndex : number = 0;
            $data.vxNodes.forEach(($data : IVXNode) : void => {
                const vxNodeModel : VXNode = this.getVXNodeModel($data);
                vxNodeModel.Parent(model);
                vxNodeModel.Index(nodeIndex);
                model.getVxNodes().Add(vxNodeModel);
                nodeIndex++;
            });

            nodeIndex = 0;
            $data.vxData.forEach(($data : IVXNode) : void => {
                const vxDataModel : VXNode = this.getVXNodeModel($data);
                vxDataModel.Parent(model);
                vxDataModel.Index(nodeIndex);
                model.getVxData().Add(vxDataModel);
                nodeIndex++;
            });
            return model;
        }

        private getVXContextModel($data : IVXContext) : VXContext {
            const model : VXContext = new VXContext();
            model.Configuration(this.getConfigurationInstance());
            model.FromNodeConfiguration($data);
            this.registerEntity($data, model);

            let nodeIndex : number = 0;
            $data.vxGraphs.forEach(($data : IVXGraph) : void => {
                const graphModel : VXGraph = this.getVXGraphModel($data);
                graphModel.Parent(model);
                graphModel.Index(nodeIndex);
                model.getVxGraphs().Add(graphModel);
                nodeIndex++;
            });

            return model;
        }

        private getEntity($param : IVisualGraphNode) : VisualGraphNode {
            return this.nodesMap[this.connectionsMap.indexOf($param)];
        }

        private registerConnection($handler : ($index : number) => void, $index : number) : void {
            this.connectionsRegister.push({handler: $handler, index: $index});
        }

        private getNodeReference<T>($param : () => T) : T {
            if (!ObjectValidator.IsEmptyOrNull($param)) {
                return $param.apply(this.getConfigurationInstance(), []);
            } else {
                return null;
            }
        }
    }

    export interface IVisualGraphDAOEvents {
        setOnError($handler : IErrorEventsHandler) : void;
    }
}
