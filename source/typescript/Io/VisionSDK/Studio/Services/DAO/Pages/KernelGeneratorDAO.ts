/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import KernelTemplatesDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.KernelTemplatesDAO;
    import KernelEditorPanelViewerArgs =
        Io.VisionSDK.Studio.Gui.BaseInterface.ViewersArgs.Panels.KernelGenerator.KernelEditorPanelViewerArgs;
    import IKernelGeneratorLocalization = Io.VisionSDK.Studio.Services.Interfaces.DAO.IKernelGeneratorLocalization;
    import KernelsRegisterDAO = Io.VisionSDK.Studio.Services.DAO.CodeGenerator.KernelsRegisterDAO;
    import IKernelsCodeProject = Io.VisionSDK.Studio.Services.Interfaces.DAO.CodeGenerator.IKernelsCodeProject;

    export class KernelGeneratorDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/VisionSDK/Studio/Services/Localization/KernelGeneratorLocalization.jsonp";
        private readonly generatorDao : KernelTemplatesDAO;
        private readonly register : KernelsRegisterDAO;

        constructor() {
            super();
            this.generatorDao = new KernelTemplatesDAO();
            this.register = new KernelsRegisterDAO();
        }

        public getPageConfiguration() : IKernelGeneratorLocalization {
            return <IKernelGeneratorLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : KernelEditorPanelViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IKernelGeneratorLocalization = this.getPageConfiguration();
                const args : KernelEditorPanelViewerArgs = new KernelEditorPanelViewerArgs();
                args.HeaderText(pageConfiguration.header);

                args.ProjectPathText(pageConfiguration.projectPath);
                args.ProjectPathValue("");

                args.ProjectNameText(pageConfiguration.projectName);
                this.register.getProjects(pageConfiguration.companyId).foreach(($project : IKernelsCodeProject) : void => {
                    args.AddProject($project.name);
                });

                args.ProjectNamespaceText(pageConfiguration.projectNamespace);
                args.ProjectNamespaceValue(this.register.getProjects(pageConfiguration.companyId).getFirst().namespace);

                args.KernelNamespaceText(pageConfiguration.kernelNamespace);
                args.KernelNamespaceValue(pageConfiguration.defaultKernelNamespace);

                args.KernelNameText(pageConfiguration.kernelName);
                args.KernelNameValue(pageConfiguration.defaultKernelName);

                args.DescriptionText(pageConfiguration.description);
                args.DescriptionValue("");

                args.ParametersText(pageConfiguration.parameters);
                args.ParameterDirectionText(pageConfiguration.parameterDirection);
                args.ParameterNameText(pageConfiguration.parameterName);
                args.ParameterTypeText(pageConfiguration.parameterType);
                args.ParameterDescriptionText(pageConfiguration.parameterDescription);
                args.AddParameterDirection("VX_INPUT");
                args.AddParameterDirection("VX_OUTPUT");

                let type : string;
                const types : string[] = this.generatorDao.getStaticConfiguration().typesMapping;
                for (type in types) {
                    if (types.hasOwnProperty(type)) {
                        args.AddParameterType(type);
                    }
                }

                args.GenerateButtonText(pageConfiguration.generateButton);

                this.modelArgs = args;
            }
            return <KernelEditorPanelViewerArgs>this.modelArgs;
        }

        public getTemplates() : KernelTemplatesDAO {
            return this.generatorDao;
        }

        public getRegister() : KernelsRegisterDAO {
            return this.register;
        }

        public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
            super.Load($language, () : void => {
                this.generatorDao.Load($language, () : void => {
                    this.register.Load($language, $asyncHandler, $force);
                }, $force);
            }, $force);
        }
    }
}
