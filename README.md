# io-vision-sdk-studio-services v2019.1.0

> WUI Framework library focused on services and business logic for Vision SDK Studio.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Added full support for OVX 1.2 and NN extension. Responsive design enhancements. Stability bug fixes. WUI Core update. 
Switched to project specific Hub locations.
### v2019.0.2
Added compatibility with web release. Usage of new app loader and initial features management. 
Fixed issues with project and task manager initialization. Fixed sync issues.
### v2019.0.1
Stability bug fixes. Updated Wizard and localizations. Added docker config for web release. Updated settings page.
### v2019.0.0
Refactoring of namespaces. Added ability to communicate with devices hosted in cloud. Added settings page instead of dialog. 
Added several managers and tests for handling history, persistence, connections etc. Added ability to create export for i.MX targets.
Added full handling of local toolchain and libraries on the fly. Enabled communication with EAP and Dev WUI Hub environments. 
Integration of lint tool.
### v2018.3.0
WUI Core update. Integrated nodes grouping. Performance enhancements. Stability bug fixes.
### v2018.2.0
Integrated update button. Properties panel, Hierarchy view and NodesPicker integrated into Accordions. Update of Wizard content.
Stability and performance bug fixes. Converted docs to TypeDoc.
### v2018.1.3
Stability and start up bug fixes.
### v2018.1.2
Added novel History manager. Added project export. Update/cleanup of templates and functional tests. 
Stability bug fixes and minor project manager fixes.
### v2018.1.1
Cumulative stability bug fixes. Added animation to AppWizard. Added shared releases configuration. Update of WUI Core.
### v2018.1.0
Update of WUI Core supporting multithreading. Console handling update. Added new examples. Integration of Context and Graph viewer. 
Project manager stability bug fixes. Added localizable notifications. Added conditional connection points and help. Added selfupdate support.
Stopped regeneration of CMake cache.
### v2018.0.5
Added support for multi-select and manipulation with group of nodes. Added new examples. Added support for various image types and Video IOComs. 
Clean up of MainPageController implementation with usage of subscribers. Added runtime test for project management. 
Added support for basic control graph. Added automatic detection of run-in-loop. Added strict filter for code variables and namespaces.
Added support for conditional properties driven by templates.
### v2018.0.4
Integration of projects management and examples pool. Added custom context menu for drop-area items.
### v2018.0.3
Integration of connection settings dialog. Added remote connections manager. 
Update of task manager for ability to manage remote synchronization and other build/run tasks. 
Added ability to override help by new OpenVX version. Toolbar look-and-feel update. Added automated rebuild. 
Fixed input image and video stream read issues. Added missing nodes for OpenVX 1.1.0. Added support for nodes connections correction API.
Added lazy toolchain validation. 
### v2018.0.2
Added crash reporter. Added installation protocol for CMake and MSYS2. Added support for OpenVX 1.1.0 and multi-version templates tests. 
Refactoring of templates loader and runtime tests execution. Added notifications panel. Better support for Linux builds.
### v2018.0.1
Fixed GUI flickering on app and graphs load. Fixed blur effect for app wizard in ChromiumRE. Added ability to use stand-alone toolchain. 
Runtime SDK and templates update. Added support for remote build. Fixed history and CMake cache management.
### v2018.0.0
Added AppWizard and Console output. Added support for GCC toolchain. Update of runtime libraries. 
Nodes and properties readability enhancements. Added support for output connections grouping. 
Fixed loading of projects and front-end to back-end models synchronization. GUI stability bug fixes. Changed version format.
### v1.2.2
Bug fixes for loading of saved projects. Added ability to display messages from connection validator. Added detailed API documentation. 
Split of templates into multiple files for better readability. Responsive design related updates.
### v1.2.1
Added connections validator driven by templates. Responsive design related updates. 
Added support for open of native directory browser dialog.
### v1.2.0
Integration of new GUI features like directory browser, unlimited matrix size, responsive design. Performance and stability bug fixes. 
Update of SCR and change of history ordering. Updated of runtime libraries. Added support for vc140 toolchain. 
Update of connection validator.
### v1.1.2
Converted runner config to JSONP. Added implementation of templates for all VX Nodes. 
Updated synchronization with dynamic properties tab and nodes with connection points. Refactoring and update of tasks manager. 
Added connection to static probing hook. Updated back-end libraries required by generated code at runtime.
### v1.1.1
Added novel data-driven code generator core. Minor front-end enhancements. Update of dependencies.
### v1.1.0
Added fixed version for dependencies. Added regression data and fully automated testing. 
Added synchronization of front-end and back-end models. Added ability to build and run generated code. 
Created jsonp reconstruction from back-end model.
### v1.0.2
Simplification of test cases runner. Added test case 2.1. Fixed Model generator.
### v1.0.1
Integration of GUI layer. Added basic configuration for desktop release.
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
